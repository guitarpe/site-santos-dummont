<?php
$url = filter_input(INPUT_SERVER, 'HTTP_HOST');

if ($url === 'localhost') {
    define('AMBIENTE', 'DEV');
    $diretorio = '/portfolio/santosdummontsite';
    $diretorioup = '/portfolio/santosdummont';
    define('URL', 'http://localhost');
    define('SITE_URL', '/portfolio/santosdummontsite');
    define('FOTOCLIENTE_PATH', substr(realpath(dirname(__FILE__)), 0, -11) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotocliente');
    define('FOTOPRODUTO_PATH', substr(realpath(dirname(__FILE__)), 0, -11) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoproduto');
    define('FOTOPARCEIRO_PATH', substr(realpath(dirname(__FILE__)), 0, -11) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoparceiro');
    define('API_PAGSEGURO_PATH', substr(realpath(dirname(__FILE__)), 0, -11) . 'site' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Application' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . 'PagSeguroLibrary');
    define('ENVIA_EMAIL', true);
    define('MINIMO', '.min');
} else {
    define('AMBIENTE', 'PROD');
    $diretorio = '';
    $diretorioup = '/admin';
    define('URL', 'https://www.santosdumontcomp.com.br');
    define('SITE_URL', $diretorio);
    define('FOTOCLIENTE_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotocliente');
    define('FOTOPRODUTO_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoproduto');
    define('FOTOPARCEIRO_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'fotoparceiro');
    define('API_PAGSEGURO_PATH', substr(realpath(dirname(__FILE__)), 0, -12) . DIRECTORY_SEPARATOR . 'site' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Application' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . 'PagSeguroLibrary');
    define('ENVIA_EMAIL', true);
    define('MINIMO', '.min');
}
define('LOGS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/logs');
define('FAIL_FACEBOOK_CALLBACK', URL . SITE_URL . "/Login/SocialRedirect/1");
define('FAIL_GOOGLE_CALLBACK', URL . SITE_URL . "/Login/SocialRedirect/2");
define('IMAGEM_URL', $diretorio . '/public_html/static/geral/img');
define('SYSTEM_PATH', substr(realpath(dirname(__FILE__)), 0, -6));
define('CONTROLLER_PATH', substr(realpath(dirname(__FILE__)), 0, -6) . 'src' . DIRECTORY_SEPARATOR . 'Application' . DIRECTORY_SEPARATOR . 'Controller');
define('API_PATH', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'geral' . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . 'apis');

define('FOTOENTREGAS_URL', $diretorioup . '/public_html/static/img/meiosentregas');
define('FOTOPAGAMENTO_URL', $diretorioup . '/public_html/static/img/meiospagamentos');
define('FOTOPRODUTO_URL', $diretorioup . '/public_html/static/fotoproduto');
define('FOTOCLIENTE_URL', $diretorioup . '/public_html/static/fotocliente');
define('FOTOCATEGORIAS_URL', $diretorioup . '/public_html/static/fotocategoria');
define('FOTOPARCEIROS_URL', $diretorioup . '/public_html/static/fotoparceiro');
define('FOTOBANNERS_URL', $diretorioup . '/public_html/static/banners');
define('FOTOMODAIS_URL', $diretorioup . '/public_html/static/modais');
define('IMGS_URL', $diretorioup . '/public_html/static/img');
define('PATHSTATIC_URL', $diretorioup . '/public_html/static');
define('GERAL_CSS', $diretorio . '/public_html/static/geral/css');
define('GERAL_JS', $diretorio . '/public_html/static/geral/js');
define('TELAS_JS', $diretorio . '/public_html/static/geral/js/telas');
define('GERAL_IMG', $diretorio . '/public_html/static/geral/img');
define('GERAL_PLUGINS', $diretorio . '/public_html/static/geral/plugins');
define('JS_JQUERY', $diretorio . '/public_html/static/geral/js/jquery');
define('JS_JQUERY_UI', $diretorio . '/public_html/static/geral/js/jquery-ui');
define('TEMA_CSS', $diretorio . '/public_html/static/site/css');
define('TEMA_JS', $diretorio . '/public_html/static/site/js');
define('TEMA_ICONES', $diretorio . '/public_html/static/site/icones');
define('TEMA_FONTES', $diretorio . '/public_html/static/site/fonts');
define('TITLE_SITE', 'Santos Dummont - Componentes Eletrônicos');
define('PASTAS_CONTROLLER', 'Cron/Acesso/Compras/Common/Facebook/PagSeguro/Config/Relatorios/Site');

/* KEYS API CIELO */
define('API_CLIENTID', 'ACbZYRuiYrqdw75PZxuyHVuyhcSM6irP30cYQvGU0DMHLDIV1K/ tnKPXxMqo0WfweYwLXxwZpT2ZsT2ZzJIIR0ZPDDTQRN1wDIkMi');

/* API CORREIOS */
define('CALC_FRETE_WS_API', 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx');
define('CALC_FRETE_USERNAME', '');
define('CALC_FRETE_SENHA', '');
define('IMG_PADRAO_PARCEIRO', URL . $diretorioup . '/public_html/static/img/semimagem.jpg');
define('IMG_PADRAO_PRODUTO', URL . $diretorioup . '/public_html/static/img/sem_imagem_produto.jpg');
define('IMG_PADRAO_CLIENTE', URL . $diretorioup . '/public_html/static/img/semimagem.jpg');

//ENVIO DE EMAILS DO SISTEMA
define('URL_IMG_MAIL', 'https://www.santosdumontcomp.com.br/admin/public_html/static/');
