jQuery(document).ready(function ($) {

    $('[data-toggle="tooltip"]').tooltip();
    $('.alert').delay(6000).fadeOut();
    $('.cpf').mask('000.000.000-00');
    $('.rg').mask('00.000.000-00');
    $('.cep').mask('00000-000');
    $('.data').mask('00/00/0000');
    $('.data-s').mask('00/0000');
    $('.data-ano').mask('0000');
    $('.cnpj').mask('00.000.000/0000-00');
    $('.dinheiro').mask('#.##0,00', { reverse: true });
    $('.numero').mask('#');
    $('.fones').mask('(00) 0000-00009');
    $('.cartcred').mask('0000-0000-0000-0000');
    $('.nccv').mask('000');
    $('.cartdtval').mask('00/0000');

    $isEmpty = function (data) {
        if (typeof (data) == 'number' || typeof (data) == 'boolean') {
            return false;
        }

        if (typeof (data) == 'undefined' || data === null) {
            return true;
        }

        if (typeof (data.length) != 'undefined') {
            return data.length == 0;
        }

        var count = 0;

        for (var i in data) {
            if (data.hasOwnProperty(i)) {
                count++;
            }
        }

        return count == 0;
    };

    $(".cpfcnpj").keydown(function (objEvent) {
        if (objEvent.keyCode != 9) {
            try {
                $(".cpfcnpj").unmask();
            } catch (e) { }

            var tamanho = $(".cpfcnpj").val().replace(/\D/g, '').length;

            if (tamanho < 11) {
                $(".cpfcnpj").mask("999.999.999-99");
            } else {
                $(".cpfcnpj").mask("99.999.999/9999-99");
            }

            // ajustando foco
            var elem = this;
            setTimeout(function () {
                // mudo a posição do seletor
                elem.selectionStart = elem.selectionEnd = 10000;
            }, 0);
            // reaplico o valor para mudar o foco
            var currentValue = $(this).val();
            $(this).val('');
            $(this).val(currentValue);
        }
    });

    $('.no-childs').on('click', function (e) {
        e.preventDefault();
    });

    $(document).on('keyup', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 113) {
            $('#termo_busca').closest('#form-busca').find('#msg-f2').remove();
            $('#termo_busca').focus();
        }

        if (keyCode === 13) {
            $('#form-busca').submit();
        }
    });

    $(document).on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;

        if (keyCode === 13) {
            $('#form_busca_home').submit();
        }
    });

    $('#termo_busca').on('click', function () {
        $(this).closest('#form-busca').find('#msg-f2').remove();
    });

    $('#msg-f2').on('click', function () {
        $(this).remove();
    });

    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('body').on('mouseup', function (e) {
        /*if ($('.popup-carrinho').is(":visible")) {
         $('.popup-carrinho').fadeOut(350);
         }*/
    });

    $('.item_pedido').on('click', function (e) {
        e.preventDefault();

        var detalhe = $(this).data('detalhe');
        $(detalhe).fadeToggle("slow");

        $(this).find('i').toggleClass("down");
    });

    $('#ir_login').on('click', function (e) {
        e.preventDefault();

        $(".area_esqueci_senha").fadeToggle("fast", 0, function () {
            $(".area_login").fadeToggle("slow");
        });
    });

    $('#ir_esqueci_senha').on('click', function (e) {
        e.preventDefault();

        $(".area_login").fadeToggle("fast", 0, function () {
            $(".area_esqueci_senha").fadeToggle("slow");
        });
    });

    $('.numbers').keyup(function () {
        this.value = this.value.replace(/[^0-9\/]/g, '');
    });

    $(".validemail").on("blur", function () {
        var email = $(this).val();
        if (email != "") {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $dispararAlerta('Atenção: E-mail Inválido', 'warning');
            }
        }
    });

    $(".confmail").on('change', function () {
        var senha = $(this).val();
        var campo = $(".email").val();
        if (campo !== senha) {
            $dispararAlerta("Atenção: Repita o mesmo e-mail corretamente", 'warning');
            $(this).val("");
            $(this).focus();
        }
    });

    $(".confisenha").on('change', function () {
        var senha = $(this).val();
        var campo = $(".txtSenha").val();
        if (campo !== senha) {
            $dispararAlerta("Repita a senha corretamente", 'warning');
            $(this).val("");
            $(this).focus();
        }
    });

    $(document).on("blur", ".ecep", function () {

        var cep = $(this).val();

        if (cep != "") {
            var obj = $(this).closest('.area_endereco');

            if ($("#cep_frete").length > 0) {
                $("#cep_frete").val(cep);
            }

            obj.find(".elogradouro").val("");
            obj.find(".ebairro").val("");
            obj.find(".eestado").val("");
            obj.find(".ecidade").val("");
            obj.find(".epais").val("");

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url, function (data) {
                if (!("erro" in data)) {
                    if (Object.prototype.toString.call(data) === '[object Array]') {
                        var data = data[0];
                    }

                    obj.find(".elogradouro").val(data.logradouro.toUpperCase());
                    obj.find(".ebairro").val(data.bairro.toUpperCase());
                    obj.find(".eestado").val(data.uf.toUpperCase()).change();
                    obj.find(".ecidade").val(data.localidade.toUpperCase());
                    obj.find(".eibge").val(data.ibge);
                    obj.find(".epais").val('BRASIL');
                }
            });
        }
    });

    $(document).on('blur', '.cpf', function () {

        var numdoc = $(this);

        if (numdoc.val() !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/Home/ValidaDoc",
                data: {
                    tipo: 1,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == 0) {
                        $dispararAlerta("CPF Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.cpf', function () {

        var numdoc = $(this);
        var tipo = 1;
        var msg = "CPF Inválido";

        if (numdoc.val() !== "") {
            var tamanho = $("#cpfcnpj").val().length;

            if (tamanho < 11) {
                tipo = 1;
                var msg = "CPF Inválido";
            } else {
                tipo = 2;
                var msg = "CNPJ Inválido";
            }

            $.ajax({
                type: "POST",
                url: caminho + "/Home/ValidaDoc",
                data: {
                    tipo: tipo,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == 0) {
                        $dispararAlerta(msg, 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.cnpj', function () {

        var numdoc = $(this);

        if (numdoc.val() !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/Home/ValidaDoc",
                data: {
                    tipo: 2,
                    numdoc: numdoc.val()
                },
                success: function (result) {

                    if (result == 0) {
                        $dispararAlerta("CNPJ Inválido", 'warning');
                        numdoc.val("");
                        numdoc.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.verificaemailcliente', function () {

        var field = $(this);

        if (field.val() !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/Home/VerificaEmailCliente",
                data: {
                    email: field.val()
                },
                success: function (result) {
                    if (result > 0) {
                        $dispararAlerta("E-mail já cadastrado", 'warning');
                        field.val("");
                        field.focus();
                    }
                }
            });
        }
    });

    $(document).on('blur', '.verificaemailcliente', function () {

        var field = $(this);

        if (field.val() !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/Home/VerificaEmailCliente",
                data: {
                    email: field.val()
                },
                success: function (result) {
                    if (result > 0) {
                        $dispararAlerta("E-mail já cadastrado", 'warning');
                        field.val("");
                        field.focus();
                    }
                }
            });
        }
    });

    $(document).on("blur", ".validcpfcliente", function () {

        var field = $(this);

        if (field.val() !== "") {
            $.ajax({
                type: "POST",
                url: caminho + "/Home/VerificaCPFCliente",
                data: {
                    cpf: field.val()
                },
                success: function (result) {
                    if (result > 0) {
                        $dispararAlerta("Documento já cadastrado", 'warning');
                        field.val("");
                        field.focus();
                    }
                }
            });
        }
    });

    $(".addclone").on('click', function () {

        var bloco = $(this).data("bloco");
        var btnrem = $(this).data("rem");

        var $obj = "div" + bloco;
        var $objlast = "div" + bloco + ":last";

        var $row = $($objlast).clone();

        if ($row.find("div.tit h4").length > 0) {
            $row.find("div.tit h4").html("Endereço - " + ($($obj).length + 1));
        }

        if ($row.find("button.remarea").length > 0) {
            $row.find("button.remarea").removeClass("hidden");

            $row.find(".slct_end").val("");
            $row.find(".complto").val("");
            $row.find(".ecep").val("");
            $row.find(".numbers").val("");
            $row.find(".elogradouro").val("");
            $row.find(".ebairro").val("");
            $row.find(".eestado").val("");
            $row.find(".ecidade").val("");
            $row.find(".epais").val("");
        }

        $row.insertAfter($objlast);

        if ($($obj).length > 1) {
            $(btnrem).css('display', 'block');
        } else {
            $(btnrem).css('display', 'none');
        }
    });

    $(".remclone").on('click', function () {

        var bloco = $(this).data("bloco");
        var $obj = "div" + bloco;
        var $objlast = "div" + bloco + ":last";

        if ($($obj).length > 1) {
            $($objlast).remove();
        }

        if ($($obj).length > 1) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $(document).on("click", '.remarea', function () {

        var bloco = $(this).data("bloco");
        var action = $(this).data("action");

        var $objlast = "div" + bloco + ":last";

        if (action == "") {
            $($objlast).remove();
            $dispararAlerta("Removido com sucesso", "success");
        } else {
            var id = $(this).data("id");

            $.ajax({
                url: action,
                type: "POST",
                data: {
                    id: id
                },
                success: function (ret) {
                    $dispararAlerta("Removido com sucesso", "success");

                    $($objlast).remove();
                }
            });
        }
    });

    $(".slct_envio").on("change", function () {

        var value = $(this).val();

        if (value == 1) {
            $('.slct_envio').val(0);
            $(this).val(1);

            if ($(document).find('cep_frete').length) {
                var cep = $(this).closest('div.area_endereco').find('input.cep').val();
                $(document).find('cep_frete').val(cep);

                $.ajax({
                    url: caminho + '/Usuarios/AjaxInibirOpcaoEntrega',
                    type: "POST",
                    data: {
                        cep: cep
                    },
                    success: function (ret) {
                        var data = JSON.parse(ret);

                        if (data[0].ERRO == 0) {
                            $.each(data[0].MEIOS, function (i, item) {

                                var obj = "#op_entrega_" + item.MEIO_ID;

                                $(obj).addClass('hidden');
                            });
                        } else {
                            $('.op_meios_entr').removeClass('hidden');
                        }
                    },
                    error: function (jqXHR, textStatus, errorMessage) {
                        console.log(errorMessage);
                    }
                });
            }
        } else {
            $('.slct_envio').val(1);
            $(this).val(0);
        }
    });

    $(".slct_end").on("change", function () {

        var value1 = $(this).val();
        var id1 = $(this).attr("id");

        $('.slct_end').each(function () {

            var value2 = $(this).val();
            var id2 = $(this).attr("id");

            if (id1 != id2) {
                if (value1 == value2) {
                    $(this).val($.data(this, ''));
                    return false;
                }
            }
        });
    });

    $('.infoprod').scrollbar();

    $('.adicionar').on('click', function (e) {
        e.preventDefault();
        var estoque = 0;
        var id = $(this).data('id');
        var campo = $(this);
        $.ajax({
            type: "POST",
            url: caminho + '/Home/GetEstoque/' + id,
            success: function (ret) {
                var data = JSON.parse(ret);
                estoque = eval(data.PRD_QTDE_ESTOQUE);

                var qty = campo.parent().parent().parent().find('.qty').val();

                if (!isNaN(qty)) {
                    qty++;
                }

                if (estoque >= qty) {
                    campo.parent().parent().parent().find('.qty').val(qty);
                } else {
                    $dispararAlerta('Atençao: Não há estoque suficiente', 'danger');
                }
            }
        });
    });

    $('.remover').on('click', function (e) {
        e.preventDefault();

        var qty = $(this).parent().parent().parent().find('.qty').val();

        if (!isNaN(qty) && qty > 1) {
            qty--;
        }

        $(this).parent().parent().parent().find('.qty').val(qty);

    });

    $("#ex2").slider({});

    /*$('.btn_login_home').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "400px",
            height: "auto",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('#ir_login').on('click', function (e) {
                        e.preventDefault();

                        $(".area_esqueci_senha").fadeToggle("fast", 0, function () {
                            $(".area_login").fadeToggle("slow");
                        });
                    });

                    $('#ir_esqueci_senha').on('click', function (e) {
                        e.preventDefault();

                        $(".area_login").fadeToggle("fast", 0, function () {
                            $(".area_esqueci_senha").fadeToggle("slow");
                        });
                    });

                }
            }
        });
    });*/

    $('.btn_prd_detalhes').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "493px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('.fancybox-wrap .infoprod').scrollbar();

                    $('.adicionar').on('click', function (e) {
                        e.preventDefault();
                        var estoque = 0;
                        var id = $(this).data('id');
                        var campo = $(this);
                        $.ajax({
                            type: "POST",
                            url: caminho + '/Home/GetEstoque/' + id,
                            success: function (ret) {
                                var data = JSON.parse(ret);
                                estoque = eval(data.PRD_QTDE_ESTOQUE);

                                var qty = campo.parent().find('.qty').val();

                                if (!isNaN(qty)) {
                                    qty++;
                                }

                                if (estoque >= qty) {
                                    campo.parent().find('.qty').val(qty);
                                } else {
                                    $dispararAlerta('Atençao: Não há estoque suficiente', 'danger');
                                }
                            }
                        });
                    });

                    $('.remover').on('click', function (e) {
                        e.preventDefault();

                        var qty = $(this).parent().find('.qty').val();

                        if (!isNaN(qty) && qty > 0) {
                            qty--;
                        }

                        $(this).parent().find('.qty').val(qty);
                    });

                    $('.btn_adicionar').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');
                        var qtde = $(this).parent().find('.qty').val();

                        $adicionarProduto(action, id, qtde);
                    });

                    $('.btn_lembrar').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');

                        $lembrarProduto(action, id);

                    });

                    $('.add_favorite').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');
                        var obj = $(this);

                        $adicionarFavorito(action, id, obj);
                    });

                    $('.add_classificacao').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');

                        $adicionarClassificacao(action, id);
                    });

                    $('.tab_prd_info').on('click', function (e) {
                        e.preventDefault();

                        var area = $(this).attr('href');

                        if (area == '#tbp2') {
                            $(document).find('div' + area).addClass('active');
                            $(document).find('div#tbp1').removeClass('active');
                        } else {
                            $(document).find('div' + area).addClass('active');
                            $(document).find('div#tbp2').removeClass('active');
                        }
                    });

                    $(".fa-facebook-square").on('click', function () {
                        var act = $(this).data('action');
                        FB.ui({
                            method: 'share',
                            href: act,
                        }, function (response) { });
                    });
                }
            }
        });
    });

    $('.btn_prd_detalhes_mob').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            autoSize: false,
            width: "900px",
            height: "493px",
            afterLoad: function () {
                this.wrap.find('.fancybox-inner').css({
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden'
                });
            },
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('.fancybox-wrap .infoprod').scrollbar();

                    $('.adicionar').on('click', function (e) {
                        e.preventDefault();
                        var estoque = 0;
                        var id = $(this).data('id');
                        var campo = $(this);
                        $.ajax({
                            type: "POST",
                            url: caminho + '/Home/GetEstoque/' + id,
                            success: function (ret) {
                                var data = JSON.parse(ret);
                                estoque = eval(data.PRD_QTDE_ESTOQUE);

                                var qty = campo.parent().find('.qty').val();

                                if (!isNaN(qty)) {
                                    qty++;
                                }

                                if (estoque >= qty) {
                                    campo.parent().find('.qty').val(qty);
                                } else {
                                    $dispararAlerta('Atençao: Não há estoque suficiente', 'danger');
                                }
                            }
                        });
                    });

                    $('.remover').on('click', function (e) {
                        e.preventDefault();

                        var qty = $(this).parent().find('.qty').val();

                        if (!isNaN(qty) && qty > 0) {
                            qty--;
                        }

                        $(this).parent().find('.qty').val(qty);
                    });

                    $('.btn_adicionar').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');
                        var qtde = $(this).parent().find('.qty').val();

                        $adicionarProduto(action, id, qtde);
                    });

                    $('.btn_lembrar').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');

                        $lembrarProduto(action, id);
                    });

                    $('.add_favorite').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');
                        var obj = $(this);

                        $adicionarFavorito(action, id, obj);
                    });

                    $('.add_classificacao').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');

                        $adicionarClassificacao(action, id);
                    });

                    $('.tab_prd_info').on('click', function (e) {
                        e.preventDefault();

                        var area = $(this).attr('href');

                        if (area == '#tbp2') {
                            $(document).find('div' + area).addClass('active');
                            $(document).find('div#tbp1').removeClass('active');
                        } else {
                            $(document).find('div' + area).addClass('active');
                            $(document).find('div#tbp2').removeClass('active');
                        }
                    });

                    $(".fa-facebook-square").on('click', function () {
                        var act = $(this).data('action');
                        FB.ui({
                            method: 'share',
                            href: act,
                        }, function (response) { });
                    });
                }
            }
        });
    });

    $('.btn_prd_parceiro').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "525px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {
                    $('.infoprod').scrollbar();
                }
            }
        });
    });

    $('.btn_lembrar').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');

        $lembrarProduto(action, id);
    });

    $('.btn_adicionar').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');
        var qtde = $(this).parent().parent().parent().find('.qty').val();

        $adicionarProduto(action, id, qtde);
    });

    $('.add_favorite').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');
        var obj = $(this);

        $adicionarFavorito(action, id, obj);
    });

    $('.add_classificacao').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');

        $adicionarClassificacao(action, id);
    });

    $(".fa-facebook-square").on('click', function () {
        var act = $(this).data('action');
        FB.ui({
            method: 'share',
            href: act,
        }, function (response) { });
    });


    $(".btn_whatsapp").on('click', function (e) {
        e.preventDefault();
        var action = $(this).data('action');

        $.fancybox.open({
            href: caminho + '/Home/ContatoWhats',
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "290px",
            height: "320px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('.form_submit').on('click', function (e) {

                        var form = $(this).closest('form');
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(),
                            success: function (ret) {
                                var data = JSON.parse(ret);

                                if (data.O_COD_RETORNO == 0) {
                                    $.fancybox.close();
                                } else {
                                    $dispararAlerta('Atençao: ' + data.O_COD_RETORNO, 'danger');
                                }
                            },
                            complete: function () {
                                window.open(action)
                            }
                        });

                        e.preventDefault();
                    });
                }
            }
        });

    });

    $informarEmail = function (id) {
        $.fancybox.open({
            href: caminho + '/Home/InformarEmail/' + id,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "290px",
            height: "180px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('#form_login').on('submit', function (e) {

                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(),
                            success: function (ret) {
                                var data = JSON.parse(ret);

                                if (data[0].O_COD_RETORNO == 0) {
                                    $dispararAlerta('Obrigado!! Assim que o produto estiver em estoque lhe enviaremos um e-mail com as informações!', 'success');
                                }
                                $.fancybox.close();
                            }
                        });

                        e.preventDefault();
                    });
                }
            }
        });
    };

    $adicionarProduto = function (action, id, qtde) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id,
                qtde: qtde
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {
                    $("span#count_carrinho").html(data[0].V_TOTAL_QTDE);
                    //$("a#total_carrinho").html(data[0].V_TOTAL_COMPRA);

                    $dispararAlerta('Produto adicionado no carrinho', 'success');
                } else {
                    $dispararAlerta('Erro: ' + data[0].O_COD_RETORNO + '-' + data[0].O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $lembrarProduto = function (action, id) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {
                    $dispararAlerta('Obrigado!! Assim que o produto estiver em estoque lhe enviaremos um e-mail com as informações!', 'success');
                } else {
                    $informarEmail(id);
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $adicionarFavorito = function (action, id, obj) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {
                    obj.addClass('my_favorite');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $adicionarClassificacao = function (action, id) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {

                    $('.add_classificacao').each(function () {
                        $(this).removeClass("classficado");

                        if ($(this).data("class") <= data[0].I_NOTA) {
                            $(this).addClass("classficado");
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $dispararAlerta = function (msg, tipo) {

        var dialog = bootbox.dialog({
            message: "<h4 class='" + tipo + "'>" + msg + "</h4>",
            closeButton: true,
            onEscape: true,
            backdrop: false
        });

        if (tipo == 'success') {
            dialog.find('.modal-content').css('background-color', '#a3e1a3');
        } else if (tipo == 'warning') {
            dialog.find('.modal-content').css('background-color', 'rgb(243, 210, 115)');
        } else if (tipo == 'danger') {
            dialog.find('.modal-content').css('background-color', 'rgb(225, 163, 163)');
        }

        dialog.init(function () {
            setTimeout(function () {
                dialog.modal('hide');
            }, 4000);
        });
    };

    $dispararNotificacao = function (msg, tipo, tempo, op = null) {

        if (op == 1) {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: tempo,
                pos: 'top-right',
                onClose: function () {
                    location.reload();
                }
            });
        } else {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: tempo,
                pos: 'top-right'
            });
        }

        return false;
    };

    //wizard
    $validarCheackoutMensagens = function (form, $tab) {
        var formapag = null;
        var opcart = null;
        var retorno = [];
        var mensagens = [];
        mensagens['cep_frete'] = "Selecione o endereço de destino";
        mensagens['tipo_entrega'] = "Selecione uma opção de entrega";
        mensagens['meio'] = "Selecione uma forma de pagamento";
        mensagens['formaspagamento'] = "Selecione uma forma de pagamento";
        mensagens['banco'] = "Selecione um banco";
        mensagens['opcart'] = "Selecione um cartão ou informe um novo";
        mensagens['titular'] = "Informe o nome do titular do cartão";
        mensagens['cpftitular'] = "Informe o cpf/cnpj do titular do cartão";
        mensagens['numerocartao'] = "Informe o número do cartão";
        mensagens['cvv'] = "informe o CVV do cartão";
        mensagens['validadecartao'] = "Informe a validade do cartão";
        mensagens['bandeira'] = "Não foi possível obter a bandeira do cartão";
        mensagens['valorPgto'] = "Não foi possível obter total da compra";
        mensagens['parcs'] = "Informe o número de parcelas";
        mensagens['hashComprador'] = "Não foi possível obter o hash do cliente";
        mensagens['hashcartao'] = "Não foi possível obter o hash do cartão informado";
        mensagens['valorparcela'] = "Não foi possível obter o valor da parcela";

        var msg = "";
        var count = 0;

        var form = $("form#" + form.attr('id')).serializeArray();

        $.each(form, function (i, field) {
            if ($tab == 3) {
                if (field.name == 'cep_frete') {
                    if ($isEmpty(field.value)) {
                        msg = msg + mensagens[field.name] + "<br/>";
                        count = count + 1;
                    }
                } else if (field.name == 'tipo_entrega') {
                    if ($isEmpty(field.value)) {
                        msg = msg + mensagens[field.name] + "<br/>";
                        count = count + 1;
                    }
                }
            }

            if ($tab == 4) {
                if (field.name == 'formaspagamento') {
                    if ($isEmpty(field.value)) {
                        msg = msg + mensagens[field.name] + "<br/>";
                        count = count + 1;
                    } else {
                        formapag = field.value;
                    }
                } else {

                    if (formapag == 'ONLINE_DEBIT') {

                        if (field.name == 'banco') {
                            if ($isEmpty(field.value)) {
                                msg = msg + mensagens[field.name] + "<br/>";
                                count = count + 1;
                            }
                        }

                    } else if (formapag == 'CREDIT_CARD') {
                        if (field.name == 'opcart') {
                            if ($isEmpty(field.value)) {
                                msg = msg + mensagens[field.name] + "<br/>";
                                count = count + 1;
                            } else {
                                opcart = field.value
                            }
                        } else {
                            if (field.name == 'titular') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            } else if (field.name == 'cpftitular') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            } else if (field.name == 'numerocartao') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            } else if (field.name == 'cvv') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            } else if (field.name == 'validadecartao') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            } else if (field.name == 'parcs') {
                                if ($isEmpty(field.value)) {
                                    msg = msg + mensagens[field.name] + "<br/>";
                                    count = count + 1;
                                }
                            }
                        }
                    }
                }
            }
        });

        retorno['msg'] = msg;
        retorno['result'] = count;

        return retorno;
    };

    var checkFormWizardValidaion = function (tab, navigation, index) {

        if ($("#rootwizard").find("form").hasClass('validate')) {

            var $form = $("#rootwizard").find("form");

            var $ret = $validarCheackoutMensagens($form, index);

            if ($ret['result'] > 0) {
                $dispararAlerta($ret['msg'], 'warning');
                return false;
            }
        }

        if (index == 4) {
            if ($('#formaspagamento').val() == 'CREDIT_CARD') {
                setCredtCardToken();
            }
        }

        return true;
    };

    if ($("#rootwizard").length) {
        $("#rootwizard").bootstrapWizard({
            tabClass: "itens",
            onTabShow: function ($tab, $navigation, index) {
                setCurrentProgressTab($navigation, $tab, index);
            },
            onNext: checkFormWizardValidaion,
            onTabClick: checkFormWizardValidaion
        });
    }

    function setCurrentProgressTab($nav, $tab, index) {

        switch (index) {
            case 0:
                $('ul.pager.wizard li.previous').addClass('hidden');
                $('ul.pager.wizard li.next').removeClass('hidden');
                break;
            case 1:
                $('ul.pager.wizard li.previous').removeClass('hidden');
                $('ul.pager.wizard li.next').removeClass('hidden');
                break;
            case 2:
                $('ul.pager.wizard li.previous').addClass('hidden');
                $('ul.pager.wizard li.next').removeClass('hidden');
                break;
            case 3:
                $('ul.pager.wizard li.previous').removeClass('hidden');
                $('ul.pager.wizard li.next').removeClass('hidden');
                break;
            case 4:
                $('ul.pager.wizard li.previous').removeClass('hidden');
                $('ul.pager.wizard li.next').addClass('hidden');
                break;
        }

        var $progress = $("#rootwizard").find(".steps-progress div");

        $tab.prevAll().addClass('completed');
        $tab.nextAll().removeClass('completed');

        var items = $nav.children().length;
        var pct = parseInt((index + 1) / items * 100, 10);
        var $first_tab = $nav.find('li:first-child');
        var margin = (1 / (items * 2) * 100) + '%';

        if ($first_tab.hasClass('active')) {
            $progress.width(0);
        } else {
            if (rtl()) {
                $progress.width($progress.parent().outerWidth(true) - $tab.prev().position().left - $tab.find('span').width() / 2);
            } else {
                $progress.width(((index - 1) / (items - 1)) * 100 + '%');
            }
        }

        $progress.parent().css({
            marginLeft: margin,
            marginRight: margin
        });
    };

    function rtl() {
        if (typeof window.isRTL == 'boolean')
            return window.isRTL;

        window.isRTL = jQuery("html").get(0).dir == 'rtl' ? true : false;

        return window.isRTL;
    }

    $("a.link-inativo").on("click", function (e) {
        e.preventDefault();
    });

    function setCredtCardToken() {
        let card = getCardValues();

        PagSeguroDirectPayment.createCardToken({
            cardNumber: card.cardNumber,
            brand: card.brand,
            cvv: card.cvv,
            expirationMonth: card.expirationMonth,
            expirationYear: card.expirationYear,
            success: function (response) {
                $("input#hashcartao").val(response.card.token);
            },
            error: function (response) {
                console.log(response);
                $dispararAlerta("Ocorreu algum problema com o seu cartão, entre em contato com a administradora", 'warning');
            }
        });
    }

    function getCardValues() {
        let card = {};

        var parcelas = $($('#parcs option:selected')).val().split(':');

        card.cardNumber = $('input[name="numerocartao"]').val();
        card.cardNumber = card.cardNumber.replace(/\D/g, '');
        card.brand = $('input[name="bandeira"]').val();
        card.cvv = $('input[name="cvv"]').val();
        card.expiration = $('input[name="validadecartao"]').val();
        card.installments = parcelas[0];

        if (card.expiration) {
            let split = card.expiration.toString().split("/");
            card.expirationMonth = split[0];
            card.expirationYear = split[1];
        }

        return card;
    }
});
