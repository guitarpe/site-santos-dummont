jQuery(document).ready(function ($) {

    $('.btn_remover').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');

        $(this).closest('tr').remove();

        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {
                    return $dispararAlerta('Produto removido dos favoritos', 'success');
                } else {
                    return $dispararAlerta('Erro: ' + data[0].O_COD_RETORNO + '-' + data[0].O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });
});