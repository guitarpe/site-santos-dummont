jQuery(document).ready(function ($) {

    $('.img-pag').click(function () {
        $('.meio_pag').val($(this).attr('alt'));
    });

    $(".op_outro_cartao :input").attr("disabled", true);
    $(".op_cartoes_usuarios :input").attr("disabled", true);
    $(".op_slct_outro_cartao :input").attr("disabled", true);

    $("#slct-bancos").addClass('hidden');

    $('.op_cartao').on('change', function () {

        $(".op_outro_cartao :input").val("");
        $('#parcs').html("");
        $('#parcs').attr("disabled", true);

        $('.bandeira-cartao').html("");
        $("input#bandeira").val("");

        if ($(this).val() == 1) {
            $(".op_outro_cartao :input").attr("disabled", true);
            $(".op_cartoes_usuarios :input").attr("disabled", false);
            $(".op_cartoes_usuarios").removeClass('hidden');
        } else {
            $(".op_outro_cartao :input:not(.slct)").attr("disabled", false);
            $(".op_cartoes_usuarios :input").attr("disabled", true);
            $(".op_cartoes_usuarios").addClass('hidden');
        }
    });

    $("#slct-bancos").removeClass('hidden');

    $('.img_banco').on('click', function () {
        var info = $(this).data('value');

        $('.img_banco').removeClass('img-banco-selected');
        $('.img_banco').addClass('img-banco');

        if ($(this).hasClass('img-banco')) {
            $(this).removeClass('img-banco');
            $(this).addClass('img-banco-selected');
        }
        $('#banco').val(info);
    })

    $('#formaspagamento').on('change', function (ev) {

        $(".op_cartao").val("");

        if ($(this).val() == 'CREDIT_CARD') {
            $(".op_slct_outro_cartao").removeClass('hidden');
            $(".op_cartoes_usuarios").addClass('hidden');
            $(".op_outro_cartao").removeClass('hidden');
            $('.op_cartao').attr("disabled", false);
            $("#slct-bancos").addClass('hidden');
            $("#slct-bancos").prop('required', false);

            $(".op_outro_cartao :input").each(function () {
                $(this).prop('required', true);
            });

            $('#parcs').attr("disabled", true);
            $(".op_cartao").prop('required', true);

        } else if ($(this).val() == 'ONLINE_DEBIT') {
            $(".op_slct_outro_cartao").addClass('hidden');
            $(".op_cartoes_usuarios").addClass('hidden');
            $(".op_outro_cartao").addClass('hidden');
            $('.op_cartao').attr("disabled", true);
            $("#slct-bancos").removeClass('hidden');
            $("#slct-bancos").prop('required', true);

            $(".op_outro_cartao :input").each(function () {
                $(this).prop('required', false);
            });

            $(".op_cartao").prop('required', false);

            $('.img_banco').removeClass('img-banco-selected');
            $('.img_banco').addClass('img-banco');
            $('.img_banco').first().addClass('img-banco-selected');
            $('#banco').val('BRADESCO');

        } else {
            $(".op_slct_outro_cartao").addClass('hidden');
            $(".op_cartoes_usuarios").addClass('hidden');
            $(".op_outro_cartao").addClass('hidden');
            $('.op_cartao').attr("disabled", true);
            $("#slct-bancos").addClass('hidden');
            $("#slct-bancos").prop('required', false);

            $(".op_outro_cartao :input").each(function () {
                $(this).prop('required', false);
            });

            $(".op_cartao").prop('required', false);
        }
    });

    $("label.frm_op_cielo").on("click", function () {

        $('label.frm_op_cielo').each(function () {
            $(this).removeClass("frm_selecionado_cielo");
        });

        $("#op_info_cartao").removeClass('hidden');

        $(this).addClass("frm_selecionado_cielo");
    });

    if ($("select#cep_frete").length > 0) {
        var id = $("#cep_frete option:selected").data('id');

        $.ajax({
            url: caminho + '/Usuarios/GetEndereco',
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {
                var data = JSON.parse(ret);
                if (data.TIPO == 1) {
                    $("#titulo_end").html("ENDEREÇO DE RETIRADA");
                } else {
                    $("#titulo_end").html("ENDEREÇO DE ENTREGA");
                }
                $("#set_endereco").html(data.DADOS.END_ENDERECO + ", " + data.DADOS.END_NUM + " " + data.DADOS.END_COMPLEMENTO + " - " + data.DADOS.END_BAIRRO + " - " + data.DADOS.END_CIDADE + "-" + data.DADOS.END_UF + "<br/>CEP:" + data.DADOS.END_CEP);
            }
        });
    }

    $("select#cep_frete").on('change', function () {
        var id = $(this).data('id');

        $.ajax({
            url: caminho + '/Usuarios/GetEndereco',
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);
                if (data.TIPO == 1) {
                    $("#titulo_end").html("ENDEREÇO DE RETIRADA");
                } else {
                    $("#titulo_end").html("ENDEREÇO DE ENTREGA");
                }
                $("#set_endereco").html(data.DADOS.END_ENDERECO + ", " + data.DADOS.END_NUM + " " + data.DADOS.END_COMPLEMENTO + " - " + data.DADOS.END_BAIRRO + " - " + data.DADOS.END_CIDADE + "-" + data.DADOS.END_UF + "<br/>CEP:" + data.DADOS.END_CEP);
            }
        });
    });

    $("label.frm_entrega").on("click", function () {

        var forvalue = $(this).attr('for');

        $("label.frm_entrega").each(function () {
            $(this).removeClass("frm_selecionado_ent");
        });

        $(this).addClass("frm_selecionado_ent");
        $(".tipo_entr").addClass('grayscale');
        $('.' + forvalue).removeClass("grayscale");

        var codigo = $(this).data('codigo');
        $("#tipo_entrega_hide").val(codigo);
    });

    $retornaDadosFrete = function (codigo) {
        $.ajax({
            url: caminho + '/Usuarios/DadosOpEntrega/' + codigo,
            type: "POST",
            success: function (data) {
                var ret = JSON.parse(data);

                $("#img_op_entrega").attr('src', ret.imagem);
                $("#frete_op_entrega").html(ret.frete);
                $("#prazo_op_entrega").html(ret.prazo);
                $("#total_frete_op_entrega").html(ret.frete);
                $("#total_pedido_op_entrega").html(ret.total);

            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $(".rem_qtde_prod").on("click", function () {

        var qtde = $(this).closest('td').find(".qty").val();

        if (!isNaN(qtde) && qtde > 0) {
            qtde--;
        }

        var action = $(this).data('action');
        var id = $(this).data('id');
        var tr = $(this).closest('tr');
        $(this).closest('td').find(".qty").val(qtde);

        var obj = $("input[name='tipo_entrega']:checked").closest('li');
        $addRemQtdePedido(action, id, tr, qtde, obj);
    });

    $(".add_qtde_prod").on("click", function () {

        var campo = $(this);
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            url: caminho + '/Home/GetEstoque/' + id,
            success: function (ret) {

                var data = JSON.parse(ret);
                estoque = data.PRD_QTDE_ESTOQUE;

                var qty = campo.closest('td').find(".qty").val();

                if (!isNaN(qty)) {
                    qty++;
                }

                if (estoque >= qty) {

                    campo.closest('td').find(".qty").val(qty);

                    var action = campo.data('action');
                    var id = campo.data('id');
                    var tr = campo.closest('tr');

                    var obj = $("input[name='tipo_entrega']:checked").closest('li');

                    $addRemQtdePedido(action, id, tr, qty, obj);
                } else {
                    $dispararAlerta('Atençao: Não há estoque suficiente', 'danger');
                }
            }
        });
    });

    $('.btn_item_carrinho').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');
        var refresh = $(this).data('refresh');
        var tr = $(this).closest('tr');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $removerdoPedido(action, id, tr, refresh);
                }
            }
        });
    });

    $('.btn_remover').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var id = $(this).data('id');
        var refresh = $(this).data('refresh');
        var tr = $(this).closest('tr');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $removerdoPedido(action, id, tr, refresh);
                }
            }
        });
    });

    $('#btn_cancela_pedido').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var refresh = $(this).data('refresh');

        bootbox.confirm({
            message: "<h3>Deseja realmente cancelar o pedido?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        success: function (data) {
                            window.location.href = refresh;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $('.calcula_frete').click(function () {

        var form = $(this).closest('form').attr("id");
        var action = $(this).data('action');
        var calc = $(this).data('calcfrete');
        var obj = $(this).closest('li');

        if ($("select#cep_frete").length > 0) {
            var cep = $("form#" + form).find("select#cep_frete").val();
        } else {
            var cep = $("form#" + form).find("input#cep_frete").val();
        }

        if ((cep == null || cep == "") && calc == 1) {
            $dispararAlerta("Atenção: Informe um cep", "warning");
        } else {
            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url, function (data) {
                if (!("erro" in data)) {
                    $calculaFrete(form, action, obj);
                } else {
                    $(".validcep").val("");
                    $dispararAlerta("Atenção: CEP Inválido", "warning");
                }
            });
        }
    });

    $calculaFrete = function (form, action, obj) {
        var msg = "Verifique os campos<br/>";

        var cont = 0;
        var check = 0;

        $("form#" + form + " input").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {
                var input = $(this).attr('name');

                if (input == "cep_frete" || input == 'tipo_entrega') {
                    if ($(this).val() == "" && input == 'cep_frete') {
                        var nomecampo = $(this).data('name');
                        msg += nomecampo + '<br/>';

                        cont++;
                    }

                    if (input == 'tipo_entrega') {
                        $.each($("input[name='tipo_entrega']:checked"), function () {
                            check++;
                        });
                    }
                }
            }
        });

        if (check == 0) {
            msg += 'Tipo de Entrega' + '<br/>';
            cont++;
        }

        if (cont > 0) {
            return $dispararAlerta('Atenção: ' + msg, 'warning');
        } else {

            $('.carr_prods').waitMe({
                effect: 'bounce',
                text: 'Calculando Frete',
                bg: '#33333354',
                color: '#e77817',
                maxSize: '',
                waitTime: -1,
                textPos: 'vertical',
                fontSize: '',
                source: '',
                onClose: function () { }
            });

            var tipo = $("form#" + form).find("input[name='tipo_entrega']:checked").val();
            if ($("select#cep_frete").length > 0) {
                var cep = $("form#" + form).find("select#cep_frete").val();
            } else {
                var cep = $("form#" + form).find("input#cep_frete").val();
            }
            var qtde = $("#qty").val();

            $.ajax({
                url: action,
                type: "POST",
                data: {
                    tipo_entrega: tipo,
                    cep_frete: cep,
                    qtde: qtde
                },
                success: function (ret) {

                    var data = JSON.parse(ret);

                    $(".frete_total").html("");
                    $(".przo_pedido").html("");

                    obj.find(".frete_total").html("R$ " + data.FRETE);

                    if (eval(data.PRAZO) === 0) {
                        if (data.O_COD_RETORNO != 0) {
                            obj.find(".przo_pedido").html('Não atende');
                        } else {
                            obj.find(".przo_pedido").html('');
                        }
                    } else {
                        if (eval(data.PRAZO) > 1) {
                            obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dias úteis');
                        } else {
                            obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dia útil');
                        }
                    }

                    if (data.O_COD_RETORNO != 0) {
                        $("#msg_retorno").html(data.MSG);
                    } else {
                        $("#msg_retorno").html("");
                    }
                    $("#total_produtos").html("R$ " + data.PRECO_TOTAL);
                    $("#total_pedido").html("R$ " + data.TOTAL_PEDIDO);
                    $("#valorPgto").val(data.TOTAL_PEDIDO);
                    $('.carr_prods').waitMe("hide");

                    if ($("#formaspagamento").length) {
                        $("#formaspagamento").removeAttr('disabled');
                    }

                    $(".frmcheckout :input").each(function () {
                        if ($(this).is(':disabled')) {
                            if ($(this).attr('id') !== 'btn_cancela_pedido' && $(this).attr('id') !== 'formaspagamento') {
                                $(this).prop('disabled', false);
                            }
                        }
                    });

                    $retornaDadosFrete(tipo);
                }
            });
        }
    };

    /*$calculaFreteRetiraLoja = function (form, action, tipo, obj) {

        $('.carr_prods').waitMe({
            effect: 'bounce',
            text: 'Calculando Frete',
            bg: '#33333354',
            color: '#e77817',
            maxSize: '',
            waitTime: -1,
            textPos: 'vertical',
            fontSize: '',
            source: '',
            onClose: function () { }
        });

        if ($("select#cep_frete").length > 0) {
            var cep = $("form#" + form).find("select#cep_frete").val();
        } else {
            var cep = $("form#" + form).find("input#cep_frete").val();
        }

        $.ajax({
            url: action,
            type: "POST",
            data: {
                tipo_entrega: tipo,
                cep_frete: cep
            },
            success: function (ret) {

                var data = JSON.parse(ret);
                $(".frete_total").html("");
                $(".przo_pedido").html("");

                obj.find(".frete_total").html("R$ " + data.FRETE);

                if (eval(data.PRAZO) === 0) {
                    if (data.O_COD_RETORNO != 0) {
                        obj.find(".przo_pedido").html('Não atende');
                    } else {
                        obj.find(".przo_pedido").html('');
                    }
                } else {
                    if (eval(data.PRAZO) > 1) {
                        obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dias úteis');
                    } else {
                        obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dia útil');
                    }
                }

                $("#total_pedido").html("R$ " + data.TOTAL_PEDIDO);
                $("#total_produtos").html("R$ " + data.PRECO_TOTAL);
                $("#valorPgto").val(data.TOTAL_PEDIDO);
                if (data.O_COD_RETORNO != 0) {
                    $("#msg_retorno").html(data.MSG);
                } else {
                    $("#msg_retorno").html("");
                }
                $('.carr_prods').waitMe("hide");

                if ($("#formaspagamento").length) {
                    $("#formaspagamento").removeAttr('disabled');
                }

                $(".frmcheckout :input").each(function () {
                    if ($(this).is(':disabled')) {
                        if ($(this).attr('id') !== 'btn_cancela_pedido' && $(this).attr('id') !== 'formaspagamento') {
                            $(this).prop('disabled', false);
                        }
                    }
                });
            }
        });
    };*/

    $atualizarPedido = function (action) {

        if ($("select#cep_frete").length > 0) {
            var cep = $("select#cep_frete").val();
        } else {
            var cep = $("input#cep_frete").val();
        }
        var tipo = $("input[name='tipo_entrega']:checked").val();
        var obj = $("input[name='tipo_entrega']:checked").closest('li');

        $.ajax({
            url: action,
            type: "POST",
            data: {
                cep_frete: cep,
                tipo_entrega: tipo
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                obj.find(".frete_total").html("R$ " + data.FRETE);

                if (data.O_COD_RETORNO != 0) {
                    obj.find(".przo_pedido").html('Não atende');
                } else {
                    obj.find(".przo_pedido").html('');
                }

                if (eval(data.PRAZO) > 1) {
                    obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dias úteis');
                } else {
                    obj.find(".przo_pedido").html('Em até ' + data.PRAZO + ' dia útil');
                }

                $("#total_produtos").html("R$ " + data.PRECO_TOTAL);
                $("#total_pedido").html("R$ " + data.TOTAL_PEDIDO);
                $("#valorPgto").val(data.TOTAL_PEDIDO);
                $("#msg_retorno").html(data.MSG);
                $('.carr_prods').waitMe("hide");
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $removerdoPedido = function (action, id, tr, refresh) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {

                    tr.remove();
                    $atualizarPedido(refresh);

                    $("span#count_carrinho").html(data[0].V_TOTAL_QTDE);
                    $("a#total_carrinho").html(data[0].V_TOTAL_COMPRA);

                    return $dispararAlerta('Produto removido do pedido', 'success');

                } else if (data[0].O_COD_RETORNO == 13) {
                    location.reload();
                } else {
                    $("span#count_carrinho").html(data[0].V_TOTAL_QTDE);
                    $("a#total_carrinho").html(data[0].V_TOTAL_COMPRA);

                    return $dispararAlerta('Erro: ' + data[0].O_COD_RETORNO + '-' + data[0].O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $addRemQtdePedido = function (action, id, tr, qtde, obj) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id,
                qtde: qtde
            },
            success: function (ret) {
                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    tr.find(".prec_produto").html("R$ " + data.O_PRECO_PRODUTO);
                    tr.find(".prec_total_produto").html("R$ " + data.O_PRECO_TOTAL);

                    obj.find(".frete_total").html("R$ " + data.O_FRETE);

                    if (data.O_COD_RETORNO != 0) {
                        obj.find(".przo_pedido").html('Não atende');
                    } else {
                        obj.find(".przo_pedido").html('');
                    }

                    if (eval(data.O_PRAZO) > 1) {
                        obj.find(".przo_pedido").html('Em até ' + data.O_PRAZO + ' dias úteis');
                    } else {
                        obj.find(".przo_pedido").html('Em até ' + data.O_PRAZO + ' dia útil');
                    }

                    $("#total_produtos").html("R$ " + data.O_PRECO_TOTAL);
                    $("#total_pedido").html("R$ " + data.O_PRECO_TOTAL_PEDIDO);
                    $("#valorPgto").val(data.O_PRECO_TOTAL_PEDIDO);
                    $("#msg_retorno").html(data.O_MSG);

                    $("a#total_carrinho").html("R$ " + data.O_PRECO_TOTAL_PEDIDO);
                    $("span#count_carrinho").html(qtde);
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data[0].O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });

        return 1;
    };

    /*METODOS E FUNÇÕES PARA O CHECKOUT TRANSPARENTE*/
    function getCardValues() {
        let card = {};

        var parcelas = $($('#parcs option:selected')).val().split(':');

        card.cardNumber = $('input[name="numerocartao"]').val();
        card.cardNumber = card.cardNumber.replace(/\D/g, '');
        card.brand = $('input[name="bandeira"]').val();
        card.cvv = $('input[name="cvv"]').val();
        card.expiration = $('input[name="validadecartao"]').val();
        card.installments = parcelas[0];

        if (card.expiration) {
            let split = card.expiration.toString().split("/");
            card.expirationMonth = split[0];
            card.expirationYear = split[1];
        }

        return card;
    }

    function getBrandCartao() {
        PagSeguroDirectPayment.getBrand({
            cardBin: $("#numerocartao").val().replace(/[^0-9\.]/g, ''),
            success: function (response) {
                if (response && response.brand && response.brand.name) {
                    var imgBand = response.brand.name;
                    $('.bandeira-cartao').html("<img src='https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" + imgBand + ".png'>");
                    $("input#bandeira").attr("value", imgBand);
                    $("input#bandeiraimg").val("https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" + imgBand + ".png");
                }
            },
            error: function (response) {
                $('.bandeira-cartao').empty();
                $("input#bandeira").val("");
                $("input#bandeiraimg").val("");
                $(this).val("");
                $dispararAlerta('Atenção: Cartão inválido', 'warning');
            }
        });
    }

    function getBrandCartaoLogado() {
        PagSeguroDirectPayment.getBrand({
            cardBin: $("#numerocartao").val().replace(/[^0-9\.]/g, ''),
            success: function (response) {
                if (response && response.brand && response.brand.name) {
                    var imgBand = response.brand.name;
                    $('.bandeira-cartao').html("<img src='https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" + imgBand + ".png'>");
                    $("input#bandeira").attr("value", imgBand);
                    $("input#bandeiraimg").val("https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/" + imgBand + ".png");

                    var total = $("input#valorPgto").val();

                    $setarBandeiraParcelas(imgBand, total);
                }
            },
            error: function (response) {
                $('.bandeira-cartao').empty();
                $("input#bandeira").val("");
                $("input#bandeiraimg").val("");
                $(this).val("");
                $dispararAlerta('Atenção: Cartão inválido', 'warning');
            }
        });
    }

    function setCredtCardToken() {
        let card = getCardValues();

        PagSeguroDirectPayment.createCardToken({
            cardNumber: card.cardNumber,
            brand: card.brand,
            cvv: card.cvv,
            expirationMonth: card.expirationMonth,
            expirationYear: card.expirationYear,
            success: function (response) {
                $("input#hashcartao").val(response.card.token);
            },
            error: function (response) {
                console.log(response);
                $dispararAlerta("Ocorreu algum problema com o seu cartão, entre em contato com a administradora", 'warning');
            }
        });
    }

    $('.op_cartaosclt').on('change', function () {
        $.ajax({
            url: caminho + '/Usuarios/retornaDadoscartao',
            type: "POST",
            data: {
                cartid: $(this).val(),
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                $("#titular").val(data.CART_NOME);
                $("#numerocartao").val(data.CART_NUMERO);
                $("#cvv").val(data.CART_CCV);
                $("#validadecartao").val(data.CART_VALIDADE);
                $("#cpftitular").val(data.CART_CPF);

                $("#numerocartao").unmask().mask("0000-0000-0000-0000");
                $("#cvv").unmask().mask("000");

                $(".op_outro_cartao input[type='hidden']").attr("disabled", false);

                getBrandCartaoLogado();
                setCredtCardToken();
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });

        var valor = $($('#parcs')).val().split(':');

        $("input#parcelas").val(valor[0]);
        $("input#valorparcela").val(valor[1]);
    });

    $('#numerocartao').on('keyup', function () {
        var numCartao = $(this).val().replace(/[^0-9\.]/g, '');
        var qntNumero = numCartao.length;

        if (qntNumero == 6) {
            $(".op_outro_cartao input[type='hidden']").attr("disabled", false);

            getBrandCartao();
        }
    });

    $('#numerocartao').on('blur', function () {
        var numCartao = $(this).val().replace(/[^0-9\.]/g, '');
        var qntNumero = numCartao.length;

        if (qntNumero == 16) {

            var bandeira = $("input#bandeira").val();

            var total = $("input#valorPgto").val();
            $setarBandeiraParcelas(bandeira, total);
        }
    });

    $("#comentario").keyup(function () {
        var max = 400;
        var len = $(this).val().length;
        if (len >= max) {
            $('#contador').text(' Você atingiu o limite');
        } else {
            var char = max - len;
            $('#contador').text(char + ' caracteres restantes');
        }
    });

    if ($("select#cep_frete").length > 0) {
        PagSeguroDirectPayment.getPaymentMethods({
            success: function (response) {

                $('#formaspagamento').html("");

                if (response.error == false) {
                    var data = response.paymentMethods;

                    $.each(data, function (key, item) {
                        if ($opcaoPagamento(item.name) != null && $opcaoPagamento(item.name) != "") {
                            $('#formaspagamento').append($('<option>', { value: item.name, text: $opcaoPagamento(item.name) }));
                        }
                    });

                    $('#formaspagamento').val($("#formaspagamento option:first").val());
                }
            },
            error: function (response) {
                console.log(response);
                $('#formaspagamento').html("");
                $('#formaspagamento').append('<option value="">Selecione</option>');
            }
        });
    }

    $('#parcs').on('change', function () {
        var valor = $(this).val().split(':');

        $("input#parcelas").val(valor[0]);
        $("input#valorparcela").val(valor[1]);
    });

    $("#btn_confirma_pedido").on('click', function (e) {
        e.preventDefault();

        var mensagens = [];
        mensagens['hashComprador'] = "Não foi possível obter o hash do cliente";
        mensagens['hashcartao'] = "Não foi possível obter o hash do cartão informado";
        mensagens['valorparcela'] = "Não foi possível obter o valor da parcela";
        mensagens['bandeira'] = "Não foi possível obter a bandeira do cartão";
        mensagens['valorPgto'] = "Não foi possível obter total da compra";

        var msg = "";
        var count = 0;

        $('input#hashComprador').val(PagSeguroDirectPayment.getSenderHash());

        if ($('#formaspagamento').val() == 'CREDIT_CARD') {
            setCredtCardToken();
        }

        var form = $(this).closest('form').serializeArray();

        $.each(form, function (i, field) {
            if (field.name == 'formaspagamento') {
                formapag = field.value;

                if (field.value == 'CREDIT_CARD') {
                    setCredtCardToken();
                }
            } else {
                if ($('#formaspagamento').val() == 'CREDIT_CARD') {
                    if (field.name == 'hashcartao') {
                        if ($isEmpty(field.value)) {
                            msg = msg + mensagens[field.name] + "<br/>";
                            count = count + 1;
                        }
                    }
                }

                if (field.name == 'hashComprador') {
                    if ($isEmpty(field.value)) {
                        msg = msg + mensagens[field.name] + "<br/>";
                        count = count + 1;
                    }
                }

                if ($('#formaspagamento').val() == 'CREDIT_CARD') {
                    if (field.name == 'valorparcela') {
                        if ($isEmpty(field.value)) {
                            msg = msg + mensagens[field.name] + "<br/>";
                            count = count + 1;
                        }
                    }
                }

                if ($('#formaspagamento').val() == 'CREDIT_CARD') {
                    if (field.name == 'bandeira') {
                        if ($isEmpty(field.value)) {
                            msg = msg + mensagens[field.name] + "<br/>";
                            count = count + 1;
                        }
                    }
                }

                if (field.name == 'valorPgto') {
                    if ($isEmpty(field.value)) {
                        msg = msg + mensagens[field.name] + "<br/>";
                        count = count + 1;
                    }
                }
            }
        });

        if (count > 0) {
            $dispararAlerta(msg, 'warning');
        } else {
            $(this).closest('form').submit();

            $('.checkout-page').waitMe({
                effect: 'bounce',
                text: 'Finalizando o Pedido',
                bg: '#33333354',
                color: '#e77817',
                maxSize: '',
                waitTime: -1,
                textPos: 'vertical',
                fontSize: '',
                source: '',
                onClose: function () { }
            });
        }
    });

    $setarBandeiraParcelas = function (bandeira, total) {

        $('#parcs').html("");
        var numparsjuros = 2;

        var valtotal = parseFloat(total.replace(".", "").replace(",", ".")).toFixed(2)

        if (valtotal > 100) {
            numparsjuros = 3;
        }

        PagSeguroDirectPayment.getInstallments({
            amount: total.replace(".", "").replace(",", "."),
            brand: bandeira,
            maxInstallmentNoInterest: numparsjuros,
            success: function (response) {
                if (response.error == false) {

                    $('#parcs').attr("disabled", false);

                    var data = response.installments[bandeira];

                    $('#parcs').html("");

                    $.each(data, function (key, item) {

                        var valor = item.installmentAmount;
                        var text = item.quantity + 'x de ' + $mascaraValor(valor.toFixed(2));

                        if (item.interestFree == true) {
                            text = text + ' sem juros';
                        }

                        $('#parcs').append($('<option>', { value: item.quantity + ':' + valor, text: text }));
                    });

                    $('#parcs').val($("#parcs option:first").val());

                    var valor = $("#parcs option:first").val().split(':');

                    $("input#parcelas").val(valor[0]);
                    $("input#valorparcela").val(valor[1]);
                }
            },
            error: function (response) {
                if (response.error == true) {
                    console.log(response);
                    $dispararAlerta(response.errors, 'warning');
                    $('#parcs').html("");
                    $('#parcs').attr("disabled", true);
                }
            }
        });
    };

    $mascaraValor = function (valor) {
        valor = valor.toString().replace(/\D/g, "");
        valor = valor.toString().replace(/(\d)(\d{8})$/, "$1.$2");
        valor = valor.toString().replace(/(\d)(\d{5})$/, "$1.$2");
        valor = valor.toString().replace(/(\d)(\d{2})$/, "$1,$2");

        return valor;
    };

    $opcaoPagamento = function (valor) {
        var opcoes = [];

        if (valor !== 'BALANCE' && valor !== 'DEPOSIT') {
            opcoes['BOLETO'] = "Boleto";
            //opcoes['BALANCE'] = "Saldo PagSeguro";
            opcoes['ONLINE_DEBIT'] = "Débito OnLine";
            opcoes['CREDIT_CARD'] = "Cartão de Crédito";
            //opcoes['DEPOSIT'] = "Depósito";

            return opcoes[valor];
        }
    };
});
