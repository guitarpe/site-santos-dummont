jQuery(document).ready(function ($) {

    $('.infoprod').scrollbar();

    $('.btn_prd_parceiro').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "900px",
            height: "525px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {
                    $('.fancybox-wrap .infoprod').scrollbar();
                }
            }
        });
    });

    $("label.frm_entrega").on("click", function () {

        var forvalue = $(this).attr('for');

        $("label.frm_entrega").each(function () {
            $(this).removeClass("frm_selecionado_ent");
        });

        $(this).addClass("frm_selecionado_ent");
        $(".tipo_entr").addClass('grayscale');
        $('.' + forvalue).removeClass("grayscale");
    });

    $('.calcula_frete').click(function () {

        var form = $(this).closest('form').attr("id");
        var action = $(this).data('action');
        var refresh = $(this).data('refresh');
        var cep = $("form#" + form).find("input[name='cep_frete']").val();
        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        if (cep != '') {
            $.get(url, function (data) {
                if (!("erro" in data)) {
                    $calculaFreteProd(form, action, refresh);
                } else {
                    $(".validcep").val("");
                    $dispararAlerta("Atenção: CEP Inválido", "warning");
                }
            });
        } else {
            $dispararAlerta("Atenção: Informe um Cep", "warning");
        }
    });

    $calculaFreteProd = function (form, action, refresh) {
        var msg = "Verifique os campos<br/>";

        var cont = 0;
        var check = 0;

        $("form#" + form + " input").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {
                var input = $(this).attr('name');

                if (input === "cep_frete" || input === 'tipo_entrega') {
                    if ($(this).val() === "" && input === 'cep_frete') {
                        var nomecampo = $(this).data('name');
                        msg += nomecampo + '<br/>';

                        cont++;
                    }

                    if (input === 'tipo_entrega') {
                        $.each($("input[name='tipo_entrega']:checked"), function () {
                            check++;
                        });
                    }
                }
            }
        });

        if (check == 0) {
            msg += 'Tipo de Entrega' + '<br/>';
            cont++;
        }

        if (cont > 0) {
            return $dispararAlerta('Atenção: ' + msg, 'warning');
        } else {

            $('.carr_prods').waitMe({
                effect: 'bounce',
                text: 'Calculando o Frete',
                bg: '#33333354',
                color: '#e77817',
                maxSize: '',
                waitTime: -1,
                textPos: 'vertical',
                fontSize: '18',
                source: '',
                onClose: function () { }
            });

            var tipo = $("form#" + form).find("input[name='tipo_entrega']:checked").val();
            var cep = $("form#" + form).find("input[name='cep_frete']").val();
            var qtde = $("#qty").val();

            $.ajax({
                url: action,
                type: "POST",
                data: {
                    tipo_entrega: tipo,
                    cep_frete: cep,
                    qtde: qtde
                },
                success: function (ret) {

                    var data = JSON.parse(ret);

                    if (data.O_COD_RETORNO == 0) {
                        $("#frete_total").html("R$ " + data.FRETE);
                        $("#przo_pedido").html(data.PRAZO + ' dias úteis');
                        $("#msg_retorno").html(data.MSG);
                    } else {
                        $("#msg_retorno").html(data.MSG);
                        $("#frete_total").html("R$ " + data.FRETE);
                        $("#przo_pedido").html(data.PRAZO + ' dias úteis');
                    }

                    $('.carr_prods').waitMe("hide");

                    if ($("#formaspagamento").length) {
                        $("#formaspagamento").removeAttr('disabled');
                    }
                }
            });
        }
    };
});
