jQuery(document).ready(function ($) {

    $.ajax({
        url: caminho + '/Home/CarregarModais',
        type: "POST",
        success: function (data) {
            if (data != null && data != "") {
                $.fancybox.open({
                    href: caminho + '/Home/CarregarModais',
                    type: "ajax",
                    scrolling: 'no',
                    padding: 0,
                    autoSize: false,
                    width: "700px",
                    height: "500px"
                });
            }
        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(errorMessage);
        }
    });

    $('#termo_busca').on('keypress', function (e) {
        if (e.which === 13) {
            $("#form_busca_home").submit();
        }
    });

    setTimeout(function () {
        if ($("#form-busca").find('#msg-f2').val() == "") {
            $("#form-busca").find('#msg-f2').css('display', 'block');
        }
    }, 2000);

    $('a.uk-close-cust').on('click', function () {
        $("#form-busca").find('#msg-f2').remove();
    })
});
