jQuery(document).ready(function ($) {
    $('#tipo_doc').on('change', function () {
        if ($(this).val() == 'CPF') {
            $("#lbl_doc").text("CPF");
            $('#cpf').attr("placeholder", "CPF");
            $('#cpf').removeClass('cnpj');
            $('#cpf').addClass('cpf');
            $("input[name='genero']").attr("disabled", false);
        } else {
            $("#lbl_doc").text("CNPJ");
            $('#cpf').attr("placeholder", "CNPJ");
            $('#cpf').removeClass('cpf');
            $('#cpf').addClass('cnpj');
            $("input[name='genero']").attr("disabled", true);
        }
    });

    $('#form_submit_registro').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');
        var email_login_reg = $("#email_login_reg").val();
        var confemail_login_reg = $("#confemail_login_reg").val();

        $.ajax({
            url: action,
            type: "POST",
            data: {
                email_login_reg: email_login_reg,
                confemail_login_reg: confemail_login_reg
            },
            success: function (ret) {
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });
});