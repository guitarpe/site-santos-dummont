$(document).ready(function () {
    $(document).on('click', '.navi-menu', function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

            var rel = $(this).attr("rel");
            if (rel != "") {
                $("#" + rel).click();
            }

            var target = $(this.hash);

            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.ver_carrinho').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "400px",
            height: "600px",
            minHeight: "100px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {

                    $('.enviar_para_loja').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');

                        $.fancybox.open({
                            href: action,
                            type: "ajax",
                            scrolling: 'no',
                            autoSize: false,
                            width: "400px",
                            height: "auto",
                            minHeight: "100px",
                            ajax: {
                                type: "POST",
                                complete: function (jqXHR, textStatus) {
                                }
                            }
                        });
                    });

                    $('.btn_item_carrinho').on('click', function (e) {
                        e.preventDefault();

                        var action = $(this).data('action');
                        var id = $(this).data('id');
                        var refresh = $(this).data('refresh');
                        var tr = $(this).closest('tr');

                        bootbox.confirm({
                            message: "<h3>Deseja realmente remover o registro?</h3>",
                            buttons: {
                                confirm: {
                                    label: 'Sim',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'Não',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result == true) {
                                    $removerdoPedido(action, id, tr, refresh);
                                }
                            }
                        });
                    });
                }
            }
        });
    });

    $removerdoPedido = function (action, id, tr, refresh) {
        $.ajax({
            url: action,
            type: "POST",
            data: {
                id: id
            },
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data[0].O_COD_RETORNO == 0) {

                    tr.remove();
                    $atualizarPedido(refresh);

                    $("span#count_carrinho").html(data[0].V_TOTAL_QTDE);
                    $("a#total_carrinho").html(data[0].V_TOTAL_COMPRA);

                    return $dispararAlerta('Produto removido do pedido', 'success');

                } else if (data[0].O_COD_RETORNO == 13) {
                    location.reload();
                } else {
                    $("span#count_carrinho").html(data[0].V_TOTAL_QTDE);
                    $("a#total_carrinho").html(data[0].V_TOTAL_COMPRA);

                    return $dispararAlerta('Erro: ' + data[0].O_COD_RETORNO + '-' + data[0].O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    };

    $('.enviar_para_loja').on('click', function (e) {
        e.preventDefault();

        var action = $(this).data('action');

        $.fancybox.open({
            href: action,
            type: "ajax",
            scrolling: 'no',
            autoSize: false,
            width: "400px",
            height: "auto",
            minHeight: "100px",
            ajax: {
                type: "POST",
                complete: function (jqXHR, textStatus) {
                }
            }
        });
    });

    $('#menu-principal li > a.no-childs').click(function (e) {

        var item = $(this).closest('li');

        if ($(item).children('ul:visible').length) {
            $(item).children('ul:visible').stop().slideUp('slow');
        } else {
            $(item).children('ul').stop().slideToggle('slow');
        }

        return false;
    });

    $('#menu-principal-no-xs > li:has(ul) > a').click(function (e) {
        e.preventDefault();

        var item = $(this).closest('li');
        var main = $(this).closest('ul');

        if ($(main).find('ul.aberto').length) {
            $(main).find('ul.submenu-area').removeClass('aberto').addClass('fechado');
            $(main).find(item).find('ul.submenu-area2').removeClass('aberto').addClass('fechado');
        }

        $(item).find('ul.submenu-area').removeClass('fechado').addClass('aberto');
    });

    $('ul.submenu-child > li:has(ul) > a').click(function (e) {
        e.preventDefault();
        var item = $(this).closest('li');
        var main = $(this).closest('ul');

        if ($(main).find('ul.aberto').length) {
            $(main).find('ul.submenu-area2').removeClass('aberto').addClass('fechado');
        }

        $(item).find('ul.submenu-area2').removeClass('fechado').addClass('aberto');
    });
});
