<?php

namespace santosdummontsite;

use santosdummontsite\Session,
    santosdummontsite\Common,
    santosdummontsite\ReCaptcha;

class Controller
{

    protected $session;
    protected $model;

    public function __construct()
    {
        Session::inicializar();
        self::loadModel('Application\Model\ModelSistema', 'modelsistema');
    }

    protected function loadView($nome, $vars = null)
    {
        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'data');
        }

        $arquivo = VIEW_PATH . '/' . $nome . '.phtml';

        if (!file_exists($arquivo)) {
            $this->error("Houve um erro. Essa View {$nome} nao existe.");
        }

        require_once($arquivo);
    }

    protected function loadModel($nome, $apelido = "")
    {
        $this->$nome = new $nome();

        if ($apelido !== '') {
            $this->$apelido = &$this->$nome; //Passando por referencia
        }
    }

    protected function error($msg)
    {
        throw new \Exception($msg);
    }

    protected function validarRetornoServico($retorno = array())
    {

        if ($retorno['O_WRET_COD'] > ZERO) {
            Common::gerarMensagem(OPCAOAVISO, $retorno['O_ARET_DESC_CURTO']);
        }
    }

    protected function carregarMenu()
    {
        $request = new Request();

        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
            $social_tipo = 0;
            $dados['logado'] = false;
        } else {
            $token = Session::get('acesso');
            $social_tipo = Session::get('social_tipo');

            $dados['logado'] = true;
            $dados['cliente'] = $this->modelsistema->UsuarioPorToken($token, $social_tipo);
        }

        $dados['controller'] = $request->getControlador();
        $dados['metodo'] = $request->getMetodo() == 'main' ? $request->getControlador() : $request->getMetodo();

        if ($dados['controller'] == 'Home') {
            //$dados['informacoessite'] = $this->modelsistema->InformacoesSite()[0];
        }
        $dados['menu'] = $this->modelsistema->MenuPrincipal();
        $fabricantes = $this->modelsistema->ListaFabricantesCadastrados();
        $dados['fabricante'] = array_chunk($fabricantes, 6);

        $dados['contatosite'] = $this->modelsistema->ContatosdoSite()[0];
        $dados['redessociais'] = $this->modelsistema->RedesSociais();
        $dados['carrinho'] = $this->modelsistema->ListaCarrinhoCompra($token, $social_tipo);
        $dados['infocarrinho'] = $this->modelsistema->InfoCarrinhoCompra($token);
        $dados['listaoutras'] = $this->modelsistema->ListaOutrasCategorias();
        $dados['meiospagamentos'] = $this->modelsistema->ListaMeiosPagamentos();
        //$dados['urlnewsletter'] = SITE_URL . "/Home/GravarNewsletter";

        return $dados;
    }

    protected function prepararView($view, $dados = array())
    {

        $dados['config'] = $this->modelsistema->Configuracoes()[0];

        if (empty($dados['menu'])) {
            $dados += $this->carregarMenu();
        }

        $this->loadView("Template/cabecalho", $dados);
        $this->loadView("Template/topbar", $dados);
        $this->loadView("Template/mainmenu", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/footerbar", $dados);
        $this->loadView("Template/rodape", $dados);
    }

    protected function prepararViewTeste($view, $dados = array())
    {

        if (empty($dados['menu'])) {
            $dados += $this->carregarMenu();
        }

        $this->loadView("Template/cabecalho", $dados);
        $this->loadView("Template/topbar", $dados);
        $this->loadView($view, $dados);
    }

    protected function prepararBasicView($view, $dados = array())
    {
        $request = new Request();
        $dados['controller'] = $request->getControlador();
        $dados['metodo'] = $request->getMetodo();
        $this->loadView($view, $dados);
    }

    protected function loadException(Exception $msg, $voltar = TRUE)
    {
        Common::relatarExcecaoParaAdmin($msg);
        Common::gerarMensagem(OPCAOERRO, $msg->getMessage(), $voltar);
    }

    function validarCaptcha($captcha, $ip)
    {
        $recaptcha = new ReCaptcha(CAPTCHA_SECRET);

        $resp = $recaptcha->verify($captcha, $ip);

        if ($resp->isSuccess()) {
            return ['erro' => false, 'message' => 'Sucesso!'];
        } else {
            return ['erro' => true, 'message' => $resp->getErrorCodes()];
        }
    }

    function consumir_cetrus($dados)
    {
        $url = 'http://' . $dados['ip'] . URL_API_CONSULTAR . '{"IdUsuario": "' . INT_ID_USUARIO . '", "Senha": "' . INT_ID_SENHA . '", "Produto_Id": "' . $dados['sku'] . '"}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result !== FALSE) {
            $retorno = [
                'ERRO' => 0,
                'DADOS' => json_decode($result, true)
            ];
        } else {
            $retorno = [
                'ERRO' => 1,
                'DADOS' => curl_error($ch)
            ];
        }

        $ret = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', json_encode($retorno)), true);

        return $ret;
    }

    function consulta_cetrus($dados)
    {
        $url = 'http://' . $dados['ip'] . URL_APU_CONSUMIR . '{"IdUsuario": "' . INT_ID_USUARIO . '", "Senha": "' . INT_ID_SENHA . '", "Produto_Id": "' . $dados['sku'] . '"}';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result !== FALSE) {
            $retorno = [
                'ERRO' => 0,
                'DADOS' => json_decode($result, true)
            ];
        } else {
            $retorno = [
                'ERRO' => 1,
                'DADOS' => curl_error($ch)
            ];
        }

        $ret = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', json_encode($retorno)), true);

        return $ret;
    }

    function valida_cep_correio($cep)
    {
        $url = 'https://viacep.com.br/ws/' . $cep . '/json/';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result !== FALSE) {
            $ret = json_decode($result, true);
        } else {
            $ret = curl_error($ch);
        }

        return $ret;
    }

    public function GetGoogleAccessToken($client, $code)
    {
        $url = 'https://www.googleapis.com/oauth2/v4/token';

        $client_id = $client->getClientId();
        $client_secret = $client->getClientSecret();
        $redirect_uri = $client->getRedirectUri();

        $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code=' . $code . '&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200)
            throw new Exception('Error : Failed to receieve access token');

        return $data;
    }

    public function GetGoogleUserProfileInfo($access_token)
    {
        $url = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=name,email,gender,id,picture,verified_email';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $access_token));
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200)
            throw new Exception('Error : Failed to get user information');

        return $data;
    }

    private function getProfileGoogle($client)
    {
        return 'https://accounts.google.com/o/oauth2/auth?scope=' . $client->getScopes()[0] . '&redirect_uri=' . $client->getRedirectUri() . '&response_type=code&client_id=' . $client->getClientId() . '&access_type=online';
    }

    public function LoadAPISocial($config)
    {

        //CONFIGURAÇÃO GOOGLE
        $KEY_FILE_LOCATION = CONTROLLER_PATH . '/Common/client_secret.json';

        $ggclient = new \Google_Client();
        $ggclient->setApplicationName('Login Santos Dumont');
        $ggclient->setAuthConfig($KEY_FILE_LOCATION);
        $ggclient->setRedirectUri(FAIL_GOOGLE_CALLBACK);
        $ggclient->setScopes('profile');

        $gghelper = self::getProfileGoogle($ggclient);

        //CONFIGURAÇÃO FACEBOOK
        $fb = new \Facebook\Facebook([
            'app_id' => Common::encrypt_decrypt('decrypt', $config['FACEBOOK_ID']),
            'app_secret' => Common::encrypt_decrypt('decrypt', $config['APP_SECRET_ID']),
            'default_graph_version' => 'v2.10'
        ]);

        $fbhelper = $fb->getRedirectLoginHelper();

        //RETORNO
        $dados['fb'] = $fb;
        $dados['gg'] = $ggclient;
        $dados['facebook'] = $fbhelper;
        $dados['google-plus'] = $gghelper;

        return $dados;
    }

    function sair()
    {
        Session::destroy();
        Common::retornarPagina(get_called_class());
    }

    public function validar_cpf($str){
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/', '', (string) $str);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }

    public function validar_cnpj($str)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $str);

        // Valida tamanho
        if (strlen($cnpj) != 14) {
            return false;
        }

        // Lista de CNPJs inválidos
        $invalidos = [
            '00000000000000',
            '11111111111111',
            '22222222222222',
            '33333333333333',
            '44444444444444',
            '55555555555555',
            '66666666666666',
            '77777777777777',
            '88888888888888',
            '99999999999999'
        ];

        // Verifica se o CNPJ está na lista de inválidos
        if (in_array($cnpj, $invalidos)) {
            return false;
        }

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto)) {
            return false;
        }

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj[$i] * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
    }
}
