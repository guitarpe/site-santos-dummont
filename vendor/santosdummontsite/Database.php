<?php
namespace santosdummontsite;

use \PDO,
    santosdummontsite\Common;

class Database extends PDO
{

    public function __construct($DB_HOST, $DB_PORT, $DB_NAME, $DB_USER, $DB_PASS)
    {
        $dns = "mysql:host=$DB_HOST; dbname=$DB_NAME; port=$DB_PORT; charset=utf8;";
        $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

        try {
            $conn = parent::__construct($dns, $DB_USER, $DB_PASS, $options);
        } catch (PDOException $e) {
            var_dump($e);
            return $e->getMessage();
        } finally {
            $conn = null;
        }
    }

    public function select($sql, $array = [], $all = TRUE, $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);

        foreach ($array as $key => $value) {
            $tipo = ( is_int($value) ) ? PDO::PARAM_INT : PDO::PARAM_STR;

            $sth->bindValue("$key", $value, $tipo);
        }

        $sth->execute();

        if ($all) {
            return $sth->fetchAll($fetchMode);
        } else {
            return $sth->fetch($fetchMode);
        }
    }

    public function insert($table, $data = [])
    {
        ksort($data);

        $camposNomes = implode(', ', array_keys($data));
        $camposValores = ':' . implode(', :', array_keys($data));

        $sth = $this->prepare('INSERT INTO ' . $table . ' (' . $camposNomes . ') VALUES (' . $camposValores . ')');

        foreach ($data as $key => $value) {
            $tipo = ( is_int($value) ) ? PDO::PARAM_INT : (is_string($value) ? PDO::PARAM_STR : (is_bool($value) ? PDO::PARAM_BOOL : PDO::PARAM_NULL));

            $sth->bindValue(":$key", $value, $tipo);
        }

        return $sth->execute();
    }

    public function update($table, $data = [], $where)
    {
        ksort($data);

        $dados = '';

        foreach ($data as $key => $value) {
            $dados .= "$key=:$key,";
        }

        $novosDados = rtrim($dados, ',');

        $sql = "UPDATE $table SET $novosDados WHERE $where";
        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $tipo = (is_int($value)) ? PDO::PARAM_INT : PDO::PARAM_STR;

            $sth->bindValue(":$key", $value, $tipo);
        }

        return $sth->execute();
    }

    public function delete($table, $data = [])
    {
        ksort($data);

        $dados = '';

        foreach ($data as $key => $value) {
            $dados .= "$key=:$key,";
        }

        $where = rtrim($dados, ',');

        $sql = "DELETE FROM $table WHERE $where";
        $sth = $this->prepare($sql);

        foreach ($data as $key => $value) {
            $tipo = (is_int($value)) ? PDO::PARAM_INT : PDO::PARAM_STR;

            $sth->bindValue(":$key", $value, $tipo);
        }

        $result = $sth->execute();

        if (!$result) {
            return 0;
        } else {
            return $sth->rowCount();
        }
    }

    public function executeProcedure($sql, $parametros)
    {
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        try {
            $stmt = $this->prepare($sql);

            foreach ($parametros as $key => $value) {
                $tipo = ( is_int($value) ) ? PDO::PARAM_INT : (is_string($value) ? PDO::PARAM_STR : (is_bool($value) ? PDO::PARAM_BOOL : PDO::PARAM_NULL));

                $stmt->bindValue(":$key", $value, $tipo);
            }
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $rows;
        } catch (PDOException $erro) {
            print_r($erro->getMessage());
        }
    }
}
