<?php

namespace Application\Model;

use santosdummontsite\Model;

class ModelSistema extends Model
{

    public function ConfigEnvioEmail()
    {
        $parametros = [];

        $sql = "SELECT
                    SMTP_HOST,
                    SMTP_PORTA,
                    SMTP_USER,
                    SMTP_SENHA,
                    MAIL_CONTATO_DEFAULT,
                    NOME_CONTATO_DEFAULT,
                    URL_SISTEMA,
                    URL_SISTEMA,
                    LOGOTIPO,
                    FAVICON,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CONFIGURACOES LIMIT 1";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }
    /* public function TokenByAPI($user, $tipo)
      {

      $parametros = [
      'I_API_ID' => $user['id'],
      'I_TIPO' => $tipo
      ];

      $sql = "CALL PRC_GERA_TOKEN_CLIENTE(:I_API_ID, :I_TIPO);";

      $resultado = self::callprocedure($sql, $parametros);

      return $resultado['list'];
      } */

    public function ValidaTokenRegistro($token)
    {

        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_VALIDA_TOKEN_CLIENTE(:I_TOKEN, @O_COD_RETORNO, @O_DESC_CURTO, @O_TOKEN_INVALIDO);";

        $resultado = self::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    /*public function getTokenClienteByTokenPedido($tokenped)
    {
        $parametros = [
            'I_TOKEN_PED' => $tokenped
        ];

        $sql = "SELECT
                    DISTINCT LI.TOKEN,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PEDIDO_LISTA_COMPRA LI
                    INNER JOIN PEDIDOS PED ON PED.PED_ID=LI.PED_ID
                WHERE PED.TOKEN=:I_TOKEN_PED";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"][0];
    }*/

    public function UsuarioPorToken($token, $social_tipo = null)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "SELECT
                    CLI.CLI_ID,
                    CLI_EMAIL,
                    (CASE WHEN DC.EMAIL != NULL THEN DC.EMAIL ELSE CLI.CLI_EMAIL END) AS EMAIL,
                    CLI_IMAGEM,
                    CLI.PASTA_CLI,
                    DC.TIPO,
                    DC.GENERO,
                    DC.NOME_COMPLETO,
                    DC.DT_NASCIMENTO,
                    DC.TELEFONE_RESID,
                    DC.TELEFONE_CEL,
                    DC.TELEFONE_COM,
                    DC.OCUPACAO,
                    DC.TIPO_DOC,
                    DC.NUM_DOC,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                    INNER JOIN TOKENS TK ON TK.US_ID=CLI.CLI_ID AND TK.PER_ID=CLI.PER_ID
                WHERE TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        $dadoscliente = [
            'dados' => count($resultado["list"]) > 0 ? $resultado["list"][0] : null,
            'enderecos' => count($resultado["list"]) > 0 ? self::EnderecosUsuarioPorToken($token) : null,
            'cartoes' => count($resultado["list"]) > 0 ? self::CartoesUsuarioPorToken($token) : null
        ];

        return $dadoscliente;
    }

    //    public function VerificaToken($token)
    //    {
    //        $parametros = [
    //            'I_TOKEN' => $token
    //        ];
    //
    //        $sql = "CALL PRC_VERIFICAR_TOKEN_ACESSO(:I_TOKEN);";
    //        $resultado = self::callprocedure($sql, $parametros);
    //
    //        return $resultado['list'];
    //    }

    public function VerificaEmailClienteExiste($email)
    {
        $parametros = [
            'I_EMAIL' => $email
        ];

        $sql = "SELECT
                    COUNT(1) AS TOTAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                WHERE COALESCE(CLI.CLI_EMAIL, DC.EMAIL)=:I_EMAIL";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function VerificaCPFClienteExiste($cpf, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CPF' => $cpf
        ];

        $sql = "SELECT
                    COUNT(1) AS TOTAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    INNER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                    INNER JOIN TOKENS TK ON TK.US_ID!=CLI.CLI_ID
                WHERE DC.NUM_DOC=:I_CPF AND TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function UsuarioPorID($id)
    {
        $parametros = [
            'I_CLI_ID' => $id,
        ];

        $sql = "SELECT
                    CLI.CLI_ID,
                    CLI.PER_ID,
                    CLI.CLI_EMAIL,
                    CLI.CLI_STATUS,
                    CLI.CLI_IMAGEM,
                    DC.NOME_COMPLETO,
                    (CASE WHEN DC.EMAIL != NULL THEN DC.EMAIL ELSE CLI.CLI_EMAIL END) AS EMAIL,
                    DC.GENERO,
                    (CASE
                        WHEN DC.TELEFONE_RESID != NULL THEN DC.TELEFONE_RESID
			WHEN DC.TELEFONE_CEL != NULL THEN DC.TELEFONE_CEL
			WHEN DC.TELEFONE_COM != NULL THEN DC.TELEFONE_COM
                    END) AS TELEFONE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                WHERE CLI.CLI_ID=:I_CLI_ID";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AtivarCliente($id)
    {
        $parametros = [
            'I_CLI_ID' => $id,
        ];

        $sql = "CALL PRC_ATIVAR_CLIENTE(:I_CLI_ID);";

        $resultado = self::callprocedure($sql, $parametros);

        return $resultado["list"][0];
    }

    /*public function UsuarioPorTokenRegistro($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "SELECT
                    CLI.CLI_ID,
                    CLI_EMAIL,
                    CLI_IMAGEM,
                    CLI.PASTA_CLI,
                    CLI.CLI_STATUS,
                    DC.TIPO,
                    DC.GENERO,
                    DC.NOME_COMPLETO,
                    DC.DT_NASCIMENTO,
                    DC.TELEFONE_RESID,
                    DC.TELEFONE_CEL,
                    DC.TELEFONE_COM,
                    DC.OCUPACAO,
                    DC.TIPO_DOC,
                    DC.NUM_DOC,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                    INNER JOIN TOKENS_REGISTRO TK ON TK.US_ID=CLI.CLI_ID AND TK.PER_ID=CLI.PER_ID
                WHERE TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        $dadoscliente = [
            'dados' => $resultado["list"][0],
            'enderecos' => self::EnderecosUsuarioPorTokenRegistro($token)
        ];

        return $dadoscliente;
    }*/

    public function EnderecosUsuarioPorToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "SELECT
                    END_ID,
                    END_ENDERECO,
                    END_COMPLEMENTO,
                    END_BAIRRO,
                    END_CIDADE,
                    END_CEP,
                    END_NUM,
                    END_UF,
                    END_PAIS,
                    IS_FATURAMENTO,
                    IS_ENVIO,
                    END_TIPO,
                    END_STATUS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM ENDERECOS END
                INNER JOIN TOKENS TK ON TK.US_ID=END.USUARIO_ID AND TK.PER_ID=END.PER_ID
                WHERE END_STATUS=1 AND TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function CartoesUsuarioPorToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "SELECT
                    CART_ID,
                    CART_NOME,
                    CART_NUMERO,
                    CART_CCV,
                    CART_VALIDADE,
                    IS_USO,
                    CART_CPF,
                    CART_STATUS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CARTOES CAR
                    INNER JOIN TOKENS TK ON TK.US_ID=CAR.USUARIO_ID
                WHERE CAR.CART_STATUS=1 AND TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function CartoesUsuarioPorTokenId($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        $sql = "SELECT
                    CART_ID,
                    CART_NOME,
                    CART_NUMERO,
                    CART_CCV,
                    CART_VALIDADE,
                    IS_USO,
                    CART_STATUS,
                    CART_CPF,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CARTOES CAR
                    INNER JOIN TOKENS TK ON TK.US_ID=CAR.USUARIO_ID
                WHERE CAR.CART_STATUS=1
                    AND TK.TOKEN=:I_TOKEN
                    AND CAR.CART_ID=:I_ID";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function EnderecosUsuarioPorTokenRegistro($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "SELECT
                    END_ID,
                    END_ENDERECO,
                    END_COMPLEMENTO,
                    END_BAIRRO,
                    END_CIDADE,
                    END_CEP,
                    END_NUM,
                    END_UF,
                    END_PAIS,
                    IS_FATURAMENTO
                    IS_ENVIO,
                    END_TIPO,
                    END_STATUS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM ENDERECOS END
                INNER JOIN TOKENS_REGISTRO TK ON TK.US_ID=END.USUARIO_ID AND TK.PER_ID=END.PER_ID
                WHERE END_STATUS=1 AND TK.TOKEN=:I_TOKEN";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function MenuPrincipal()
    {
        $parametros = [];

        $sql = "SELECT
                    CAT_ID,
                    CAT_NOME,
                    CAT_URL,
                    CAT_NIVEL,
                    CAT_STATUS,
                    CAT_PAI,
                    CAT_POSICAO,
                    CAT_IMAGEM,
                    FILHOS,
                    CTFILHOS,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                 FROM VW_MENU
                    ORDER BY CTFILHOS DESC";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaOutrasCategorias()
    {
        $parametros = [];

        $sql = "SELECT
                    CAT_ID,
                    CAT_NOME,
                    CAT_URL,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    'N' AS O_TOKEN_INVALIDO
                FROM VW_CAT_RAND";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaFabricantesCadastrados()
    {
        $parametros = [];

        $sql = "SELECT
                    PAR_ID,
                    PAR_NOME,
                    PAR_STATUS,
                    PAR_DESCRICAO,
                    IMAGEM,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                FROM VW_FABRICANTES_RAND";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function Configuracoes()
    {
        $parametros = [];

        $sql = "SELECT
                    URL_SISTEMA,
                    LOGOTIPO,
                    FAVICON,
                    IP_INTEGRACAO,
                    UNI_MED_PESO,
                    UNI_MED_TAM,
                    CORREIO_USUARIO,
                    CORREIO_SENHA,
                    TRACKING_ID,
                    SITE_ID,
                    CHAVE_CAPTCHA,
                    FACEBOOK_ID,
                    APP_SECRET_ID,
                    CIELO_MECHANT_ID,
                    CIELO_MECHANT_KEY,
                    CIELO_CLIENT_ID,
                    PAGSEG_EMAIL,
                    PAGSEG_TIPO_ENVIO,
                    PAGSEG_SAND_TOKEN,
                    PAGSEG_SAND_APP_ID,
                    PAGSEG_SAND_APP_KEY,
                    PAGSEG_PRD_TOKEN,
                    PAGSEG_PRD_APP_ID,
                    PAGSEG_PRD_APP_KEY,
                    TAG_MANAGER_ID,
                    GOOGLE_USUARIO,
                    GOOGLE_SENHA,
                    EMAIL_RECEBE_PAGAMENTO,
                    ATUALIZA_AUTO_CETRUS,
                    VIDEO_BOASVINDAS,
                    TIPO_VIDEO_BV,
                    BANNER_SENTIDO,
                    BANNER_TEMPO,
                    EXIBIR_ESTOQUE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CONFIGURACOES LIMIT 1";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ContatosdoSite()
    {
        $parametros = [];

        $sql = "SELECT
                    EMAIL_TECNICO,
                    ENDERECO,
                    BAIRRO,
                    NUMERO,
                    CIDADE,
                    UF,
                    CEP,
                    COMPLEMENTO,
                    TELEFONE,
                    WHATSAPP,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CONTATO;";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InformacoesSite()
    {
        $parametros = [];

        $sql = "SELECT
                    SOBRENOS,
                    INFOENTREGA,
                    SERVICOCLIENTE,
                    TRANSRET,
                    METODOSPAG,
                    TERMOSUSO,
                    POLITICA,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM INFOSITE;";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaMeiosPagamentos()
    {
        $parametros = [];

        $sql = "SELECT
                    MEIO_ID,
                     NOME_MEIO,
                     DESCRICAO_MEIO,
                     IMAGEM,
                    0 as O_COD_RETORNO,
                     '' as O_DESC_CURTO,
                     'N' as O_TOKEN_INVALIDO
                FROM
                    MEIOS_PAGAMENTOS
                WHERE MP_STATUS=1
                ORDER BY NOME_MEIO";
        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function RedesSociais()
    {
        $parametros = [];

        $sql = "SELECT
                    RS_NOME,
                    RS_DESC,
                    RS_LINK,
                    RS_ICO,
                    RE_EXEC_LOGIN,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM REDES_SOCIAIS;";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GravaNewsletter($dados)
    {
        $parametros = [
            'I_EMAIL' => $dados['EMAIL']
        ];

        $sql = "CALL PS_GRAVAR_NEWSLETTER(:I_EMAIL);";

        $resultado = self::callprocedure($sql, $parametros);

        return $resultado["list"];
    }

    public function InativarToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_INATIVAR_TOKEN(:I_TOKEN);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado;
    }

    protected function verificarToken($list)
    {
        $token = null;

        if ($list['erro'] == 1) {
            $token = $list['att_token'];
        } else {

            foreach ($list['list'] as $key => $data) {
                if (is_array($data)) {
                    foreach ($data as $key => $dados) {
                        $token = $data['TOKEN_INVALIDO'];
                        break;
                    }
                } else {
                    $token = $list['list']['O_TOKEN_INVALIDO'];
                    break;
                }
            }
        }

        return $token;
    }

    public function AdicionarCarrinho($id, $token, $qtde)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_ID' => $id,
            'I_QTDE' => $qtde
        ];

        $sql = "CALL PRC_ADICIONAR_CARRINHO(:I_TOKEN, :I_PRD_ID, :I_QTDE);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function AdicionarFavoritos($id, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_ID' => $id
        ];

        $sql = "CALL PRC_ADICIONAR_FAVORITOS(:I_TOKEN, :I_PRD_ID);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function ClassificarProduto($id, $token, $nota)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PRD_ID' => $id,
            'I_NOTA' => $nota
        ];

        $sql = "CALL PRC_CLASSIFICAR_PRODUTO(:I_TOKEN, :I_PRD_ID, :I_NOTA);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function LembrarEstoqueProduto($id, $token, $email = null)
    {
        if (!empty($email)) {
            $parametros = [
                'I_EMAIL' => $email,
                'I_PRD_ID' => $id
            ];

            $sql = "CALL PRC_LEMBRAR_DISPONIBILIDADE(:I_EMAIL, :I_PRD_ID);";
            $resultado = self::callprocedure($sql, $parametros);

            return $resultado['list'];
        } else {
            $parametros = [
                'I_TOKEN' => $token,
                'I_PRD_ID' => $id
            ];

            $sql = "CALL PRC_LEMBRAR_ESTOQUE_PRODUTO(:I_TOKEN, :I_PRD_ID);";
            $resultado = self::callprocedure($sql, $parametros);

            return $resultado['list'];
        }
    }

    public function ResgistroContatoWhatts($dados)
    {
        $parametros = [
            'I_NOME' => $dados['I_NOME'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_TELEFONE' => $dados['I_TELEFONE']
        ];

        $sql = "CALL PRC_REGISTRO_CONTATO_WHATS(:I_NOME, :I_EMAIL, :I_TELEFONE);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function ListaCarrinhoCompra($token, $social_tipo = null)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    CC.CAR_ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    SUM(CC.QTDE) AS QTDE,
                    PR.PRD_PRECO,
                    COALESCE((CC.QTDE*PR.PRD_PRECO),0) AS PRECO_TOTAL,
                    PR.PASTA_IMAGENS,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                GROUP BY PR.PRD_ID ORDER BY PR.PRD_ID
                LIMIT 12";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InfoCarrinhoCompra($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    SUM(CC.QTDE) AS QTDE,
                    SUM(CC.PRC_TOTAL) AS TOTAL
                FROM (
                    SELECT
                        CC.QTDE,
                        PR.PRD_PRECO AS PRC_UNITARIO,
                        COALESCE((CC.QTDE*PR.PRD_PRECO),0) AS PRC_TOTAL
                    FROM VW_PRODUTOS PR
                        INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                    WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                ) CC";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GravarUsuarioSocial($token, $dados, $pasta, $op)
    {
        if($dados['gender'] == 'male'){
            $genero = 1;
        }elseif ($dados['gender'] == 'female'){
            $genero = 2;
        }else{
            $genero = 0;
        }


        if ($op == 1) {
            $parametros = [
                'I_TOKEN' => $token,
                'I_TIPO' => $op,
                'I_CLI_EMAIL' => $dados['email'],
                'I_CLI_IMAGE' => $dados['picture']['url'],
                'I_NOME_CLIENTE' => $dados['first_name'] . ' ' . $dados['last_name'],
                'I_GENERO_CLIENTE' =>  $genero,
                'I_PASTA_CLIENTE' => $pasta
            ];
        } else if ($op == 2) {
            $parametros = [
                'I_TOKEN' => $token,
                'I_TIPO' => $op,
                'I_CLI_EMAIL' => $dados['email'],
                'I_CLI_IMAGE' => $dados['picture'],
                'I_NOME_CLIENTE' => $dados['name'],
                'I_GENERO_CLIENTE' => $genero,
                'I_PASTA_CLIENTE' => $pasta
            ];
        }

        $sql = "CALL PS_IN_CLIENT_SOCIAL(:I_TOKEN, :I_TIPO, :I_CLI_EMAIL, :I_CLI_IMAGE, :I_NOME_CLIENTE, :I_GENERO_CLIENTE, :I_PASTA_CLIENTE);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado;
    }

    public function TotalPedido($token, $tokenped)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TOKEN_P' => $tokenped
        ];

        $sql = "SELECT
                    PE.VALOR_FRETE AS FRETE_TOTAL,
                    PE.VALOR_PEDIDO AS PRECO_TOTAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PEDIDOS PE
                    INNER JOIN PEDIDO_LISTA_COMPRA PL ON PL.PED_ID=PE.PED_ID
                WHERE PL.CLI_ID=(SELECT US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN) AND PE.TOKEN=:I_TOKEN_P";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function UpdatePedido($dados)
    {
        if (intval($dados['I_TIPO']) == 1) {
            $parametros = [
                'I_TOKEN' => $dados['I_TOKEN'],
                'I_TOKEN_PED' => $dados['I_TOKEN_PED'],
                'I_EMAIL_ENVIADO' => $dados['I_EMAIL_ENVIADO']
            ];

            $sql = "CALL PS_ATUALIZAR_PEDIDO(:I_TOKEN, :I_TOKEN_PED, :I_EMAIL_ENVIADO);";
        }

        $resultado = self::callprocedure($sql, $parametros);

        return $resultado;
    }

    public function DeletePedido($id)
    {
        $parametros = [
            'I_PED_ID' => $id
        ];

        $sql = "CALL PS_EXCLUIR_PEDIDO(:I_PED_ID);";

        $resultado = self::callprocedure($sql, $parametros);

        return $resultado;
    }

    public function LimparCarrinhoFinalPedido($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PS_LIMPAR_CARRINHO(:I_TOKEN);";

        self::callprocedure($sql, $parametros);
    }
}
