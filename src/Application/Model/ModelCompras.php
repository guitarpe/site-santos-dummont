<?php

namespace Application\Model;

use santosdummontsite\Model,
    santosdummontsite\Common;

class ModelCompras extends Model
{

    public function ProdutoUri($uri)
    {
        $parametros = [
            'I_PRD_PATH' => $uri
        ];

        $sql = "SELECT PRD_ID, PRD_SKU FROM PRODUTOS WHERE PRD_PATH=:I_PRD_PATH LIMIT 1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function CategoriaUri($uri)
    {
        $parametros = [
            'I_CAT_URL' => $uri
        ];

        $sql = "SELECT CAT_ID FROM CATEGORIAS WHERE CAT_URL=:I_CAT_URL LIMIT 1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ParceirosUri($uri)
    {
        $parametros = [
            'I_PAR_URL' => $uri
        ];

        $sql = "SELECT PAR_ID FROM PARCEIROS WHERE PAR_URL=:I_PAR_URL LIMIT 1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function DadosProdutoSite($id, $cliente = null, $token = null)
    {
        if (empty($cliente)) {
            $parametros = [
                'I_PRD_ID' => $id,
                'I_TOKEN' => $token
            ];

            $sql = "SELECT
                        PR.PRD_ID,
                        CAT.CAT_ID,
                        CT.CAT_NOME,
                        PR.PRD_NOME,
                        PR.PRD_UNIDADE,
                        PR.PRD_SKU,
                        PR.PRD_STATUS,
                        PR.PRD_PATH,
                        PR.PRD_DESTAQUE,
                        PR.PRD_QTDE_ESTOQUE,
                        PR.PRD_DESCRICAO_MINIMA,
                        PR.PRD_MODELO,
                        PR.PRD_FICHA_TECNICA,
                        PR.PRD_PRECO,
                        PR.PRD_TAXA,
                        PR.PASTA_IMAGENS,
                        PR.PRD_PRECO_ANTERIOR,
                        COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                        (CASE WHEN FAV.PRD_ID IS NOT NULL THEN 1 ELSE 0 END) AS FAVORITO,
                        COALESCE(CLP.NOTA, 0) AS NOTA,
                        (SELECT   GROUP_CONCAT(IMAGEM) FROM PRODUTOS_IMAGENS PI WHERE PI.PRD_ID=PR.PRD_ID AND PI.IMAGEM!=PR.PRD_IMAGEM_PRINCIPAL) AS OUTRAS_IMGS,
                        0 as O_COD_RETORNO,
                        '' as O_DESC_CURTO,
                        'N' as O_TOKEN_INVALIDO
                    FROM VW_PRODUTOS PR
                        LEFT OUTER JOIN CATEGORIAS_PRD CAT ON CAT.PRD_ID=PR.PRD_ID
                        LEFT OUTER JOIN CATEGORIAS CT ON CT.CAT_ID=CAT.CAT_ID
                        LEFT OUTER JOIN FAVORITOS FAV ON FAV.PRD_ID=PR.PRD_ID AND FAV.TOKEN=:I_TOKEN
                        LEFT OUTER JOIN CLASSIFICACAO_PRODUTO CLP ON CLP.PRD_ID=PR.PRD_ID AND CLP.TOKEN=:I_TOKEN
                        LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                    WHERE PR.PRD_STATUS=1 AND PR.PRD_ID=:I_PRD_ID LIMIT 1";
        } else {
            $parametros = [
                'I_PRD_ID' => $id,
                'I_CLI_ID' => $cliente
            ];

            $sql = "SELECT
                        PR.PRD_ID,
                        CAT.CAT_ID,
                        CT.CAT_NOME,
                        PR.PRD_NOME,
                        PR.PRD_UNIDADE,
                        PR.PRD_SKU,
                        PR.PRD_STATUS,
                        PR.PRD_PATH,
                        PR.PRD_DESTAQUE,
                        PR.PRD_QTDE_ESTOQUE,
                        PR.PRD_DESCRICAO_MINIMA,
                        PR.PRD_MODELO,
                        PR.PRD_FICHA_TECNICA,
                        PR.PRD_PRECO,
                        PR.PRD_TAXA,
                        PR.PASTA_IMAGENS,
                        PR.PRD_PRECO_ANTERIOR,
                        COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                        (CASE WHEN FAV.PRD_ID IS NOT NULL THEN 1 ELSE 0 END) AS FAVORITO,
                        COALESCE(CLP.NOTA, 0) AS NOTA,
                        (SELECT   GROUP_CONCAT(IMAGEM) FROM PRODUTOS_IMAGENS PI WHERE PI.PRD_ID=PR.PRD_ID AND PI.IMAGEM!=PR.PRD_IMAGEM_PRINCIPAL AND PI.TAMANHO='image') AS OUTRAS_IMGS,
                        0 as O_COD_RETORNO,
                        '' as O_DESC_CURTO,
                        'N' as O_TOKEN_INVALIDO
                    FROM VW_PRODUTOS PR
                        INNER JOIN CATEGORIAS_PRD CAT ON CAT.PRD_ID=PR.PRD_ID
                        LEFT OUTER JOIN FAVORITOS FAV ON FAV.PRD_ID=PR.PRD_ID AND FAV.CLI_ID=:I_CLI_ID
                        LEFT OUTER JOIN CLASSIFICACAO_PRODUTO CLP ON CLP.PRD_ID=PR.PRD_ID AND CLP.CLI_ID=:I_CLI_ID
                        INNER JOIN CATEGORIAS CT ON CT.CAT_ID=CAT.CAT_ID
                        LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                    WHERE PR.PRD_STATUS=1 AND PR.PRD_ID=:I_PRD_ID LIMIT 1";
        }

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GravarComentario($dados)
    {
        $parametros = [
            'I_NOME' => $dados['NOME'],
            'I_EMAIL' => $dados['EMAIL'],
            'I_COMENTARIO' => $dados['COMENTARIO'],
            'I_PRD_ID' => $dados['PRD_ID']
        ];

        $sql = "CALL PS_GRAVAR_COMENT(:I_NOME, :I_EMAIL, :I_COMENTARIO, :I_PRD_ID);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function ImagensProdutoSite()
    {
        $parametros = [];

        $sql = "";
        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutosPopulares()
    {
        $parametros = [];

        $sql = "SELECT
                    PRD_ID,
                    PRD_SKU,
                    PRD_DESTAQUE,
                    PRD_NOME,
                    PRD_PRECO,
                    PRD_PRECO_ANTERIOR,
                    PRD_TAXA,
                    PRD_PATH,
                    PASTA_IMAGENS,
                    CAT_NOME,
                    PRD_QTDE_ESTOQUE,
                    PRD_IMAGEM_PRINCIPAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS_POPULARES ";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaComentarios($id)
    {
        $parametros = [
            'PRD_ID' => $id
        ];

        $sql = "SELECT
                    PRD_ID,
                    NOME,
                    EMAIL,
                    COMENTARIO,
                    DATA_CAD,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_COMENTARIOS WHERE PRD_ID=:PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutosSimilares($cat)
    {
        $parametros = [
            'I_CAT_ID' => $cat
        ];

        $sql = "SELECT   DISTINCT
                    PR.PRD_ID AS PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME AS PRD_NOME,
                    PR.PRD_PRECO,
                    PR.PRD_TAXA,
                    PR.PRD_PATH,
                    PR.PRD_QTDE_ESTOQUE,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PASTA_IMAGENS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PR.PRD_ID
                    INNER JOIN CATEGORIAS CAT ON CAT.CAT_ID=CP.CAT_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND CAT.CAT_ID=:I_CAT_ID
                ORDER BY RAND() LIMIT 4";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotalCategoriaProdutos($busca)
    {
        $parametros = [
            'I_CAT_ID' => $busca['CAT']
        ];

        $sql = "SELECT
                    COUNT(DISTINCT PR.PRD_ID) AS TOTAL
                FROM VW_PRODUTOS PR
                    INNER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PR.PRD_ID
                WHERE PR.PRD_STATUS=1 AND COALESCE(PR.PRD_PRECO,0) > 0 AND CAT_ID=:I_CAT_ID";

        if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaCategoriaProdutos($busca)
    {
        $parametros = [
            'I_CAT_ID' => $busca['CAT']
        ];

        $sql = "SELECT   DISTINCT
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_DESTAQUE,
                    TRIM(PR.PRD_NOME) AS PRD_NOME,
                    PR.PRD_PRECO,
                    PR.PRD_TAXA,
                    PR.PRD_PRECO_ANTERIOR,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    PR.PASTA_IMAGENS,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PRD_QTDE_ESTOQUE,
                    PR.PRD_STATUS,
                    (CASE
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) > 0 AND PR.PRD_STATUS = 1 THEN 1
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) = 0 AND PR.PRD_STATUS = 2 THEN 2
                        WHEN PR.PRD_STATUS = 2 THEN 3
                    ELSE 4 END) ORDENACAO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PR.PRD_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND COALESCE(PR.PRD_PRECO,0) > 0 AND CP.CAT_ID=:I_CAT_ID ";

        if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        if (isset($busca['ORDENACAO'])) {
            switch (intval($busca['ORDENACAO'])) {
                case 1:
                    $sql .= " ORDER BY 14, 4 ASC";
                    break;
                case 2:
                    $sql .= " ORDER BY PR.PRD_NOME ASC";
                    break;
                case 3:
                    $sql .= " ORDER BY PR.PRD_NOME DESC";
                    break;
                case 4:
                    $sql .= " ORDER BY COALESCE(PR.PRD_PRECO,0) ASC";
                    break;
                case 5:
                    $sql .= " ORDER BY COALESCE(PR.PRD_PRECO,0) DESC";
                    break;
                default;
                    $sql .= " ORDER BY 14, 4 ASC";
                    break;
            }
        } else {
            $sql .= " ORDER BY 14, 4 ASC";
        }

        if (intval($busca['TODOS']) == 0) {
            $sql .= " LIMIT " . $busca['OFFSET'] . ',' . $busca['ITENS'];
        }

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaCategoriasIrmas($id)
    {
        $parametros = [
            'I_CAT_ID' => $id
        ];

        $sql = "SELECT
                    CAT_ID,
                    CAT_NOME,
                    CAT_PAI,
                    CAT_URL,
                    (SELECT   GROUP_CONCAT(CAT_ID) FROM CATEGORIAS WHERE CAT_PAI=:I_CAT_ID) AS FILHOS,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                FROM CATEGORIAS
                WHERE CAT_PAI=(SELECT   CAT_PAI FROM CATEGORIAS WHERE CAT_ID=:I_CAT_ID)
                AND CAT_ID != :I_CAT_ID
                ORDER BY RAND() LIMIT 12";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InfoCategoria($id)
    {
        $parametros = [
            'I_CAT_ID' => $id
        ];

        $sql = "SELECT
                    CAT.CAT_ID,
                    CAT.CAT_NOME,
                    CAT.CAT_URL,
                    CAT.CAT_PAI,
                    CAT.CAT_STATUS,
                    CAT.CAT_IMAGEM,
                    CAT.CAT_POSICAO,
                    CAT.CAT_NIVEL,
                    (SELECT
                        COUNT(1)
                    FROM VW_PRODUTOS PRD
                        LEFT OUTER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PRD.PRD_ID
                    WHERE COALESCE(PRD.PRD_PRECO,0) > 0 AND CP.CAT_ID=CAT.CAT_ID) AS ITENS_CADASTRADOS,
                    (SELECT
                        COUNT(1)
                    FROM VW_PRODUTOS PRD
                        LEFT OUTER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PRD.PRD_ID
                    WHERE COALESCE(PRD.PRD_QTDE_ESTOQUE,0) > 0 AND COALESCE(PRD.PRD_PRECO,0) > 0 AND CP.CAT_ID=CAT.CAT_ID) AS DISPONIVEIS,
                    (SELECT
                        COUNT(1)
                    FROM VW_PRODUTOS PRD
                        LEFT OUTER JOIN CATEGORIAS_PRD CP ON CP.PRD_ID=PRD.PRD_ID
                    WHERE COALESCE(PRD.PRD_QTDE_ESTOQUE,0) = 0 AND COALESCE(PRD.PRD_PRECO,0) > 0 AND CP.CAT_ID=CAT.CAT_ID) AS INDISPONIVEIS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CATEGORIAS CAT
                WHERE CAT.CAT_STATUS= 1
                    AND CAT.CAT_ID=:I_CAT_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotalFabricanteProdutos($busca)
    {
        $parametros = [
            'I_PAR_ID' => $busca['PAR']
        ];

        $sql = "SELECT
                    COUNT(DISTINCT PR.PRD_ID) AS TOTAL
                FROM VW_PRODUTOS PR
                WHERE PR.PRD_STATUS=1 AND COALESCE(PR.PRD_PRECO,0) > 0 AND PR.PAR_ID=:I_PAR_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaFabricateProdutos($busca)
    {
        $parametros = [
            'I_PAR_ID' => $busca['PAR']
        ];

        $sql = "SELECT   DISTINCT
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_DESTAQUE,
                    TRIM(PR.PRD_NOME) AS PRD_NOME,
                    PR.PRD_PRECO,
                    PR.PRD_TAXA,
                    PR.PRD_PRECO_ANTERIOR,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    PR.PRD_QTDE_ESTOQUE,
                    PR.PASTA_IMAGENS,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PRD_STATUS,
                    (CASE
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) > 0 AND PR.PRD_STATUS = 1 THEN 1
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) = 0 AND PR.PRD_STATUS = 2 THEN 2
                        WHEN PR.PRD_STATUS = 2 THEN 3
                    ELSE 4 END) ORDENACAO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND COALESCE(PR.PRD_PRECO,0) > 0 AND PR.PAR_ID=:I_PAR_ID
                ORDER BY 13, 3 ASC ";

        $sql .= " LIMIT " . $busca['OFFSET'] . ',' . $busca['ITENS'];

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaOutrosFabricantes($id)
    {
        $parametros = [
            'I_PAR_ID' => $id
        ];

        $sql = "SELECT
                    PAR_ID,
                    PAR_URL,
                    PAR_NOME,
                    PAR_STATUS,
                    PAR_DESCRICAO,
                    IMAGEM,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                FROM PARCEIROS
                WHERE PAR_STATUS=1 AND PAR_ID != :I_PAR_ID
                ORDER BY RAND() LIMIT 12";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InfoFabricante($id)
    {
        $parametros = [
            'I_PAR_ID' => $id
        ];

        $sql = "SELECT
                    PAR_ID,
                    PAR_NOME,
                    PAR_STATUS,
                    PAR_DESCRICAO,
                    IMAGEM,
                    (SELECT   COUNT(1) FROM VW_PRODUTOS PRD WHERE PRD.PRD_STATUS=1 AND PRD.PAR_ID=:I_PAR_ID) AS ITENS_CADASTRADOS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PARCEIROS PAR
                WHERE PAR_STATUS=1
                    AND PAR.PAR_ID=:I_PAR_ID;";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InformacoesProdutosCategoria($cat)
    {
        $parametros = [
            'I_CAT_ID' => $cat
        ];

        $sql = "CALL PS_INFO_CATEGORIA(:I_CAT_ID);";
        $resultado = self::callprocedure($sql, $parametros);

        return $resultado;
    }

    public function ListaItensPedido($token, $tokenped)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TOKEN_P' => $tokenped
        ];

        $sql = "SELECT
                    PL.PED_ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    PR.PRD_MODELO,
                    PR.PRD_PRECO,
                    PR.PRD_TAXA,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    SUM(PL.QTDE) AS QTDE,
                    SUM(COALESCE((PL.QTDE*PR.PRD_PRECO),0)) AS PRECO_TOTAL,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PASTA_IMAGENS,
                    PR.PRD_QTDE_ESTOQUE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN PEDIDO_LISTA_COMPRA PL ON PL.PRD_ID=PR.PRD_ID
                    INNER JOIN PEDIDOS PE ON PE.PED_ID=PL.PED_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND PL.CLI_ID=(SELECT   US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN)
                    AND PE.TOKEN=:I_TOKEN_P
                GROUP BY PR.PRD_ID ORDER BY PR.PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function InfoResumoPedido($tokenped)
    {
        $parametros = [
            'I_TOKEN_P' => $tokenped
        ];

        $sql = "SELECT
                    EN.END_ENDERECO,
                    EN.END_NUM,
                    EN.END_COMPLEMENTO,
                    EN.END_BAIRRO,
                    EN.END_CIDADE,
                    EN.END_CEP,
                    EN.END_UF,
                    PED.BANDEIRA,
                    PED.PAY_METHOD_TYPE_STR,
                    PED.PAYMENT_LINK,
                    PED.DATE,
                    PED.STATUS,
                    PED.CODE,
                    MSG.MENSAGEM,
                    MSG.DESCRICAO,
                    PED.VALOR_FRETE,
                    ME.CODIGO_MEIO AS COD_MP_PAG,
                    MP.NOME_MEIO AS MEIO_PAG,
                    MP.IMAGEM AS MEIO_IMG,
                    ME.NOME_MEIO AS MEIO_ENT,
                    ME.IMAGEM AS MEIO_IMG_ENT,
                    PED.COMENTARIO,
                    PED.EMAIL_ENVIADO
                FROM PEDIDOS PED
                    INNER JOIN MEIOS_PAGAMENTOS MP ON MP.MEIO_ID=PED.MEIO_PAGAMENTO
                    INNER JOIN MEIOS_ENTREGA ME ON ME.MEIO_ID=PED.TIPO_ENTREGA
                    INNER JOIN ENDERECOS EN ON EN.USUARIO_ID=PED.CLI_ID
                    LEFT OUTER JOIN MSG_RETORNO_CHECKOUT MSG ON MSG.MEIO_ID=PED.MEIO_PAGAMENTO AND MSG.CODIGO=PED.STATUS
                WHERE PED.TOKEN=:I_TOKEN_P AND EN.IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaPedidosRealizados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    PED.TOKEN,
                    PED.DATA_CADASTRO,
                    PED.PED_STATUS,
                    EN.END_ENDERECO,
                    EN.END_NUM,
                    EN.END_COMPLEMENTO,
                    EN.END_BAIRRO,
                    EN.END_CIDADE,
                    EN.END_CEP,
                    EN.END_UF,
                    PED.BANDEIRA,
                    DATE_FORMAT(PED.DATA_CADASTRO, '%d/%m/%Y') as DATA_CADASTRO,
                    MP.NOME_MEIO AS MEIO_PAG,
                    MP.IMAGEM AS MEIO_IMG,
                    ME.CODIGO_MEIO AS COD_MP_PAG,
                    ME.NOME_MEIO AS MEIO_ENT,
                    ME.IMAGEM AS MEIO_IMG_ENT,
                    (CASE
                        WHEN PED.PED_STATUS = 0 THEN 'NÃO PROCESSADO'
                        WHEN PED.PED_STATUS = 1 THEN 'PROCESSADO'
                        WHEN PED.PED_STATUS = 2 THEN 'EM TRANSITO'
                        WHEN PED.PED_STATUS = 3 THEN 'DEVOLVIDO'
                        WHEN PED.PED_STATUS = 4 THEN 'CANCELADO'
                        WHEN PED.PED_STATUS = 5 THEN 'FINALIZADO'
                    END
                    ) AS STATUS_PED,
                    PED.COMENTARIO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PEDIDOS PED
                    INNER JOIN MEIOS_PAGAMENTOS MP ON MP.MEIO_ID=PED.MEIO_PAGAMENTO
                    INNER JOIN MEIOS_ENTREGA ME ON ME.MEIO_ID=PED.TIPO_ENTREGA
                    INNER JOIN ENDERECOS EN ON EN.USUARIO_ID=PED.CLI_ID
                WHERE PED.PED_STATUS IN (0, 3, 4, 5) AND PED.CLI_ID=(SELECT   US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN) AND EN.IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AtualizarPedidoSiteAPI($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DATE' => $dados['I_DATE'],
            'I_CODE' => $dados['I_CODE'],
            'I_STATUS' => $dados['I_STATUS']
        ];

        $sql = "CALL PS_ATUALIZAR_PEDIDO_API2(:I_TOKEN, :I_DATE, :I_CODE, :I_STATUS);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function ListaPedidosEmTransito($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    PED.TOKEN,
                    PED.DATA_CADASTRO,
                    PED.PED_STATUS,
                    EN.END_ENDERECO,
                    EN.END_NUM,
                    EN.END_COMPLEMENTO,
                    EN.END_BAIRRO,
                    EN.END_CIDADE,
                    EN.END_CEP,
                    EN.END_UF,
                    PED.BANDEIRA,
                    PED.CODIGOENVIO,
                    DATE_FORMAT(PED.DATA_CADASTRO, '%d/%m/%Y') as DATA_CADASTRO,
                    MP.NOME_MEIO AS MEIO_PAG,
                    MP.IMAGEM AS MEIO_IMG,
                    ME.CODIGO_MEIO AS COD_MP_PAG,
                    ME.NOME_MEIO AS MEIO_ENT,
                    ME.IMAGEM AS MEIO_IMG_ENT,
                    (CASE
                        WHEN PED.PED_STATUS = 0 THEN 'NÃO PROCESSADO'
                        WHEN PED.PED_STATUS = 1 THEN 'PROCESSADO'
                        WHEN PED.PED_STATUS = 2 THEN 'EM TRANSITO'
                        WHEN PED.PED_STATUS = 3 THEN 'DEVOLVIDO'
                        WHEN PED.PED_STATUS = 4 THEN 'CANCELADO'
                        WHEN PED.PED_STATUS = 5 THEN 'FINALIZADO'
                    END
                    ) AS STATUS_PED,
                    PED.COMENTARIO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PEDIDOS PED
                    INNER JOIN MEIOS_PAGAMENTOS MP ON MP.MEIO_ID=PED.MEIO_PAGAMENTO
                    INNER JOIN MEIOS_ENTREGA ME ON ME.MEIO_ID=PED.TIPO_ENTREGA
                    INNER JOIN ENDERECOS EN ON EN.USUARIO_ID=PED.CLI_ID
                WHERE PED.PED_STATUS IN (2) AND PED.CLI_ID=(SELECT   US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN) AND EN.IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaPedidosDevolvidos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    PED.TOKEN,
                    PED.DATA_CADASTRO,
                    PED.PED_STATUS,
                    EN.END_ENDERECO,
                    EN.END_NUM,
                    EN.END_COMPLEMENTO,
                    EN.END_BAIRRO,
                    EN.END_CIDADE,
                    EN.END_CEP,
                    EN.END_UF,
                    PED.BANDEIRA,
                    PED.CODIGOENVIO,
                    PED.DATADEVOLUCAO,
                    PED.MOTIVODEVOLUCAO,
                    DATE_FORMAT(PED.DATA_CADASTRO, '%d/%m/%Y') as DATA_CADASTRO,
                    MP.NOME_MEIO AS MEIO_PAG,
                    MP.IMAGEM AS MEIO_IMG,
                    ME.CODIGO_MEIO AS COD_MP_PAG,
                    ME.NOME_MEIO AS MEIO_ENT,
                    ME.IMAGEM AS MEIO_IMG_ENT,
                    (CASE
                        WHEN PED.PED_STATUS = 0 THEN 'NÃO PROCESSADO'
                        WHEN PED.PED_STATUS = 1 THEN 'PROCESSADO'
                        WHEN PED.PED_STATUS = 2 THEN 'EM TRANSITO'
                        WHEN PED.PED_STATUS = 3 THEN 'DEVOLVIDO'
                        WHEN PED.PED_STATUS = 4 THEN 'CANCELADO'
                        WHEN PED.PED_STATUS = 5 THEN 'FINALIZADO'
                    END
                    ) AS STATUS_PED,
                    PED.COMENTARIO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PEDIDOS PED
                    INNER JOIN MEIOS_PAGAMENTOS MP ON MP.MEIO_ID=PED.MEIO_PAGAMENTO
                    INNER JOIN MEIOS_ENTREGA ME ON ME.MEIO_ID=PED.TIPO_ENTREGA
                    INNER JOIN ENDERECOS EN ON EN.USUARIO_ID=PED.CLI_ID
                WHERE PED.PED_STATUS IN (3) AND PED.CLI_ID=(SELECT   US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN) AND EN.IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AreaAbrangenciaMeio($dados)
    {
        $parametros = [
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_CIDADE' => $dados['CIDADE'],
            'I_UF' => $dados['UF']
        ];

        $sql = "SELECT
                    COUNT(1) AS EXISTE
                FROM CIDADES_ABRANGENCIA
                WHERE MEIO_ID=:I_MEIO_ID
                    AND CIDADE=:I_CIDADE
                    AND UF=:I_UF";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaMeiosEntregaCepToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    MEIO_ID,
                    NOME_MEIO,
                    DESCRICAO_MEIO,
                    CODIGO_MEIO,
                    EMPRESA,
                    VALOR_COBRADO,
                    CALC_FRETE,
                    IMAGEM,
                    (SELECT
                        END_CEP
                     FROM ENDERECOS END
                        INNER JOIN TOKENS TK ON TK.US_ID=END.USUARIO_ID
                     WHERE IS_ENVIO=1 AND TK.TOKEN=:I_TOKEN) CEP_USER
                FROM MEIOS_ENTREGA ME WHERE ME.ME_STATUS=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }
}
