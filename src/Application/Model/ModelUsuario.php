<?php

namespace Application\Model;

use santosdummontsite\Model;

class ModelUsuario extends Model
{

    public function TotalProdutosFavoritos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    COUNT(1) AS TOTAL
                FROM VW_PRODUTOS PR
                    INNER JOIN FAVORITOS FAV ON FAV.PRD_ID=PR.PRD_ID
                    INNER JOIN TOKENS TK ON TK.US_ID=FAV.CLI_ID
                WHERE PR.PRD_STATUS=1 AND TK.TOKEN=:I_TOKEN
                ORDER BY PR.PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutosFavoritos($busca)
    {
        $parametros = [
            'I_TOKEN' => $busca['TOKEN']
        ];

        $sql = "SELECT
                    FAV.ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    PR.PRD_PRECO,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PRD_TAXA,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    PR.PASTA_IMAGENS,
                    PR.PRD_QTDE_ESTOQUE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN FAVORITOS FAV ON FAV.PRD_ID=PR.PRD_ID
                    INNER JOIN TOKENS TK ON TK.US_ID=FAV.CLI_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND TK.TOKEN=:I_TOKEN
                ORDER BY PR.PRD_ID
                LIMIT " . $busca['OFFSET'] . ',' . $busca['ITENS'];

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaCheckout($busca)
    {
        $parametros = [
            'I_TOKEN' => $busca['TOKEN']
        ];

        $sql = "SELECT
                    CC.CAR_ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    PR.PRD_MODELO,
                    PR.PRD_PRECO,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    SUM(CC.QTDE) QTDE,
                    SUM(COALESCE((CC.QTDE*PR.PRD_PRECO),0)) AS PRECO_TOTAL,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PASTA_IMAGENS,
                    PR.PRD_QTDE_ESTOQUE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                GROUP BY PR.PRD_ID";

        $sql .= " ORDER BY PR.PRD_ID LIMIT " . $busca['OFFSET'] . ',' . $busca['ITENS'];

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutosCarrinhoCliente($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    CC.CAR_ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    PR.PRD_MODELO,
                    PR.PRD_PRECO,
                    PR.PRD_PATH,
                    PR.PRD_DESTAQUE,
                    SUM(CC.QTDE) QTDE,
                    SUM(COALESCE((CC.QTDE*PR.PRD_PRECO),0)) AS PRECO_TOTAL,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.PASTA_IMAGENS,
                    PR.PRD_QTDE_ESTOQUE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                    LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID
                        AND PI.TAMANHO='image' AND PI.PRINCIPAL=1
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                GROUP BY PR.PRD_ID";

        $sql .= " ORDER BY PR.PRD_ID ASC";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function registrarIntencaoCompra($token){

        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_REGISTRO_CONTATO_CLIENTE(:I_TOKEN)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    CC.CAR_ID,
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    PR.PRD_NOME,
                    PR.PRD_PESO,
                    PR.PRD_TAXA,
                    PR.PRD_ALTURA,
                    PR.PRD_COMPRIMENTO,
                    PR.PRD_QTDE_ESTOQUE,
                    PR.PRD_LARGURA,
                    CC.QTDE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                ORDER BY PR.PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function PesoRealPedido($token, $id, $qtde)
    {
        if ($id != 0) {
            $parametros = [
                'I_PRD_ID' => $id,
                'I_QTDE' => $qtde
            ];

            $sql = "SELECT
                        PESO,
                        (CASE WHEN PESO_CUB < 0.3 THEN 0.3 ELSE PESO_CUB END) AS PESO_CUBICO,
                        0 as O_COD_RETORNO,
                        '' as O_DESC_CURTO,
                        'N' as O_TOKEN_INVALIDO
                    FROM (
                        SELECT
                            ((COALESCE(PR.PRD_PESO,0)/1000)*:I_QTDE) AS PESO,
                            (COALESCE(PR.PRD_ALTURA,0)*COALESCE(PR.PRD_LARGURA,0)*COALESCE(PR.PRD_COMPRIMENTO,0)*:I_QTDE) AS PESO_CUB
                        FROM VW_PRODUTOS PR
                        WHERE PR.PRD_STATUS=1 AND PRD_ID=:I_PRD_ID
                    ) P";
        } else {
            $parametros = [
                'I_TOKEN' => $token
            ];

            $sql = "SELECT PESO,
                        (CASE WHEN PESO_CUB < 0.3 THEN 0.3 ELSE PESO_CUB END) AS PESO_CUBICO,
                    0 as O_COD_RETORNO, '' as O_DESC_CURTO, 'N' as O_TOKEN_INVALIDO
                FROM (
                    SELECT
                        SUM(PESO) AS PESO,
                        SUM(PESO_CUB) AS PESO_CUB
                    FROM (
                        SELECT
                            ((COALESCE(PRD.PRD_PESO, 0)/1000)*CC.QTDE) as PESO,
                            (COALESCE(PRD.PRD_ALTURA,0)*COALESCE(PRD.PRD_LARGURA,0)*COALESCE(PRD.PRD_COMPRIMENTO,0)*CC.QTDE) AS PESO_CUB
                        FROM VW_PRODUTOS PRD
                            INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PRD.PRD_ID
                        WHERE PRD.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                        ORDER BY PRD.PRD_ID
                    ) PR
                ) P";
        }

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"][0];
    }

    public function ListaMeiosEntrega($token)
    {
        $parametros = [];

        $sql = "SELECT
                    MEIO_ID,
                    NOME_MEIO,
                    DESCRICAO_MEIO,
                    CODIGO_MEIO,
                    EMPRESA,
                    VALOR_COBRADO,
                    CALC_FRETE,
                    IMAGEM,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM MEIOS_ENTREGA WHERE ME_STATUS=1;";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaCartoesCliente($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    CART_ID,
                    CART_NUMERO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CARTOES CAR
                    INNER JOIN TOKENS TK ON TK.US_ID=CAR.USUARIO_ID
                WHERE CAR.CART_STATUS=1
                    AND TK.TOKEN=:I_TOKEN";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function DadosCartaoCliente($id)
    {
        $parametros = [
            'I_CART_ID' => $id
        ];

        $sql = "SELECT
                    CART_ID,
                    CART_NUMERO,
                    CART_CPF,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CARTOES
                WHERE CART_id=:I_CART_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaMeiosEntregaCepToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    MEIO_ID,
                    NOME_MEIO,
                    DESCRICAO_MEIO,
                    CODIGO_MEIO,
                    EMPRESA,
                    VALOR_COBRADO,
                    CALC_FRETE,
                    RESTRICAO_ABRANG,
                    IMAGEM,
                    (SELECT
                        END_CEP
                     FROM ENDERECOS END
                        INNER JOIN TOKENS TK ON TK.US_ID=END.USUARIO_ID
                     WHERE IS_ENVIO=1 AND TK.TOKEN=:I_TOKEN) CEP_USER
                FROM MEIOS_ENTREGA ME WHERE ME.ME_STATUS=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotaisCheckout($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    SUM(COALESCE((CC.QTDE*PR.PRD_PRECO),0)) AS PRECO_TOTAL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotaisCarrinho($id, $qtde)
    {
        $parametros = [
            'I_PRD_ID' => $id,
            'I_QTDE' => $qtde
        ];

        $sql = "SELECT
                        SUM(COALESCE((:I_QTDE*PR.PRD_PRECO),0)) AS PRECO_TOTAL,
                        0 as O_COD_RETORNO,
                        '' as O_DESC_CURTO,
                        'N' as O_TOKEN_INVALIDO
                    FROM VW_PRODUTOS PR
                    WHERE PR.PRD_STATUS=1 AND PR.PRD_ID=:I_PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotalCheckout($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    COUNT(1) AS TOTAL
                FROM VW_PRODUTOS PR
                    INNER JOIN CARRINHO_COMPRA CC ON CC.PRD_ID=PR.PRD_ID
                WHERE PR.PRD_STATUS=1 AND CC.SESSION=:I_TOKEN
                ORDER BY PR.PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function RemoverProdutoFavorito($id, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        $sql = "CALL PS_REMOVER_FAVORITO(:I_TOKEN, :I_ID)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function RemoveItemCarrinho($id, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        $sql = "CALL PS_REMOVER_ITEM_CARRINHO(:I_TOKEN, :I_ID)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AddRemQtdCarrinho($id, $qtde, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id,
            'I_QTDE' => $qtde
        ];

        $sql = "CALL PS_ADD_REM_QTDE_CARRINHO(:I_TOKEN, :I_ID, :I_QTDE)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"][0];
    }

    public function RemoverProdutoCheckout($id, $token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        $sql = "CALL PS_REMOVER_CARRINHO(:I_TOKEN, :I_ID)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AtualizarProdutosCheckout($token, $logado)
    {
        $totais = self::TotaisCheckout($token, $logado);

        $resultado = [
            'O_COD_RETORNO' => $totais[0]['O_COD_RETORNO'],
            'PRECO_TOTAL' => $totais[0]['PRECO_TOTAL']
        ];

        return $resultado;
    }

    public function AtualizarProdutosCarrinho($token, $op, $id = null, $qtde = null)
    {
        if ($op == 1) {
            $totais = self::TotaisCheckout($token);
        } else {
            $totais = self::TotaisCarrinho($id, $qtde);
        }

        $resultado = [
            'O_COD_RETORNO' => $totais[0]['O_COD_RETORNO'],
            'PRECO_TOTAL' => $totais[0]['PRECO_TOTAL']
        ];

        return $resultado;
    }

    public function CancelarCompraUsuario($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PS_CANCELAR_COMPRA_USUARIO(:I_TOKEN)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ValidaLogin($login, $senha)
    {
        $pass = strtoupper(md5($login . $senha));

        $parametros = [
            'I_CLI_EMAIL' => $login,
            'I_CLI_SENHA' => $pass
        ];

        $sql = "CALL PS_LOGIN_CLIENTES(:I_CLI_EMAIL, :I_CLI_SENHA)";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function RegistrarCliente($dados)
    {
        $parametros = [
            'I_NOME_COMPLETO' => $dados['I_NOME_COMPLETO'],
            'I_TEL_CEL' => $dados['I_TEL_CEL'],
            'I_NUM_DOC' => $dados['I_NUM_DOC'],
            'I_TIPO_DOC' => $dados['I_TIPO_DOC'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_DT_NASCIMENTO' => $dados['I_DT_NASCIMENTO'],
            'I_SENHA' => $dados['I_SENHA'],
            'I_CONF_SENHA' => $dados['I_CONF_SENHA']
        ];

        $sql = "CALL PS_REGISTRA_CLIENTES_2(:I_NOME_COMPLETO, :I_TEL_CEL, :I_NUM_DOC, :I_TIPO_DOC,
                                            :I_EMAIL, :I_DT_NASCIMENTO, :I_SENHA, :I_CONF_SENHA);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function ConcluirRegistroCliente($dados)
    {
        $parametros = [
            'I_TOKEN_REG' => $dados['I_TOKEN_REG'],
            'I_TOKEN_SES' => $dados['I_TOKEN_SES'],
            'I_LOGRADOURO' => $dados['I_LOGRADOURO'],
            'I_BAIRRO' => $dados['I_BAIRRO'],
            'I_NUMERO' => $dados['I_NUMERO'],
            'I_CEP' => $dados['I_CEP'],
            'I_CIDADE' => $dados['I_CIDADE'],
            'I_UF' => $dados['I_UF'],
            'I_PAIS' => $dados['I_PAIS'],
            'I_COMPLEMENTO' => $dados['I_COMPLEMENTO'],
            'I_TIPO' => $dados['I_TIPO'],
            'I_END_PADRAO' => $dados['I_END_PADRAO'],
            'I_ACEITA_CONDICOES' => $dados['I_ACEITA_CONDICOES']
        ];

        $sql = "CALL PS_CONCLUIR_CADASTRO_CLIENTE(:I_TOKEN_REG, :I_TOKEN_SES, :I_LOGRADOURO, :I_BAIRRO, :I_NUMERO, :I_CEP, :I_CIDADE, :I_UF,
                                            :I_PAIS, :I_COMPLEMENTO, :I_TIPO, :I_END_PADRAO, :I_ACEITA_CONDICOES);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function OrganizarCarrinho($carrinho, $token)
    {
        $parametros = [
            'I_SESSAO' => $carrinho,
            'I_TOKEN' => $token
        ];

        $sql = "CALL PS_ORGANIZAR_CARRINHO(:I_SESSAO, :I_TOKEN)";
        parent::callprocedure($sql, $parametros);
    }

    public function TrocarSenhaCliente($dados)
    {
        $parametros = [
            'I_CLI_ID' => $dados['CLI_ID'],
            'I_CLI_EMAIL' => $dados['CLI_EMAIL'],
            'I_CLI_SENHA' => $dados['CLI_SENHA'],
            'I_CLI_STATUS' => $dados['CLI_STATUS']
        ];

        $sql = "CALL PS_ALTERAR_SENHA_CLIENTE(:I_CLI_ID, :I_CLI_EMAIL, :I_CLI_SENHA, :I_CLI_STATUS)";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function ClientePorEmail($email)
    {
        $parametros = [
            'I_MAIL_ID' => $email,
        ];

        $sql = "SELECT
                    CLI.CLI_ID, CLI.PER_ID, CLI.CLI_EMAIL, CLI.CLI_STATUS,
                    CLI.CLI_IMAGEM, DC.NOME_COMPLETO,
                    (CASE WHEN DC.EMAIL != NULL THEN DC.EMAIL ELSE CLI.CLI_EMAIL END) AS EMAIL,
                    DC.GENERO,
                    (CASE
                        WHEN DC.TELEFONE_RESID != NULL THEN DC.TELEFONE_RESID
                        WHEN DC.TELEFONE_CEL != NULL THEN DC.TELEFONE_CEL
                        WHEN DC.TELEFONE_COM != NULL THEN DC.TELEFONE_COM
                    END) AS TELEFONE,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                WHERE CLI.CLI_EMAIL=:I_MAIL_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ClientePorToken($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    CLI.CLI_ID,
                    DC.NOME_COMPLETO,
                    DC.TELEFONE_CEL,
                    CLI.CLI_EMAIL AS EMAIL,
                    CLI.CLI_EMAIL,
                    DC.TIPO_DOC,
                    DC.NUM_DOC,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM CLIENTES CLI
                    LEFT OUTER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=CLI.CLI_ID
                    INNER JOIN TOKENS TK ON TK.US_ID=CLI.CLI_ID
                WHERE TK.TOKEN=:I_TOKEN";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"][0];
    }

    public function RemoveEndereco($dados)
    {
        $parametros = [
            'END_ID' => $dados['END_ID'],
        ];

        $resultado = parent::deleteData('ENDERECOS', $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function RemoveCartao($dados)
    {
        $parametros = [
            'CART_ID' => $dados['CART_ID'],
        ];

        $resultado = parent::deleteData('CARTOES', $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function EnderecoPadrao($id)
    {
        $parametros = [
            'IS_ENVIO' => 1
        ];

        $resultado = parent::updateData('ENDERECOS', $parametros, 'END_ID=' . $id);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GetEnderecoPadrao($id)
    {
        $parametros = [
            'I_USUARIO_ID' => $id
        ];

        $sql = "SELECT
                    END_ID,
                    END_ENDERECO,
                    END_BAIRRO,
                    END_CIDADE,
                    END_NUM,
                    END_CEP,
                    END_UF,
                    END_PAIS,
                    END_COMPLEMENTO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM ENDERECOS WHERE USUARIO_ID=:I_USUARIO_ID AND IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GetDadosEnderecoPadraoUsuario($id)
    {
        $parametros = [
            'I_END_ID' => $id
        ];

        $sql = "SELECT
                    END_ID,
                    END_ENDERECO,
                    END_BAIRRO,
                    END_CIDADE,
                    END_NUM,
                    END_CEP,
                    END_UF,
                    END_PAIS,
                    END_TIPO,
                    END_COMPLEMENTO,
                    DC.TELEFONE_CEL,
                    DC.NOME_COMPLETO,
                    DC.TIPO_DOC,
                    DC.NUM_DOC,
                    DC.EMAIL,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM ENDERECOS END
                    INNER JOIN DADOS_CLIENTES DC ON DC.CLI_ID=END.USUARIO_ID
                WHERE END_ID=:I_END_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GetEndereco($id)
    {
        $parametros = [
            'I_USUARIO_ID' => $id
        ];

        $sql = "SELECT
                    END_ID,
                    END_ENDERECO,
                    END_BAIRRO,
                    END_CIDADE,
                    END_NUM,
                    END_CEP,
                    END_UF,
                    END_PAIS,
                    END_COMPLEMENTO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM ENDERECOS WHERE USUARIO_ID=:I_USUARIO_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function SalvarInfoDadosPessoais($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOME_COMPLETO' => $dados['I_NOME_COMPLETO'],
            'I_TEL_RESID' => $dados['I_TEL_RESID'],
            'I_TEL_COM' => $dados['I_TEL_COM'],
            'I_TEL_CEL' => $dados['I_TEL_CEL'],
            'I_TIPO_DOC' => $dados['I_TIPO_DOC'],
            'I_NUM_DOC' => $dados['I_NUM_DOC'],
            'I_GENERO' => $dados['I_GENERO'],
            'I_DT_NASC' => $dados['I_DT_NASC'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_OCUPACAO' => $dados['I_OCUPACAO']
        ];

        $sql = "CALL PS_ALTERAR_INFO_DADOS_CLIENTE(:I_TOKEN, :I_NOME_COMPLETO, :I_TEL_RESID, :I_TEL_COM, :I_TEL_CEL, :I_TIPO_DOC, :I_NUM_DOC, :I_GENERO, :I_DT_NASC, :I_EMAIL, :I_OCUPACAO);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarFotoPessoal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_IMAGEM' => $dados['I_IMAGEM'],
            'I_PASTA' => $dados['I_PASTA']
        ];

        $sql = "CALL PS_ALTERAR_INFO_IMAGEM_CLIENTE(:I_TOKEN, :I_IMAGEM, :I_PASTA)";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarSenhaPessoal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_SENHA_ATUAL' => $dados['I_SENHA_ATUAL'],
            'I_NOVA_SENHA' => $dados['I_NOVA_SENHA'],
            'I_CONF_SENHA' => $dados['I_CONF_SENHA']
        ];

        $sql = "CALL PS_ALTERAR_INFO_SENHA_CLIENTE(:I_TOKEN, :I_SENHA_ATUAL, :I_NOVA_SENHA, :I_CONF_SENHA)";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarEnderecoPessoal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_END_ID' => $dados['I_END_ID'],
            'I_LOGRADOURO' => $dados['I_LOGRADOURO'],
            'I_COMPLEMENTO' => $dados['I_COMPLEMENTO'],
            'I_CIDADE' => $dados['I_CIDADE'],
            'I_BAIRRO' => $dados['I_BAIRRO'],
            'I_UF' => $dados['I_UF'],
            'I_NUM' => $dados['I_NUM'],
            'I_CEP' => $dados['I_CEP'],
            'I_PAIS' => $dados['I_PAIS'],
            'I_ENVIAR' => $dados['I_ENVIAR'],
            'I_TIPO_END' => $dados['I_TIPO_END'],
            'I_END_STATUS' => $dados['I_END_STATUS']
        ];

        $sql = "CALL PS_ALTERAR_INFO_END_CLIENTE(:I_TOKEN, :I_END_ID, :I_LOGRADOURO, :I_COMPLEMENTO, :I_CIDADE, :I_BAIRRO, :I_UF, :I_NUM, :I_CEP, :I_PAIS, :I_ENVIAR, :I_TIPO_END, :I_END_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarCartaoPessoal($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CART_ID' => $dados['I_CART_ID'],
            'I_TITULAR' => $dados['I_CART_NOME'],
            'I_NUM_CARTAO' => $dados['I_CART_NUMERO'],
            'I_CCV' => $dados['I_CART_CVV'],
            'I_CPF_TITULAR' => $dados['I_CPF_TITULAR'],
            'I_VALIDADE' => $dados['I_CART_VALIDADE'],
            'I_STATUS' => $dados['I_CART_STATUS'],
            'I_USO' => $dados['I_IS_USO'],
            'I_TIPO' => empty($dados['I_OPCAO']) ? 0 : 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_CART_CLIENTE(:I_TOKEN, :I_CART_ID, :I_NUM_CARTAO, :I_CCV, :I_TITULAR, :I_VALIDADE, :I_CPF_TITULAR, :I_STATUS, :I_USO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarDadosPessoaisAtivacao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOME_COMPLETO' => $dados['I_NOME_COMPLETO'],
            'I_TEL_RESID' => $dados['I_TEL_RESID'],
            'I_TEL_COM' => $dados['I_TEL_COM'],
            'I_TEL_CEL' => $dados['I_TEL_CEL'],
            'I_GENERO' => $dados['I_GENERO'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_OCUPACAO' => $dados['I_OCUPACAO'],
            'I_STATUS' => 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_DADOS_CLIENTE_REG(:I_TOKEN, :I_NOME_COMPLETO, :I_TEL_RESID, :I_TEL_COM, :I_TEL_CEL, :I_GENERO, :I_EMAIL, :I_OCUPACAO, :I_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarFotoAtivacao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_IMAGEM' => $dados['I_IMAGEM'],
            'I_PASTA' => $dados['I_PASTA'],
            'I_STATUS' => 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_IMAGEM_CLIENTE_REG(:I_TOKEN, :I_IMAGEM, :I_PASTA, :I_STATUS)";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarSenhaAtivacao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOVA_SENHA' => $dados['I_NOVA_SENHA'],
            'I_CONF_SENHA' => $dados['I_CONF_SENHA'],
            'I_STATUS' => 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_SENHA_CLIENTE_REG(:I_TOKEN, :I_NOVA_SENHA, :I_CONF_SENHA, :I_STATUS)";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarEnderecoAtivacao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_END_ID' => $dados['I_END_ID'],
            'I_LOGRADOURO' => $dados['I_LOGRADOURO'],
            'I_COMPLEMENTO' => $dados['I_COMPLEMENTO'],
            'I_CIDADE' => $dados['I_CIDADE'],
            'I_BAIRRO' => $dados['I_BAIRRO'],
            'I_UF' => $dados['I_UF'],
            'I_NUM' => $dados['I_NUM'],
            'I_CEP' => $dados['I_CEP'],
            'I_PAIS' => $dados['I_PAIS'],
            'I_ENVIAR' => $dados['I_ENVIAR'],
            'I_TIPO_END' => $dados['I_TIPO_END'],
            'I_END_STATUS' => $dados['I_END_STATUS'],
            'I_STATUS' => 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_END_CLIENTE_REG(:I_TOKEN, :I_END_ID, :I_LOGRADOURO, :I_COMPLEMENTO, :I_CIDADE, :I_BAIRRO, :I_UF, :I_NUM, :I_CEP, :I_PAIS, :I_ENVIAR, :I_TIPO_END, :I_END_STATUS, :I_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function SalvarCartaoAtivacao($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CART_ID' => $dados['I_CART_ID'],
            'I_TITULAR' => $dados['I_CART_NOME'],
            'I_NUM_CARTAO' => $dados['I_CART_NUMERO'],
            'I_CCV' => $dados['I_CART_CVV'],
            'I_VALIDADE' => $dados['I_CART_VALIDADE'],
            'I_CPF_TITULAR' => $dados['I_CPF_TITULAR'],
            'I_STATUS' => $dados['I_CART_STATUS'],
            'I_USO' => $dados['I_IS_USO'],
            'I_TIPO' => empty($dados['I_OPCAO']) ? 0 : 1
        ];

        $sql = "CALL PS_ALTERAR_INFO_CART_CLIENTE(:I_TOKEN, :I_CART_ID, :I_NUM_CARTAO, :I_CCV, :I_TITULAR, :I_VALIDADE, :I_CPF_TITULAR, :I_STATUS, :I_USO, :I_TIPO);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function GerarPedidoSite($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_END_PADRAO' => $dados['I_END_PADRAO'],
            'I_TIPO_ENTREGA' => $dados['I_TIPO_ENTREGA'],
            'I_MEIO_PAGAMENTO' => $dados['I_MEIO_PAGAMENTO'],
            'I_COMENTARIO' => $dados['I_COMENTARIO'],
            'I_BANDEIRA' => $dados['I_BANDEIRA'],
            'I_CART_ID' => $dados['I_CART_ID'],
            'I_NUM_CARTAO' => $dados['I_NUM_CARTAO'],
            'I_CVV' => $dados['I_CVV'],
            'I_TITULAR' => $dados['I_TITULAR'],
            'I_VALIDADE' => $dados['I_VALIDADE'],
            'I_CPF_TITULAR' => $dados['I_CPF_TITULAR'],
            'I_FRETE' => $dados['I_FRETE'],
            'I_PRAZO' => $dados['I_PRAZO']
        ];

        $sql = "CALL PS_GERAR_PEDIDO_CHECKOUT_2(:I_TOKEN, :I_END_PADRAO, :I_TIPO_ENTREGA, :I_MEIO_PAGAMENTO, :I_COMENTARIO,
                                            :I_BANDEIRA, :I_CART_ID, :I_NUM_CARTAO, :I_CVV, :I_TITULAR, :I_VALIDADE, :I_CPF_TITULAR,
                                            :I_FRETE, :I_PRAZO);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function AtualizarPedidoSiteAPI($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DATE' => $dados['I_DATE'],
            'I_CODE' => $dados['I_CODE'],
            'I_REFERENCE' => $dados['I_REFERENCE'],
            'I_STATUS' => $dados['I_STATUS'],
            'I_PAYMENT_METHOD_TYPE' => $dados['I_PAYMENT_METHOD_TYPE'],
            'I_PAY_METHOD_TYPE_STR' => $dados['I_PAY_METHOD_TYPE_STR'],
            'I_PAYMENT_METHOD_CODE' => $dados['I_PAYMENT_METHOD_CODE'],
            'I_PAYMENT_LINK' => $dados['I_PAYMENT_LINK'],
            'I_ITEM_COUNT' => $dados['I_ITEM_COUNT']
        ];

        $sql = "CALL PS_ATUALIZAR_PEDIDO_API(:I_TOKEN, :I_DATE, :I_CODE, :I_REFERENCE, :I_STATUS, :I_PAYMENT_METHOD_TYPE, :I_PAY_METHOD_TYPE_STR, :I_PAYMENT_METHOD_CODE, :I_PAYMENT_LINK, :I_ITEM_COUNT);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function ListaProdutosPedido($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PED_ID' => $id
        ];

        $sql = "SELECT
                    PED_ID,
                    PRD_ID,
                    PRD_NOME,
                    PRD_MODELO,
                    PRD_PRECO,
                    PASTA_IMAGENS,
                    PRD_PATH,
                    PRD_DESTAQUE,
                    QTDE,
                    PESO,
                    PESO_CUBICO,
                    PRECO_TOTAL,
                    O_COD_RETORNO,
                    O_DESC_CURTO,
                    O_TOKEN_INVALIDO
                FROM VW_ITENS_PESO_CUBICO
                WHERE PED_ID=:I_PED_ID AND TOKEN=:I_TOKEN
                GROUP BY PRD_ID ORDER BY PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaPedidos($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "SELECT
                    PED.TOKEN,
                    PED.DATA_CADASTRO,
                    PED.PED_STATUS,
                    EN.END_ENDERECO,
                    EN.END_NUM,
                    EN.END_COMPLEMENTO,
                    EN.END_BAIRRO,
                    EN.END_CIDADE,
                    EN.END_CEP,
                    EN.END_UF,
                    PED.BANDEIRA,
                    DATE_FORMAT(PED.DATA_CADASTRO, '%d/%m/%Y') as DATA_CADASTRO,
                    MP.NOME_MEIO AS MEIO_PAG,
                    MP.IMAGEM AS MEIO_IMG,
                    ME.CODIGO_MEIO AS COD_MP_PAG,
                    ME.NOME_MEIO AS MEIO_ENT,
                    ME.IMAGEM AS MEIO_IMG_ENT,
                    (CASE
                        WHEN PED.PED_STATUS = 0 THEN 'NÃO PROCESSADO'
                        WHEN PED.PED_STATUS = 1 THEN 'PROCESSADO'
                        WHEN PED.PED_STATUS = 2 THEN 'EM TRANSITO'
                        WHEN PED.PED_STATUS = 3 THEN 'DEVOLVIDO'
                        WHEN PED.PED_STATUS = 4 THEN 'CANCELADO'
                        WHEN PED.PED_STATUS = 5 THEN 'FINALIZADO'
                    END
                    ) AS STATUS_PED,
                    PED.COMENTARIO
                FROM PEDIDOS PED
                    INNER JOIN MEIOS_PAGAMENTOS MP ON MP.MEIO_ID=PED.MEIO_PAGAMENTO
                    INNER JOIN MEIOS_ENTREGA ME ON ME.MEIO_ID=PED.TIPO_ENTREGA
                    INNER JOIN ENDERECOS EN ON EN.USUARIO_ID=PED.CLI_ID
                WHERE PED.PED_STATUS IN (0, 1, 2) AND PED.CLI_ID=(SELECT   US_ID FROM TOKENS WHERE TOKEN=:I_TOKEN) AND EN.IS_ENVIO=1";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function DadosMeioEntrega($codigo)
    {
        $parametros = [
            'I_CODIGO' => $codigo
        ];

        $sql = "SELECT
                    MEIO_ID,
                    NOME_MEIO,
                    DESCRICAO_MEIO,
                    IMAGEM,
                    CODIGO_MEIO,
                    EMPRESA,
                    VALOR_COBRADO,
                    PRAZO_MAXIMO,
                    CALC_FRETE,
                    RESTRICAO_ABRANG,
                    DATA_CAD,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM MEIOS_ENTREGA
                WHERE CODIGO_MEIO=:I_CODIGO";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function AreaAbrangenciaMeio($dados)
    {
        $parametros = [
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_CIDADE' => $dados['CIDADE'],
            'I_UF' => $dados['UF']
        ];

        $sql = "SELECT
                    COUNT(1) AS EXISTE
                FROM CIDADES_ABRANGENCIA
                WHERE MEIO_ID=:I_MEIO_ID
                    AND CIDADE=:I_CIDADE
                    AND UF=:I_UF";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function CidadesAbrangenciaMeio($dados)
    {
        $parametros = [
            'I_CIDADE' => $dados['CIDADE'],
            'I_UF' => $dados['UF']
        ];

        $sql = "SELECT
                    MEIO_ID
                FROM MEIOS_ENTREGA
                WHERE MEIO_ID NOT IN(
                    SELECT
                        ME.MEIO_ID
                    FROM MEIOS_ENTREGA ME
                        LEFT OUTER JOIN CIDADES_ABRANGENCIA CA ON CA.MEIO_ID=ME.MEIO_ID
                    WHERE (CA.CIDADE=:I_CIDADE AND CA.UF=:I_UF) OR ME.CALC_FRETE=1)";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function VerificaEndCompleto($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
        ];

        $sql = "CALL PS_VERIFICA_DADOS_CLIENTE(:I_TOKEN);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function RefazerPedidoCliente($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PED_TOKEN' => $dados['PED_TOKEN'],
        ];

        $sql = "CALL PS_REFAZER_PEDIDO_CLIENTE(:I_TOKEN, :I_PED_TOKEN);";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'][0];
    }

    public function BuscarEndereco($token, $id, $tipo = null)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ID' => $id
        ];

        if (intval($tipo) != 719680) {
            $sql = "SELECT
                        EN.END_ENDERECO, EN.END_NUM, EN.END_BAIRRO, EN.END_COMPLEMENTO, EN.END_CIDADE, EN.END_UF, EN.END_CEP
                    FROM ENDERECOS EN
                        INNER JOIN CLIENTES CLI ON EN.USUARIO_ID=CLI.CLI_ID
                        INNER JOIN TOKENS TK ON TK.US_ID=CLI.CLI_ID
                    WHERE EN.END_ID=:I_ID AND TK.TOKEN=:I_TOKEN";

            $resultado = parent::selectData($sql, $parametros);

            if ($resultado['erro'] == 1) {
                return 0;
            }

            return ['TIPO' => 0, 'DADOS' => $resultado["list"][0]];
        } else {
            $sql = "SELECT
                        ENDERECO AS END_ENDERECO, BAIRRO AS END_BAIRRO, NUMERO AS END_NUM,
                        CIDADE AS END_CIDADE, UF AS END_UF, CEP AS END_CEP, COMPLEMENTO AS END_COMPLEMENTO
                    FROM CONTATO";

            $resultado = parent::selectData($sql, $parametros);

            if ($resultado['erro'] == 1) {
                return 0;
            }

            return ['TIPO' => 1, 'DADOS' => $resultado["list"][0]];
        }
    }
}
