<?php

namespace Application\Model;

use santosdummontsite\Model;

class ModelHome extends Model
{
    public function EstoqueProduto($id)
    {
        $parametros = [
            'I_PRD_ID' => $id
        ];

        $sql = "SELECT
                    PRD_QTDE_ESTOQUE,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                FROM VW_PRODUTOS
                WHERE PRD_STATUS=1 AND PRD_ID=:I_PRD_ID";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaBannersCadastrados()
    {
        $parametros = [];

        $sql = "SELECT
                    BAN_ID,
                    IMAGEM,
                    BAN_DESCRICAO,
                    LINK,
                    LOCAL_LINK,
                    BAN_POS_TEXTOS,
                    BAN_TEXTO_PRINCIPAL,
                    BAN_EFEITO_TXT_PRINC,
                    BAN_COR_TXT_PRI,
                    BAN_SEG_TEXTO,
                    BAN_EFEITO_SEG_TEXTO,
                    BAN_COR_TXT_SEC,
                    BAN_TER_TEXTO,
                    BAN_EFEITO_TER_TEXTO,
                    BAN_COR_TXT_TER,
                    BAN_STATUS,
                    BAN_BT_TEXTO,
                    BAN_BT_COR,
                    PASTA_IMAGENS,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                FROM VW_BANNERS_CADASTRADOS";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaProdutosDestaques()
    {
        $parametros = [];

        $sql = "CALL PRC_PRD_DESTAQUE();";

        $resultado = parent::callprocedure($sql, $parametros);

        return $resultado['list'];
    }

    public function ListaCategoriasPrincipais()
    {
        $parametros = [];

        $sql = "SELECT
                    CAT_ID,
                    CAT_NOME,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    'N' AS O_TOKEN_INVALIDO
                FROM CATEGORIAS CT1
                WHERE CAT_PAI=0";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function ListaModaisCadastrados()
    {
        $parametros = [];

        $sql = "SELECT
                    MOD_ID,
                    MOD_LINK,
                    MOD_DESCRICAO,
                    MOD_IMAGEM,
                    MOD_PASTA_IMAGENS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_MODAIS_RAND";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TotalBusca($busca)
    {
        $termo = $busca['TERMO'];

        $sql = "SELECT DISTINCT
                    COUNT(1) AS TOTAL
                FROM VW_PRODUTOS PR
                WHERE PR.PRD_STATUS=1 AND (
                    PR.PRD_NOME LIKE '%" . $termo . "%'
                    OR PR.PRD_FICHA_TECNICA LIKE '%" . $termo . "%'
                    OR PR.PRD_SKU LIKE '%" . $termo . "%'
                    OR PR.PRD_DESCRICAO_MINIMA LIKE '%" . $termo . "%'
                    OR PR.PRD_NOME_TECNICO LIKE '%" . $termo . "%'
                    OR PR.PRD_MARCA LIKE '%" . $termo . "%'
                    OR PR.PRD_MODELO LIKE '%" . $termo . "%'
                    OR PR.PRD_TAGS LIKE '%" . $termo . "%') ";
        if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        if (isset($busca['GRUPO']) && $busca['GRUPO'] != null) {
            $grupos = explode(',', $busca['GRUPO']);

            $ct = 0;

            $sql .= " AND (";
            foreach ($grupos as $value) {
                $ct != 0 ? $sql .= ' OR ' : '';
                $sql .= " PR.PRD_GRUPO='" . $value . "' ";
                $ct++;
            }
            $sql .= ") ";
        }

        $resultado = parent::selectData($sql);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function GravaBusca($termo)
    {
        $parametros = [
            'I_TERMO' => $termo,
        ];

        $sql = "CALL PRC_SALVAR_TERMO_BUSCA(:I_TERMO);";
        self::callprocedure($sql, $parametros);
    }

    public function ResultadosBusca($busca)
    {
        $termo = $busca['TERMO'];

        $sql = "SELECT
                    DISTINCT
                    PR.PRD_ID,
                    PR.PRD_SKU,
                    COALESCE(PR.PRD_DESTAQUE,0) AS PRD_DESTAQUE,
                    TRIM(PR.PRD_NOME) AS PRD_NOME,
                    PR.PRD_PRECO,
                    COALESCE(PR.PRD_PRECO_ANTERIOR,0) AS PRD_PRECO_ANTERIOR,
                    COALESCE(PR.PRD_TAXA, 0) AS PRD_TAXA,
                    PR.PRD_PATH,
                    PR.PASTA_IMAGENS,
                    COALESCE(PR.PRD_QTDE_ESTOQUE, 0) AS PRD_QTDE_ESTOQUE,
                    COALESCE(PI.IMAGEM, PR.PRD_IMAGEM_PRINCIPAL) PRD_IMAGEM_PRINCIPAL,
                    PR.IMAGENS,
                    PR.PRD_STATUS,
                    (CASE
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) > 0 AND PR.PRD_STATUS = 1 THEN 1
                        WHEN COALESCE(PR.PRD_QTDE_ESTOQUE,0) = 0 AND PR.PRD_STATUS = 2 THEN 2
                        WHEN PR.PRD_STATUS = 2 THEN 3
                    ELSE 4 END) ORDEM,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                LEFT OUTER JOIN PRODUTOS_IMAGENS PI ON PI.PRD_ID=PR.PRD_ID 
                                                           AND PI.TAMANHO='image' 
                                                           AND PI.PRINCIPAL=1 
                                                           AND PI.IMAGEM LIKE '%.wepb%'
                WHERE PR.PRD_STATUS=1 AND (
                    PR.PRD_NOME LIKE '%" . $termo . "%'
                    OR PR.PRD_FICHA_TECNICA LIKE '%" . $termo . "%'
                    OR PR.PRD_SKU LIKE '%" . $termo . "%'
                    OR PR.PRD_DESCRICAO_MINIMA LIKE '%" . $termo . "%'
                    OR PR.PRD_NOME_TECNICO LIKE '%" . $termo . "%'
                    OR PR.PRD_MARCA LIKE '%" . $termo . "%'
                    OR PR.PRD_MODELO LIKE '%" . $termo . "%'
                    OR PR.PRD_TAGS LIKE '%" . $termo . "%')";

        if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        if (isset($busca['GRUPO']) && $busca['GRUPO'] != null) {
            $grupos = explode(',', $busca['GRUPO']);

            $ct = 0;

            $sql .= " AND (";
            foreach ($grupos as $value) {
                $ct != 0 ? $sql .= ' OR ' : '';
                $sql .= " PR.PRD_GRUPO='" . $value . "' ";
                $ct++;
            }
            $sql .= ") ";
        }

        if (isset($busca['ORDENACAO'])) {
            switch (intval($busca['ORDENACAO'])) {
                case 1:
                    $sql .= " ORDER BY 15, 4 ASC";
                    break;
                case 2:
                    $sql .= " ORDER BY PR.PRD_NOME ASC";
                    break;
                case 3:
                    $sql .= " ORDER BY PR.PRD_NOME DESC";
                    break;
                case 4:
                    $sql .= " ORDER BY COALESCE(PR.PRD_PRECO,0) ASC";
                    break;
                case 5:
                    $sql .= " ORDER BY COALESCE(PR.PRD_PRECO,0) DESC";
                    break;
                default;
                    $sql .= " ORDER BY 15, 4 ASC";
                    break;
            }
        } else {
            $sql .= " ORDER BY 15, 4 ASC";
        }

        if (intval($busca['TODOS']) == 0) {
            $sql .= " LIMIT " . $busca['OFFSET'] . ',' . $busca['ITENS'];
        }

        $resultado = parent::selectData($sql);
        if ($resultado['erro'] == 1) {
            return 0;
        }
        return $resultado["list"];
    }

    public function TotaisBusca($busca)
    {
        $termo = $busca['TERMO'];

        $sql = "SELECT DISTINCT
                    SUM(CASE WHEN PR.EM_ESTOQUE = 1 THEN 1 ELSE 0 END) AS DISPONIVEIS,
                    SUM(CASE WHEN PR.EM_ESTOQUE = 0 THEN 1 ELSE 0 END) AS INDISPONIVEIS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                WHERE PR.PRD_STATUS=1 AND (
                    PR.PRD_NOME LIKE '%" . $termo . "%'
                    OR PR.PRD_FICHA_TECNICA LIKE '%" . $termo . "%'
                    OR PR.PRD_SKU LIKE '%" . $termo . "%'
                    OR PR.PRD_DESCRICAO_MINIMA LIKE '%" . $termo . "%'
                    OR PR.PRD_NOME_TECNICO LIKE '%" . $termo . "%'
                    OR PR.PRD_MARCA LIKE '%" . $termo . "%'
                    OR PR.PRD_MODELO LIKE '%" . $termo . "%'
                    OR PR.PRD_TAGS LIKE '%" . $termo . "%')";
        /*if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }*/

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        if (isset($busca['GRUPO']) && $busca['GRUPO'] != null) {
            $grupos = explode(',', $busca['GRUPO']);

            $ct = 0;

            $sql .= " AND (";
            foreach ($grupos as $value) {
                $ct != 0 ? $sql .= ' OR ' : '';
                $sql .= " PR.PRD_GRUPO='" . $value . "' ";
                $ct++;
            }
            $sql .= ") ";
        }

        $resultado = parent::selectData($sql);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function BuscaGrupos($busca)
    {
        $termo = $busca['TERMO'];

        $sql = "SELECT DISTINCT
                    PR.PRD_GRUPO,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM VW_PRODUTOS PR
                WHERE PR.PRD_STATUS=1 AND (
                    PR.PRD_NOME LIKE '%" . $termo . "%'
                    OR PR.PRD_FICHA_TECNICA LIKE '%" . $termo . "%'
                    OR PR.PRD_SKU LIKE '%" . $termo . "%'
                    OR PR.PRD_DESCRICAO_MINIMA LIKE '%" . $termo . "%'
                    OR PR.PRD_NOME_TECNICO LIKE '%" . $termo . "%'
                    OR PR.PRD_MARCA LIKE '%" . $termo . "%'
                    OR PR.PRD_MODELO LIKE '%" . $termo . "%'
                    OR PR.PRD_TAGS LIKE '%" . $termo . "%')";
        /*if (intval($busca['DISP']) > 0 || intval($busca['INDISP']) > 0) {

            $sql .= " AND PR.PRD_QTDE_ESTOQUE";

            if (intval($busca['DISP']) > 0) {
                $sql .= ">";
            }

            if (intval($busca['INDISP']) > 0) {
                $sql .= "=";
            }

            $sql .= "0";
        }*/

        if (isset($busca['PRECOI']) && isset($busca['PRECOF'])) {
            $sql .= " AND COALESCE(PR.PRD_PRECO,0) BETWEEN " . $busca['PRECOI'] . " AND " . $busca['PRECOF'];
        } else if (isset($busca['PRECOI']) || isset($busca['PRECOF'])) {
            if (isset($busca['PRECOI'])) {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOI'];
            } else {
                $sql .= " AND COALESCE(PR.PRD_PRECO,0) <= " . $busca['PRECOF'];
            }
        }

        if (isset($busca['GRUPO']) && $busca['GRUPO'] != null) {
            $grupos = explode(',', $busca['GRUPO']);

            $ct = 0;

            $sql .= " AND (";
            foreach ($grupos as $value) {
                $ct != 0 ? $sql .= ' OR ' : '';
                $sql .= " PR.PRD_GRUPO='" . $value . "' ";
                $ct++;
            }
            $sql .= ") ";
        }

        $resultado = parent::selectData($sql);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }
    /* PARCEIROS */

    public function DadosParceiros($id)
    {
        $parametros = [
            'I_PAR_ID' => $id
        ];

        $sql = "SELECT
                    PAR_ID,
                    PAR_NOME,
                    PAR_URL,
                    PAR_STATUS,
                    PAR_DESCRICAO,
                    PAR_INFO, IMAGEM,
                    PASTA_IMAGENS,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PARCEIROS
                    WHERE PAR_ID=:I_PAR_ID
                ORDER BY PAR_NOME";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function RemoverPedidoCliente($logado, $tokped, $tokcliente = null)
    {
    }
}
