<?php

namespace Application\Model;

use santosdummontsite\Model,
    santosdummontsite\Common;

class ModelTestes extends Model
{

    public function TesteAjustarPathProdutos()
    {

        $parametros = [];

        $sql = "SELECT
                    PR.PRD_ID,
                    PR.PRD_NOME,
                    0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO,
                    'N' as O_TOKEN_INVALIDO
                FROM PRODUTOS PR
                WHERE PRD_ID BETWEEN 10000 AND 13063
                ORDER BY PRD_ID ASC";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        foreach ($resultado["list"] as $row) {
            $parametros = [
                'PRD_PATH' => Common::removerEspacosPontos($row['PRD_NOME'])
            ];

            $resultado = parent::updateData('PRODUTOS', $parametros, 'PRD_ID=' . $row['PRD_ID']);
        }
    }

    public function TesteListaImagens($in, $out)
    {
        $parametros = [
            'IN' => $in,
            'OUT' => $out,
        ];

        $sql = "SELECT
                    PRD_ID,
                    IMAGEM,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    'N' AS O_TOKEN_INVALIDO
                FROM PRODUTOS_IMAGENS IMG LIMIT :IN, :OUT";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteListaGaregorias()
    {
        $parametros = [];

        $sql = "SELECT
                    CAT_ID,
                    CAT_NOME,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                 FROM CATEGORIAS";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteListaProdutosPathVazios()
    {
        $parametros = [];

        $sql = "SELECT
                    PRD_ID,
                    PRD_NOME,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                 FROM PRODUTOS WHERE PRD_PATH=''";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteUpdateCategoria($path, $id)
    {
        $parametros = [
            'CAT_URL' => $path
        ];

        $resultado = parent::updateData('CATEGORIAS', $parametros, 'CAT_ID=' . $id);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteUpdateProdutoPath($path, $id)
    {
        $parametros = [
            'PRD_PATH' => $path
        ];

        $resultado = parent::updateData('PRODUTOS', $parametros, 'PRD_ID=' . $id);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteListaProdutosComPastaImagens()
    {
        $parametros = [];

        $sql = "SELECT
                    PRD_ID,
                    PASTA_IMAGENS,
                    PRD_IMAGEM_PRINCIPAL,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                 FROM PRODUTOS
                 WHERE PASTA_IMAGENS IS NOT NULL
                    AND PRD_IMAGEM_PRINCIPAL IS NOT NULL";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteListaProdutosSemPastaImagens()
    {
        $parametros = [];

        $sql = "SELECT
                    PRD_ID,
                    PASTA_IMAGENS,
                    PRD_IMAGEM_PRINCIPAL,
                    0 AS O_COD_RETORNO,
                    '' AS O_DESC_CURTO,
                    '' AS O_TOKEN_INVALIDO
                 FROM PRODUTOS
                 WHERE PASTA_IMAGENS IS NULL
                    AND PRD_IMAGEM_PRINCIPAL IS NOT NULL";

        $resultado = self::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteUpdatePastaImagemProduto($path, $id)
    {
        $parametros = [
            'PASTA_IMAGENS' => $path
        ];

        $resultado = parent::updateData('PRODUTOS', $parametros, 'PRD_ID=' . $id);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteListaImagensProdutosLimite($in, $out)
    {
        $parametros = [
            'IN' => $in,
            'OUT' => $out
        ];

        $sql = "SELECT
                PI.PRD_ID,
                PR.PRD_NOME,
                PI.IMAGEM,
                0 as O_COD_RETORNO,
                '' as O_DESC_CURTO,
                'N' as O_TOKEN_INVALIDO
                FROM PRODUTOS_IMAGENS PI INNER JOIN VW_PRODUTOS PR ON PR.PRD_ID=PI.PRD_ID
                LIMIT :IN, :OUT";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }

    public function TesteUpdateProduto($file, $pasta, $id, $tamanho)
    {
        $parametros = [
            'IMAGEM' => $file,
            'PASTAS' => $pasta,
            'TAMANHO' => $tamanho
        ];

        $resultado = parent::updateData('PRODUTOS_IMAGENS', $parametros, 'PRD_ID=' . $id);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado["list"];
    }
}
