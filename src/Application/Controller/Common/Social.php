<?php
namespace Application\Controller\Common;

use santosdummontsite\Controller;

require_once SYSTEM_PATH . 'vendor/santosdummontsite/vendor/autoload.php';

class Social extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    public function LoadAPISocial()
    {
        $config = $this->modelsistema->Configuracoes()[0];

        $ggclient = new \Google_Client();
        $ggclient->setApplicationName('Login Santos Dumont');
        $ggclient->setClientId(GOOGLE_CLIENT_ID);
        $ggclient->setClientSecret(GOOGLE_CLIENT_SECRET);
        $ggclient->setRedirectUri(GOOGLE_REDIRECT_URL);

        $gghelper = new Oauth($ggclient);

        $fb = new \Facebook\Facebook([
            'app_id' => $config['FACEBOOK_ID'],
            'app_secret' => $config['APP_SECRET_ID'],
            'default_graph_version' => 'v2.10'
        ]);

        $fbhelper = $fb->getRedirectLoginHelper();
        $dados['fb'] = $fb;
        $dados['helperfacebook'] = $fbhelper;
        $dados['helpergoogle'] = $gghelper;

        return $dados;
    }
}
