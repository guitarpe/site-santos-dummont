<?php
namespace Application\Controller\Common;

use santosdummontsite\Controller,
    santosdummontsite\Common;

class CreditcardValidator extends Controller
{

    private $data = [];

    public function __construct($tp, $num, $cvv, $mes, $ano)
    {
        parent::__construct();

        $this->data['cardnumber'] = preg_replace("/[^0-9]/", "", $num);
        $this->data['month'] = $mes;
        $this->data['year'] = $ano;
        $this->data['cvv'] = $cvv;

        switch ($tp) {
            case 1:
                $this->data['tipo'] = 'visa';
                break;
            case 2:
                $this->data['tipo'] = 'mastercard';
                break;
            case 3:
                $this->data['tipo'] = 'amex';
                break;
            case 4:
                $this->data['tipo'] = 'jcb';
                break;
            case 5:
                $this->data['tipo'] = 'elo';
                break;
            case 6:
                $this->data['tipo'] = 'diners';
                break;
            case 7:
                $this->data['tipo'] = 'discover';
                break;
            case 8:
                $this->data['tipo'] = 'aura';
                break;
        }
    }

    public function validarCartao()
    {
        $retorno = self::isValidCardNumber();

        if (!boolval($retorno['error'])) {

            $retorno = self::checkExpDate();

            if (!boolval($retorno['error'])) {

                $retorno = self::validateCVV();

                if (!boolval($retorno['error'])) {
                    return ['error' => false, 'message' => ''];
                } else {
                    return ['error' => true, 'message' => $retorno['message']];
                }
            } else {
                return ['error' => true, 'message' => $retorno['message']];
            }
        } else {
            return ['error' => true, 'message' => $retorno['message']];
        }
    }

    private function checkSum($ccnum)
    {
        $checksum = 0;

        for ($i = (2 - (strlen($ccnum) % 2)); $i <= strlen($ccnum); $i += 2) {
            $checksum += (int) ($ccnum{$i - 1});
        }

        for ($i = (strlen($ccnum) % 2) + 1; $i < strlen($ccnum); $i += 2) {
            $digit = (int) ($ccnum{$i - 1}) * 2;
            if ($digit < 10) {
                $checksum += $digit;
            } else {
                $checksum += ($digit - 9);
            }
        }

        if (($checksum % 10) == 0) {
            return ['error' => false, 'mensage' => ''];
        } else {
            return ['error' => true, 'mensage' => 'Número de cartão inválido!'];
        }
    }

    private function isValidCardNumber()
    {
        $creditcard = array(
            'visa' => '/^4\d{12}(\d{3})?$/',
            'mastercard' => '/^(5[1-5]\d{4}|677189)\d{10}$/',
            'diners' => '/^3(0[0-5]|[68]\d)\d{11}$/',
            'discover' => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
            'elo' => '/^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|627780|636297|636368|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))$/',
            'amex' => '/^3[47]\d{13}$/',
            'jcb' => '/^(?:2131|1800|35\d{3})\d{11}$/',
            'aura' => '/^(5078\d{2})(\d{2})(\d{11})$/',
            'hipercard' => '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
            'maestro' => '/^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/',
        );

        $match = false;

        foreach ($creditcard as $type => $pattern) {
            if (preg_match($pattern, $this->data['cardnumber']) === 1) {
                $match = true;
                $this->data['tipo'] = $type;
                break;
            }
        }

        if (!$match) {
            return ['error' => true, 'mensage' => 'O Cartão não corresponde a bandeira selecionada!'];
        } else {
            return self::checkSum($this->data['cardnumber']);
        }
    }

    private function checkExpDate()
    {
        $expTs = mktime(0, 0, 0, $this->data['month'] + 1, 1, $this->data['year']);
        $curTs = time();
        $maxTs = $curTs + (10 * 365 * 24 * 60 * 60);

        if ($expTs > $curTs && $expTs < $maxTs) {
            return ['error' => false, 'mensage' => ''];
        } else {
            return ['error' => true, 'mensage' => 'Data de Validade inválida'];
        }
    }

    private function validateCVV()
    {
        $count = ($this->data['tipo'] === 'amex') ? 4 : 3;

        if (preg_match('/^[0-9]{' . $count . '}$/', $this->data['cvv'])) {
            return ['error' => false, 'mensage' => ''];
        } else {
            return ['error' => true, 'mensage' => 'Número de CVV inválido'];
        }
    }
}
