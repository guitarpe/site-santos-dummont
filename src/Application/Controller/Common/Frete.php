<?php
namespace Application\Controller\Common;

use santosdummontsite\Controller;

class Frete extends Controller
{

    const ERR_DESCONHECIDO = -1;
    const ERR_CONSULTA_WEBSERVICE = 0;
    const ERR_XML_INVALIDO = 1;
    const ERR_SERVIDO_INDISPONIVEL = 7;

    private $servico;
    private $CEPOrigem;
    private $CEPDestino;
    private $produtosPedido;
    private $maoPropria = 'n';
    private $valorDeclarado = 0;
    private $avisoRecebimento = 'n';
    private $freteTotal = null;
    private $prazoTotal = null;
    private $mensagem = null;
    private $erro = null;

    public function __construct(
    $servico, $cepOrigem, $cepDestino, $produtosPedido, $maoPropria = false, $valorDeclarado = 0, $avisoRecebimento = false
    )
    {
        if (is_string($servico) === false) {
            throw new \InvalidArgumentException('A identificação do serviço dos Correios deve ser uma string.');
        }
        
        if (is_bool($maoPropria) === false) {
            throw new \InvalidArgumentException(
            'A identificação do uso do serviço adicional "mão própria" deve ser um valor booleano.'
            );
        }
        
        if (is_numeric($valorDeclarado) === false) {
            throw new \InvalidArgumentException(
            'A identificação do uso do serviço adicional "valor declarado" deve ser um valor booleano.'
            );
        }
        
        if (is_bool($avisoRecebimento) === false) {
            throw new \InvalidArgumentException(
            'A identificação do uso do serviço adicional "aviso de recebimento" deve ser um valor booleano.'
            );
        }

        $this->servico = $servico;
        $this->CEPOrigem = $this->formataCEP($cepOrigem);
        $this->CEPDestino = $this->formataCEP($cepDestino);
        $this->produtosPedido = $produtosPedido;
        $this->maoPropria = $this->formataValorBooleano($maoPropria);
        $this->valorDeclarado = $valorDeclarado;
        $this->avisoRecebimento = $this->formataValorBooleano($avisoRecebimento);
    }

    private function formataCEP($cep)
    {
        return preg_replace('#[^0-9]#', '', $cep);
    }

    private function formataValorBooleano($valor)
    {
        return ($valor) ? 's' : 'n';
    }

    public function formataValorEmReais($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return (float) $valor;
    }

    public function valorFinal()
    {
        return $this->freteTotal;
    }

    public function valorPrazo()
    {
        return $this->prazoTotal;
    }

    public function mensagemErro()
    {
        return $this->mensagem;
    }

    public function codErro()
    {
        return $this->erro;
    }

    public function calcular($retornaUrl = false)
    {
        if (is_bool($retornaUrl) === false) {
            throw new \InvalidArgumentException(
            'A identificação do retorno da URL de consulta deve ser um valor booleano.'
            );
        }

        $pesoTotal = 0.00;
        $volumeTotal = 0;

        $pesoTotal = $this->produtosPedido['PESO_REAL'];
        $volumeTotal = $this->produtosPedido['VOLUME_TOTAL'];
        $medida = ceil(pow($volumeTotal, (1 / 3)));

        if (($medida > 66) or ( $pesoTotal > 30)) {
            if ($pesoTotal > 30 && $medida < 67) {
                $retornoapi = $this->trasnbordoDePeso($retornaUrl, $pesoTotal, $medida);
            } elseif ($pesoTotal > 30 && $medida > 66) {
                $retornoapi = $this->transbordoDePesoEMedida($retornaUrl, $pesoTotal, $medida);
            }
        } else {
            $retornoapi = $this->obterValorDoFrete($retornaUrl, $pesoTotal, $medida, $medida, $medida);
        }

        $this->freteTotal = $retornoapi['VALOR'];
        $this->prazoTotal = $retornoapi['PRAZO'];
        $this->mensagemErro = $retornoapi['MSG'];
        $this->codErro = $retornoapi['ERRO'];

        return $this->valorFinal();
    }

    private function obterValorDoFrete($retornaUrl, $peso, $altura, $largura, $comprimento)
    {
        $valores = array(
            'nCdEmpresa' => "",
            'sDsSenha' => "",
            'sCepOrigem' => $this->CEPOrigem,
            'sCepDestino' => $this->CEPDestino,
            'nVlPeso' => $peso,
            'nCdFormato' => 1,
            'nVlComprimento' => $comprimento,
            'nVlAltura' => $altura,
            'nVlLargura' => $largura,
            'sCdMaoPropria' => $this->maoPropria,
            'nVlValorDeclarado' => $this->valorDeclarado,
            'sCdAvisoRecebimento' => $this->avisoRecebimento,
            'nCdServico' => $this->servico,
            'nVlDiametro' => 0,
            'StrRetorno' => 'xml',
            'nIndicaCalculo' => 3
        );
        $webserviceUrl = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
        $queryString = http_build_query($valores);

        $url = "{$webserviceUrl}?{$queryString}";

        if ($retornaUrl) {
            return $url;
        }

        $cURL = curl_init();
        curl_setopt_array($cURL, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $respostaWebservice = curl_exec($cURL);
        curl_close($cURL);

        if ($respostaWebservice === false) {
            throw new \RuntimeException(
            'Não foi possível obter uma resposta do Webservice dos Correios', self::ERR_CONSULTA_WEBSERVICE
            );
        }

        $resposta = simplexml_load_string($respostaWebservice);
        if ($resposta === false) {
            throw new \RuntimeException(
            'Não foi possível reconhecer o XML na resposta do Webservice dos Correios. ' .
            'Talvez algum parâmetro enviado esteja inválido.', self::ERR_XML_INVALIDO
            );
        }

        $dadosFrete = $resposta->cServico;

        switch ((string) $dadosFrete->Erro) {
            case 0:
                $retorno['ERRO'] = 0;
                $retorno['MSG'] = $dadosFrete->obsFim;
                $retorno['PRAZO'] = json_decode($dadosFrete->PrazoEntrega, 0);
                $retorno['VALOR'] = $this->formataValorEmReais((string) $dadosFrete->Valor);

            break;
            case 10:
                $retorno['ERRO'] = 0;
                $retorno['MSG'] = $dadosFrete->obsFim;
                $retorno['PRAZO'] = json_decode($dadosFrete->PrazoEntrega, 0);
                $retorno['VALOR'] = $this->formataValorEmReais((string) $dadosFrete->Valor);

            break;
            case 7:
                $retorno['ERRO'] = $dadosFrete->Erro;
                $retorno['MSG'] = 'Serviço temporariamente indisponível.';
                $retorno['PRAZO'] = 0;
                $retorno['VALOR'] = 0;

            default:
                $retorno['ERRO'] = $dadosFrete->Erro;
                $retorno['MSG'] = 'Não foi possível obter o valor do frete pelos Correios.';
                $retorno['PRAZO'] = 0;
                $retorno['VALOR'] = 0;
        }

        return $retorno;
    }

    private function trasnbordoDePeso($retornaUrl, $Peso, $Medida)
    {
        $consultas = array();

        $PESO_TEMP = $Peso;
        $Total = 0;
        do {
            if ($PESO_TEMP != $Peso) {
                $medida = 16;
            } else {
                $medida = $Medida;
            }

            $Pesio = intval($PESO_TEMP / 30);
            array_push($consultas, $this->obterValorDoFrete($retornaUrl, ($Pesio * 30), $medida, $medida, $medida)['VALOR']);
            $PESO_TEMP -= 30;
        } while ($PESO_TEMP > 0);

        return ($retornaUrl) ? $consultas : array_sum($consultas);
    }

    private function transbordoDePesoEMedida($retornaUrl, $Peso, $Medida)
    {
        $consultas = array();

        $Total = 0;

        $medida = $Medida;
        $peso = $Peso;

        do {

            $medida = ($medida > 66) ? $medida - 66 : $medida;
            $peso = ($peso > 30) ? $peso - 30 : $peso;
            $MED = ( ($medida - ($medida - 66)) > 16 ) ? $medida - ($medida - 66) : $medida;
            $PES = ($peso > 30) ? $peso - ($peso - 30) : $peso;
            array_push($consultas, $this->obterValorDoFrete($retornaUrl, $PES, $MED, $MED, $MED)['VALOR']);
        } while ($peso > 30 || $medida > 66);

        return ($retornaUrl) ? $consultas : array_sum($consultas);
    }
}
