<?php

namespace Application\Controller;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Testes extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelTestes', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    public function main()
    {
        $dados['page'] =  1;
        $dados['metodos'] =  get_class_methods($this);
        parent::prepararViewTeste("Home/pag_teste", $dados);
    }

    public function TesteListaImagensDiretorio($pasta = null, $subpasta = null)
    {
        $invisiveis = [
            '.DS_Store', '__construct', 'main', 'loadView', 'loadModel', 'error', 'validarRetornoServico',
            'carregarMenu', 'prepararView', 'prepararViewTeste', 'prepararBasicView', 'loadException',
            'validarCaptcha', 'consumir_cetrus', 'consulta_cetrus', 'valida_cep_correio', 'GetGoogleAccessToken',
            'GetGoogleUserProfileInfo', 'LoadAPISocial', 'sair', 'validar_cpf', 'validar_cnpj'
        ];
        $folders = [];
        $imagens = [];
        $pastas = [];

        if (!empty($pasta)) {
            array_push($pastas, $pasta);
        }

        if (!empty($subpasta)) {
            array_push($pastas, $subpasta);
        }

        if ($dir = opendir(FOTOPRODUTO_PATH . '/' . implode('/', $pastas))) {
            while ($folder = readdir($dir)) {
                if ($folder != '.' && $folder != '..' && $folder != basename(__FILE__)) {
                    if (is_dir(FOTOPRODUTO_PATH . '/' . $folder)) {
                        $folders[] = $folder;
                    } else {
                        $imagens[] = FOTOPRODUTO_URL . '/' . implode('/', $pastas) . '/' . $folder;
                    }
                }
            }
            closedir($dir);
        }

        natsort($folders);
        natsort($imagens);

        $dados['page'] =  2;
        $dados['pastas'] = $folders;
        $dados['imagens'] = $imagens;
        parent::prepararViewTeste("Home/pag_teste", $dados);
    }

    public function TesteUpdateProduto()
    {
        $this->model->TesteAjustarPathProdutos();
    }

    public function TesteCategoriasPath()
    {
        $lista = $this->model->TesteListaGaregorias();

        foreach ($lista as $row) {
            $string = strtolower(preg_replace('/\s*$/', '', $row['CAT_NOME']));
            $path = Common::removerEspacosPontos($string);
            $this->model->TesteUpdateCategoria($path, $row['CAT_ID']);
        }
    }

    public function TesteProdutosPath()
    {
        $lista = $this->model->TesteListaProdutosPathVazios();

        foreach ($lista as $row) {
            $string = strtolower(preg_replace('/\s*$/', '', $row['PRD_NOME']));
            $path = Common::removerEspacosPontos($string);
            $this->model->TesteUpdateProdutoPath($path, $row['PRD_ID']);
        }
    }

    public function TesteImagensExiste($opcao)
    {
        set_time_limit(7200);
        ignore_user_abort(true);

        $string = "";

        if ($opcao == 1) {
            $lista = $this->model->TesteListaProdutosSemPastaImagens();

            foreach ($lista as $row) {

                $imagem = $row['PRD_IMAGEM_PRINCIPAL'];

                $pasta = 'IMGS_PRD_ID_' . $row['PRD_ID'];

                //CRIA A IMAGEM E ATUALIZA A TABELA DE PRODUTOS
                if (!file_exists(FOTOPRODUTO_URL . '/' . $pasta . '/large/' . $imagem)) {
                    //mkdir(FOTOPRODUTO_URL . '/' . $pasta . '/large', 0777, true);
                    //$this->model->TesteUpdatePastaImagemProduto($pasta, $row['PRD_ID']);
                    //$string .= 'Pasta ' . $pasta . ' criada\n';

                    $path = FOTOPRODUTO_URL;

                    if ($dir = opendir($path)) {
                        while (false !== ($file = readdir($dir))) {
                            if (is_dir($path . "/" . $file)) {
                                if ($file != '.' && $file != '..') {
                                    diretorio($path . "/" . $file);
                                }
                            } else {
                                if ($file == $imagem) {
                                    if (rename($path . '/' . $file, FOTOPRODUTO_URL . '/' . $pasta . '/large/' . $file)) {
                                        $string .= 'Arquivo ' . $file . ' movido\n';
                                    }
                                }
                            }
                        }

                        closedir($dir);
                    }
                }
            }
        } else if ($opcao == 2) {

            $lista = $this->model->TesteListaProdutosComPastaImagens();
            $contn = 0;
            $conts = 0;
            foreach ($lista as $row) {

                $id = $row['PRD_ID'];
                $pasta = $row['PASTA_IMAGENS'];
                $imagem = $row['PRD_IMAGEM_PRINCIPAL'];

                //if (is_file(FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $imagem)) {
                $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $imagem);

                if (!$file_headers) {
                    //$string .= "imagem id:" . $id . ' ' . FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $imagem . " não existe\n";
                    $string .= self::TesteMoverImagem($pasta, $imagem);
                    $contn++;
                } else {
                    //$string .= "imagem id:" . $id . ' ' . FOTOPRODUTO_PATH . '/' . $pasta . '/large/' . $imagem . " existe\n";
                    $conts++;
                }
            }
        }
        echo '<pre>';
        print_r($string);
        echo '</pre>';

        echo '<pre>';
        print_r($conts . ' Existem\n' . $contn . ' Não existem');
        echo '</pre>';
    }

    public function TesteMoverImagem($pasta, $imagem)
    {
        $string = "";

        $pastasthumb = ['150x', '135x', '100x100', '100x', '80x80', '80x80-2', '80x', '75x75', '56x', '50x'];
        $pastaslarge = ['600x', '350x', '300x', '250x'];

        $subpastas = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'z', 'w', 'y'];

        /* foreach ($pastasthumb as $main) {
          $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem);

          if (!$file_headers) {
          foreach ($subpastas as $folder) {

          $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem);

          if (!$file_headers) {
          foreach ($subpastas as $subfolder) {
          $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem);

          if ($file_headers) {
          //if (rename(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/thumb/' . $file)) {
          $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem . ' movido\n';
          //}
          }
          }
          } else {
          //if (rename(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/thumb/' . $file)) {
          $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem . ' movido\n';
          //}
          }
          }
          } else {
          //if (rename(FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/thumb/' . $file)) {
          $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem . ' movido\n';
          //}
          }
          } */

        if (!file_exists(FOTOPRODUTO_URL . '/' . $pasta . '/large')) {
            mkdir(FOTOPRODUTO_URL . '/' . $pasta . '/large', 0777, true);
        }

        foreach ($pastaslarge as $main) {
            $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem);

            if ($file_headers) {
                if (copy(FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/large/' . $imagem)) {
                    $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $imagem . ' movido';
                }

                break;
            } else {

                foreach ($subpastas as $folder) {

                    $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem);

                    if ($file_headers) {
                        if (copy(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/large/' . $imagem)) {
                            $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $imagem . ' movido\n';
                        }

                        break;
                    } else {
                        foreach ($subpastas as $subfolder) {

                            $file_headers = file_exists(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem);

                            if ($file_headers) {

                                if (copy(FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem, FOTOPRODUTO_URL . '/' . $pasta . '/large/' . $imagem)) {
                                    $string .= 'Arquivo ' . FOTOPRODUTO_PATH . '/' . $main . '/' . $folder . '/' . $subfolder . '/' . $imagem . ' movido\n';
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        echo '<pre>';
        echo print_r($string);
        echo '</pre>';
    }

    public function TesteEnvioEmail(){

        $config = $this->modelsistema->ConfigEnvioEmail()[0];

        $email = 'guitarpe@homail.com';
        $dados = [
            'NOME_COMPLETO' => 'FLAVIO',
            'EMAIL' => 'guitarpe@gmail.com'
        ];

        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['NOME_CONTATO_DEFAULT'],
            'url' => $config['URL_SISTEMA'],
            'logotipo' => $config['LOGOTIPO']
        ];

        $ret = Common::dispararEmail($dados, $email, 'Santos Dumont - Email registrado para recebimento de newsletter', $info, 4);

        echo '<pre>';
        print_r($ret);
        echo '</pre>';
    }
}
