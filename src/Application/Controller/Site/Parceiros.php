<?php
namespace Application\Controller\Site;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Parceiros extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    public function Parceiro($id)
    {
        $dados['parceiro'] = $this->model->DadosParceiros($id)[0];

        parent::prepararBasicView("Site/modal_parceiro", $dados);
    }
}
