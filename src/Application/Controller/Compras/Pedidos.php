<?php

namespace Application\Controller\Compras;

require_once SYSTEM_PATH . 'vendor/santosdummontsite/vendor/autoload.php';

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Pedidos extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCompras', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');
    }

    public function main()
    {
        $dados['exibirlogos'] = true;
        parent::prepararView("Usuarios/pag_usuarios", $dados);
    }

    public function Devolucoes($id)
    {
        if (empty(Session::get("acesso"))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $dados['exibirlogos'] = false;
        $dados['listapedidos'] = $this->model->ListaPedidosDevolvidos($token);
        if (count($dados['listapedidos']) > 0) {
            $dados['totaischeckout'] = $this->modelsistema->TotalPedido($token, $dados['listapedidos'][0]['TOKEN']);
        } else {
            $dados['totaischeckout'] = null;
        }
        parent::prepararView("Carrinho/pag_devolucoes", $dados);
    }

    private function retornaCredenciaisPagSeguro($config)
    {
        $email = Common::encrypt_decrypt('decrypt', $config['PAGSEG_EMAIL']);

        if ($config['PAGSEG_TIPO_ENVIO'] == 'sandbox') {
            $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_SAND_TOKEN']);
        } else {
            $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_PRD_TOKEN']);
        }

        $configPagSeguro = new \PagSeguro\Configuration\Configure();
        $configPagSeguro->setEnvironment($config['PAGSEG_TIPO_ENVIO']);
        $configPagSeguro::setAccountCredentials($email, $token);

        return $configPagSeguro::getAccountCredentials();
    }

    public function Transito($id)
    {
        if (empty(Session::get("acesso"))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $dados['exibirlogos'] = false;
        $dados['listapedidos'] = $this->model->ListaPedidosEmTransito($token);
        if (count($dados['listapedidos']) > 0) {
            $dados['totaischeckout'] = $this->modelsistema->TotalPedido($token, $dados['listapedidos'][0]['TOKEN']);
        } else {
            $dados['totaischeckout'] = null;
        }
        parent::prepararView("Carrinho/pag_transito", $dados);
    }

    public function RetornoPagamento($codigo)
    {
        $decode = explode('-', Common::encrypt_decrypt('decrypt', $codigo));
        $token = $decode[0];
        $pedido = $decode[1];

        if (!empty($token)) {
            Session::delete("logado");
            Session::set('social_tipo', 0);
            Session::set("logado", TRUE);
            Session::set("acesso", $token);
            Session::set('selecionado', 1);

            Common::redir('Pedidos/ResumoPedido/' . $pedido);
        }
    }

    public function ResumoPedido($tokenped)
    {
        $token = Session::get('acesso');

        self::SituacaoPedido($tokenped);

        $dados['inforesumopedido'] = $this->model->InfoResumoPedido($tokenped)[0];

        if (intval($dados['inforesumopedido']['EMAIL_ENVIADO']) == 0) {
            $clientes = $this->modelsistema->UsuarioPorToken($token);

            $config = $this->modelsistema->ConfigEnvioEmail()[0];

            $info = [
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA']
            ];

            $html = '<html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <title>Santos Dumont - Pedido Gerado com Sucesso</title>
                        </head>
                        <body>
                            <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                    <img src="' . URL_IMG_MAIL . '/' . $config['LOGOTIPO'] . '">
                                </div>
                                <div class="container" style="padding:20px">
                                    <table>
                                        <tr>
                                            <td>
                                                <h2><b></b></h2>
                                                <br/>Parabéns, o pedido ' . $tokenped . ' já foi gerado!
                                                <br/>
                                                <br/>
                                                <h3><strong>Detalhes do Pagamento</strong></h3>';

            switch ($dados['inforesumopedido']['PAY_METHOD_TYPE_STR']) {
                case 'CREDIT_CARD':

                    $html .= '<div class="col-sm-12">
                                            <h5><strong>Cartão de Crédito</strong></h5>
                                            <div class="col-sm-6">
                                                <h6>Bandeira</h6>
                                                <div>
                                                    <img src="' . $dados['inforesumopedido']['BANDEIRA'] . '"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <h6>Data da Transação</h6>
                                                <div>' . gmdate("j/m/Y H:i:s", strtotime($dados['inforesumopedido']['DATE'])) . '</div>
                                            </div>
                                            <div class="col-sm-12">
                                                <h6>Status</h6>
                                                <div><strong>' . $dados['inforesumopedido']['MENSAGEM'] . '</strong></div>
                                                <div><strong>' . $dados['inforesumopedido']['DESCRICAO'] . '</strong></div>
                                            </div>
                                        </div>';

                    break;
                case 'BOLETO':

                    $html .= '<div class="col-sm-12">
                                        <h5><strong>Boleto Bancário</strong></h5>
                                        <div class="col-sm-6">
                                            <h6>Link para Inpressão</h6>
                                            <div>
                                                <a href="' . $dados['inforesumopedido']['PAYMENT_LINK'] . '" target="_blank"><strong>Clique Aqui</strong></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h6>Data da Transação</h6>
                                            <div>' . gmdate("j/m/Y H:i:s", strtotime($dados['inforesumopedido']['DATE'])) . '</div>
                                        </div>
                                        <div class="col-sm-12">
                                            <h6>Status</h6>
                                                <div><strong>' . $dados['inforesumopedido']['MENSAGEM'] . '</strong></div>
                                                <div><strong>' . $dados['inforesumopedido']['DESCRICAO'] . '</strong></div>
                                        </div>
                                    </div>';

                    break;
                case 'BALANCE':
                    break;
                case 'ONLINE_DEBIT':

                    $html .= '<div class="col-sm-12">
                                    <h5><strong>Débito On-line</strong></h5>
                                        <div class="col-sm-6">
                                            <h6>Banco</h6>
                                            <div>
                                                <strong>Itaú</strong>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h6>Data da Transação</h6>
                                            <div>' . gmdate("j/m/Y H:i:s", strtotime($dados['inforesumopedido']['DATE'])) . '</div>
                                        </div>
                                        <div class="col-sm-12">
                                            <h6>Status</h6>
                                                <div><strong>' . $dados['inforesumopedido']['MENSAGEM'] . '</strong></div>
                                                <div><strong>' . $dados['inforesumopedido']['DESCRICAO'] . '</strong></div>
                                        </div>
                                    </div>';
                    break;
                case 'DEPOSIT':
                    break;
            }


            $html .= 'Fique atento, você receberá um e-mail informando o andamento do seu pedido.
                                                <br/>
                                                <br/>
                                                <br/>
                                                <br/>
                                                Muito obrigado por comprar conosco!
                                                <br/>
                                                <br/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

            Common::dispararEmailPersonalizado('Santos Dumont - Pedido Gerado com Sucesso!', $html, $clientes['dados']['EMAIL'], $info);

            $uppedido = [
                'I_TIPO' => 1,
                'I_TOKEN' => $token,
                'I_TOKEN_PED' => $tokenped,
                'I_EMAIL_ENVIADO' => 1
            ];

            $this->modelsistema->UpdatePedido($uppedido);
        }

        $this->modelsistema->LimparCarrinhoFinalPedido($token);

        $dados['exibirlogos'] = false;
        $dados['tokenpedido'] = $tokenped;
        $dados['listacheckout'] = $this->model->ListaItensPedido($token, $tokenped);
        $dados['totaischeckout'] = $this->modelsistema->TotalPedido($token, $tokenped);

        parent::prepararView("Carrinho/pag_detalhe_ped_final", $dados);
    }

    public function SituacaoPedido($tokenped)
    {
        $config = $this->modelsistema->Configuracoes()[0];
        $pedido = $this->model->InfoResumoPedido($tokenped)[0];

        try {
            \PagSeguro\Library::initialize();

            $credenciais = $this->retornaCredenciaisPagSeguro($config);

            $response = \PagSeguro\Services\Transactions\Search\Code::search(
                $credenciais,
                $pedido['CODE']
            );

            $dados['I_TOKEN'] = $tokenped;
            $dados['I_DATE'] = $response->getDate();
            $dados['I_CODE'] = $response->getCode();
            $dados['I_STATUS'] = $response->getStatus();

            $this->model->AtualizarPedidoSiteAPI($dados);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function Historico($id)
    {
        if (empty(Session::get("acesso"))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $dados['exibirlogos'] = false;
        $dados['listapedidos'] = $this->model->ListaPedidosRealizados($token);
        if (count($dados['listapedidos']) > 0) {
            $dados['totaischeckout'] = $this->modelsistema->TotalPedido($token, $dados['listapedidos'][0]['TOKEN']);
        } else {
            $dados['totaischeckout'] = null;
        }
        parent::prepararView("Carrinho/pag_historico_pedidos", $dados);
    }
}
