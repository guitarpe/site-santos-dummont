<?php
namespace Application\Controller\Compras;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session,
    santosdummontsite\PHPMailer;

class Favoritos extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');
    }

    public function main()
    {
        $dados['exibirlogos'] = true;
        $dados['listafavoritos'] = $this->model->ListaProdutosFavoritos($id);
        parent::prepararView("Carrinho/pag_favoritos", $dados);
    }
}
