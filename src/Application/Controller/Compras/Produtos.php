<?php

namespace Application\Controller\Compras;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Produtos extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCompras', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');
    }

    public function main()
    {
        $dados['exibirlogos'] = true;
        parent::prepararView("Carrinho/pag_favoritos", $dados);
    }

    public function Item($uri)
    {
        if (empty(Session::get('acesso'))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $prod = $this->model->ProdutoUri($uri)[0];

        $dados['produto'] = $this->model->DadosProdutoSite($prod['PRD_ID'], null, $token)[0];


        //echo '<pre>';
        //print_r($dados['produto']['OUTRAS_IMGS']);
        //echo '</pre>';
        //exit();

        //$dados['produtospopulares'] = $this->model->ListaProdutosPopulares();
        $dados['produtossimilares'] = $this->model->ListaProdutosSimilares($dados['produto']['CAT_ID']);
        $dados['exibirlogos'] = false;
        $dados['actioncoment'] = SITE_URL . '/Produtos/Comentar/' . $prod['PRD_ID'];
        $dados['listacomentarios'] = $this->model->ListaComentarios($prod['PRD_ID']);
        $dados['listameiosentrega'] = self::ListaMeiosEntregaAbrangencia($token);
        $dados['calcularfrete'] = SITE_URL . "/Usuarios/CalcularFrete/" . $prod['PRD_ID'];

        parent::prepararView("Carrinho/pag_item", $dados);
    }

    public function Detalhe($id){
        if (empty(Session::get('acesso'))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $dados['produto'] = $this->model->DadosProdutoSite($id, null, $token)[0];


        $dados['actioncoment'] = SITE_URL . '/Produtos/Comentar/' . $id;
        $dados['listacomentarios'] = $this->model->ListaComentarios($id);
        parent::prepararBasicView("Home/modal_detalhe_produto", $dados);
    }

    public function Comentar($id)
    {
        if (empty(Session::get('acesso'))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        $produto = $this->model->DadosProdutoSite($id, null, $token)[0];

        $this->validarCamposObrigatorio($produto['PRD_PATH']);

        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $comentario = filter_input(INPUT_POST, "comentario", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $dados = [
            'NOME' => $nome,
            'EMAIL' => $email,
            'COMENTARIO' => $comentario,
            'PRD_ID' => $id
        ];


        $captcha = filter_input(INPUT_POST, "g-recaptcha-response");
        $ip = $_SERVER['REMOTE_ADDR'];
        $validarcaptcha = parent::validarCaptcha($captcha, $ip);

        if ($validarcaptcha['erro']) {
            $msg = 'Atenção! reCaptcha não validado';
            $situacao = 'danger';
        }else{

            $retorno = $this->model->GravarComentario($dados);

            if (intval($retorno['O_COD_RETORNO']) != 0) {
                $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {
                $msg = 'Realizado com sucesso!';
                $situacao = 'success';
            }
        }
        Session::set('tab', 4);
        Common::alert($msg, $situacao, 'acao');
        Common::redir('Produtos/Item/' . $produto['PRD_PATH']);
    }

    public function Categoria($parametros)
    {
        $itenstermos = [];

        $busca['TODOS'] = 0;
        $busca['CAT'] = 0;
        $busca['TERMO'] = '';
        $busca['DISP'] = 0;
        $busca['INDISP'] = 0;
        $busca['ITENS'] = 12;
        $busca['ORDENACAO'] = 1;
        $busca['GRUPO'] = null;

        $dados['filtro'] = 0;

        $id = 0;

        $pagina = 0;
        $dispckd = 0;
        $indispckd = 0;

        if (count($_POST)) {
            $termo = !empty(filter_input(INPUT_POST, "cat")) ? 'cat=' . filter_input(INPUT_POST, "cat") : '';
            $disp = !empty(filter_input(INPUT_POST, "dispo")) ? ',dispo=' . filter_input(INPUT_POST, "dispo") : '';
            $indisp = !empty(filter_input(INPUT_POST, "indisp")) ? ',indisp=' . filter_input(INPUT_POST, "indisp") : '';
            $precoi = !empty(Common::returnValor(filter_input(INPUT_POST, "precoi"))) ? ',preco_ini=' . Common::returnValor(filter_input(INPUT_POST, "precoi")) : '';
            $precof = !empty(Common::returnValor(filter_input(INPUT_POST, "precof"))) ? ',preco_fin=' . Common::returnValor(filter_input(INPUT_POST, "precoi")) : '';
            $itens = !empty(filter_input(INPUT_POST, "itens")) ? ',itens=' . filter_input(INPUT_POST, "itens") : '';
            $ordenacao = !empty(filter_input(INPUT_POST, "ordenacao")) ? ',order=' . filter_input(INPUT_POST, "ordenacao") : '';

            $parametros = $termo . $disp . $indisp . $precoi . $precof . $itens . $ordenacao;
            Common::redir('Produtos/Categoria/' . $parametros);
        } else {

            $itenstermosurl = explode(',', $parametros);

            foreach ($itenstermosurl as $item) {
                if (strpos($item, 'cat=') !== false) {
                    $busca['TERMO'] = str_replace('cat=', '', $item);
                    $itenstermos[] = $item;

                    $id = $this->model->CategoriaUri($busca['TERMO'])[0]['CAT_ID'];
                    $busca['CAT'] = $id;
                }

                if (strpos($item, 'dispo=') !== false) {
                    $busca['DISP'] = str_replace('dispo=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                    $dispckd = 1;
                }

                if (strpos($item, 'indisp=') !== false) {
                    $busca['INDISP'] = str_replace('indisp=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                    $indispckd = 1;
                }

                if (strpos($item, 'preco_ini=') !== false) {
                    $busca['PRECOI'] = str_replace('preco_ini=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                }

                if (strpos($item, 'preco_fin=') !== false) {
                    $busca['PRECOF'] = str_replace('preco_fin=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                }

                if (strpos($item, 'itens=') !== false) {
                    $busca['ITENS'] = str_replace('itens=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                }

                if (strpos($item, 'order=') !== false) {
                    $busca['ORDENACAO'] = str_replace('order=', '', $item);
                    $itenstermos[] = $item;
                    $dados['filtro'] = 1;
                }

                if (strpos($item, 'pag=') !== false) {
                    $pagina = str_replace('pag=', '', $item);

                    if ($pagina == "all") {
                        $busca['TODOS'] = 1;
                    } else {
                        $busca['TODOS'] = 0;
                    }
                }
            }
        }

        if (!isset($busca['ITENS'])) {
            $busca['ITENS'] = 12;
        }

        if (!isset($busca['ORDENACAO'])) {
            $busca['ORDENACAO'] = 0;
        }

        $total = $this->model->TotalCategoriaProdutos($busca)[0];

        if (intval($busca['TODOS']) == 1) {
            $pagina = 1;
            $busca['ITENS'] = $total['TOTAL'];
        }

        $totalpaginas = ceil($total['TOTAL'] / $busca['ITENS']);

        if ($pagina > $totalpaginas) {
            $pagina = $totalpaginas;
        }

        if ($pagina < 1) {
            $pagina = 1;
        }

        $terms = implode(",", $itenstermos);
        $busca['OFFSET'] = ($pagina - 1) * $busca['ITENS'];

        $dados['listageral'] = $this->model->ListaCategoriaProdutos($busca);
        $dados['categoriasirmas'] = $this->model->ListaCategoriasIrmas($id);
        $dados['categoria'] = $this->model->InfoCategoria($id)[0];
        $dados['totalpaginas'] = $totalpaginas;
        $dados['paginaatual'] = $pagina;
        $dados['dispckd'] = $dispckd;
        $dados['indispckd'] = $indispckd;
        $dados['disponiveis'] = $dados['categoria']['DISPONIVEIS'];
        $dados['ndisponiveis'] = $dados['categoria']['INDISPONIVEIS'];
        $dados['resultados'] = ($dados['categoria']['DISPONIVEIS'] + $dados['categoria']['INDISPONIVEIS']);
        $dados['cat'] = $busca['TERMO'];
        $dados['dispo'] = $busca['DISP'];
        $dados['indisp'] = $busca['INDISP'];
        $dados['precoi'] = empty($busca['PRECOI']) ? '' : number_format($busca['PRECOI'], 2, ',', '.');
        $dados['precof'] = empty($busca['PRECOF']) ? '' : number_format($busca['PRECOF'], 2, ',', '.');
        $dados['itens'] = intval($busca['ITENS']);
        $dados['ordenacao'] = intval($busca['ORDENACAO']);
        $dados['exibirlogos'] = true;
        $dados['urlactionfiltro'] = SITE_URL . '/Produtos/Categoria/' . $terms;
        $dados['urlaction'] = SITE_URL . '/Produtos/Categoria/' . $terms;
        $dados['todos'] = $busca['TODOS'];

        parent::prepararView("Carrinho/pag_lista", $dados);
    }

    public function Fabricante($uri, $pagina = null)
    {
        $id = $this->model->ParceirosUri($uri)[0]['PAR_ID'];

        $itensporpagina = 12;

        $busca['PAR'] = $id;
        $busca['ITENS'] = 12;

        $total = $this->model->TotalFabricanteProdutos($busca)[0];

        $totalpaginas = ceil($total['TOTAL'] / $itensporpagina);

        if ($pagina > $totalpaginas) {
            $$pagina = $totalpaginas;
        }
        if ($pagina < 1) {
            $pagina = 1;
        }

        $busca['OFFSET'] = ($pagina - 1) * $itensporpagina;

        $dados['exibirlogos'] = true;
        $dados['listageral'] = $this->model->ListaFabricateProdutos($busca);
        $dados['outrosparceiros'] = $this->model->ListaOutrosFabricantes($id);
        $dados['parceiro'] = $this->model->InfoFabricante($id)[0];

        $dados['totalpaginas'] = $totalpaginas;
        $dados['paginaatual'] = $pagina;

        $resultados = 0;
        foreach ($dados['listageral'] as $row) {
            $resultados++;
        }

        $dados['resultados'] = $resultados;
        $dados['urlaction'] = SITE_URL . '/Produtos/Fabricante/' . $uri;
        parent::prepararView("Carrinho/pag_comparar", $dados);
    }

    public function ListaMeiosEntregaAbrangencia($token)
    {

        $lista = [];
        $listameios = $this->model->ListaMeiosEntregaCepToken($token);

        foreach ($listameios as $row) {
            if (intval($row['CALC_FRETE']) == 0) {
                if (!empty($row['CEP_USER']) && intval($row['CODIGO_MEIO'] != 719680)) {

                    $ret = self::InfoAbrangenciaComCep($row['MEIO_ID'], $row['CEP_USER']);

                    if (intval($ret['ERRO']) == 0 && intval($ret['EXISTE']) > 0) {
                        $lista[] = $row;
                    }
                } else {
                    $lista[] = $row;
                }
            } else {
                $lista[] = $row;
            }
        }

        return $lista;
    }

    public function InfoAbrangenciaComCep($id, $str)
    {
        $cep = preg_replace('/[^0-9]/', '', (string) $str);
        $url = "http://viacep.com.br/ws/" . $cep . "/json/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);

        if (!isset($json->erro)) {
            $params = [
                'MEIO_ID' => $id,
                'UF' => $json->uf,
                'CIDADE' => $json->localidade
            ];

            $res = $this->model->AreaAbrangenciaMeio($params);

            $dados = [
                'ERRO' => 0,
                'EXISTE' => $res[0]['EXISTE']
            ];
        } else {
            $dados = [
                'ERRO' => 1,
                'EXISTE' => 0
            ];
        }
        return $dados;
    }

    private function validarCamposObrigatorio($uri)
    {
        $dados['Nome'] = filter_input(INPUT_POST, "nome");
        $dados['E-Mail'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $dados['Comentário'] = filter_input(INPUT_POST, "comentario", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        Common::validarInputsObrigatorio($dados, 'Produtos/Item/' . $uri);
    }
}
