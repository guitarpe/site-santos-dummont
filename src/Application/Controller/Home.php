<?php

namespace Application\Controller;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }
    }

    public function main()
    {
        if (empty(Session::get("acesso"))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }

            Session::set('carrinhovazio', 1);

            //tratamento para gravar o tempo de sessão
            $dadossessao = [
                'token' => $token,
                'registered' => time()
            ];

            Session::set('dadossessao', $dadossessao);

            $dados['logado'] = false;
        }

        $dados['exibirlogos'] = true;
        $dados['listabanners'] = $this->model->ListaBannersCadastrados();

        $produtosdes = $this->model->ListaProdutosDestaques();

        $destaques = [];
        $novidades = [];
        $promocoes = [];
        $especiais = [];

        foreach ($produtosdes as $row) {
            if ($row['TIPO'] == 'DES') {
                array_push($destaques, $row);
            }
        }

        foreach ($produtosdes as $row) {
            if ($row['TIPO'] == 'NOV') {
                array_push($novidades, $row);
            }
        }

        foreach ($produtosdes as $row) {
            if ($row['TIPO'] == 'PRO') {
                array_push($promocoes, $row);
            }
        }

        foreach ($produtosdes as $row) {
            if ($row['TIPO'] == 'ESP') {
                array_push($especiais, $row);
            }
        }

        $dados['listaproddestaques'] = array_chunk($destaques, 4);
        $dados['listaprodnovidades'] = array_chunk($novidades, 4);
        $dados['listaprodpromocoes'] = array_chunk($promocoes, 4);
        $dados['listaprodespeciais'] = array_chunk($especiais, 3);

        parent::prepararView("Home/pag_home", $dados);
    }

    public function CarregarModais()
    {
        $dados['modal'] = $this->model->ListaModaisCadastrados();

        if (!empty($dados['modal']['MOD_ID'])) {
            echo parent::prepararBasicView("Home/modal_promo", $dados);
        } else {
            return null;
        }
    }

    public function ModalContatoSessao()
    {
        $dados['urlaction'] = SITE_URL . "/Home/GravarContato";
        echo parent::prepararBasicView("Home/modal_contato", $dados);
    }

    public function VerificaTempoSessao(){

        $return = 0;

        if (empty(Session::get('acesso'))) {
            $token = Session::get('carrinho');
        } else {
            $token = Session::get('acesso');
        }

        if(intval(Session::get('carrinhovazio')) == 0){
            if ((time() - Session::get("dadossessao")['registered']) > (60 * 15)) {
                Session::delete("dadossessao");
                $dadossessao = [
                    'token' => $token,
                    'registered' => time()
                ];

                Session::set('dadossessao', $dadossessao);

                Session::delete('carrinhovazio');
                Session::set('carrinhovazio', 1);

                $return = 1;
            }else{
                $return = 0;
            }
        }else{
            $return = 1;
        }

        echo $return;
    }

    public function ListaCompras()
    {
        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
        } else {
            $token = Session::get('acesso');
        }

        $dados["token"] = $token;
        $dados["listacarrinho"] = $this->modelsistema->ListaCarrinhoCompra($token);
        parent::prepararBasicView("Site/modal_carrinho", $dados);
    }

    public function Adicionar()
    {
        $id = filter_input(INPUT_POST, "id");
        $qtde = filter_input(INPUT_POST, "qtde");

        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
        } else {
            $token = Session::get('acesso');
        }

        Session::delete('carrinhovazio');
        Session::set('carrinhovazio', 0);

        $adicionar = $this->modelsistema->AdicionarCarrinho($id, $token, $qtde);
        echo json_encode($adicionar);
    }

    public function AddFavorito()
    {
        $id = filter_input(INPUT_POST, "id");

        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
        } else {
            $token = Session::get('acesso');
        }

        $adicionar = $this->modelsistema->AdicionarFavoritos($id, $token);
        echo json_encode($adicionar);
    }

    public function Classificar($nota)
    {
        $id = filter_input(INPUT_POST, "id");

        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
        } else {
            $token = Session::get('acesso');
        }

        $adicionar = $this->modelsistema->ClassificarProduto($id, $token, $nota);
        echo json_encode($adicionar);
    }

    public function Lembrar()
    {
        $id = filter_input(INPUT_POST, "id");
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);

        if (empty(Session::get('acesso'))) {
            if (!isset($email)) {

                $retorno[0] = [
                    'O_COD_RETORNO' => 1,
                    'O_DESC_CURTO' => '',
                    'O_TOKEN_INVALIDO' => 'N'
                ];

                echo json_encode($retorno);
                exit();
            } else {
                $token = Session::get('carrinho');
                $adicionar = $this->modelsistema->LembrarEstoqueProduto($id, $token, $email);
                echo json_encode($adicionar);
                exit();
            }
        } else {
            $token = Session::get('acesso');
            $email = null;

            $adicionar = $this->modelsistema->LembrarEstoqueProduto($id, $token, $email);
            echo json_encode($adicionar);
            exit();
        }
    }

    public function CadastrarContatoWhats()
    {
        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $telefone = filter_input(INPUT_POST, "telefone");

        $dados = [
            'I_NOME' => $nome,
            'I_EMAIL' => $email,
            'I_TELEFONE' => $telefone
        ];

        $adicionar = $this->modelsistema->ResgistroContatoWhatts($dados);

        echo json_encode($adicionar);
    }

    public function InformarEmail($id)
    {
        $dados["urlaction"] = SITE_URL . '/Home/Lembrar';
        $dados["id"] = $id;
        parent::prepararBasicView("Home/pag_info_email", $dados);
    }

    public function ContatoWhats()
    {
        $dados["urlaction"] = SITE_URL . '/Home/CadastrarContatoWhats';
        parent::prepararBasicView("Home/pag_contato_whats", $dados);
    }

    public function Sobre()
    {
        $dados['exibirlogos'] = true;
        $dados['informacoessite'] = $this->modelsistema->InformacoesSite()[0];
        parent::prepararView("Home/pag_sobre", $dados);
    }

    public function FAQ()
    {
        $dados['exibirlogos'] = true;
        parent::prepararView("Home/pag_faq", $dados);
    }

    public function Termos()
    {
        $dados['exibirlogos'] = true;
        $dados['informacoessite'] = $this->modelsistema->InformacoesSite()[0];
        parent::prepararView("Home/pag_termos", $dados);
    }

    public function Contato()
    {
        $dados['exibirlogos'] = true;
        parent::prepararView("Home/pag_contato", $dados);
    }

    public function Politica()
    {
        $dados['exibirlogos'] = true;
        $dados['informacoessite'] = $this->modelsistema->InformacoesSite()[0];
        parent::prepararView("Home/pag_politica", $dados);
    }

    public function GetEstoque($id)
    {
        $retorno = $this->model->EstoqueProduto($id)[0];
        echo json_encode($retorno);
    }

    public function Buscar()
    {
        $termo = !empty(filter_input(INPUT_POST, "termo")) ? 'termo=' . filter_input(INPUT_POST, "termo") : '';
        $disp = !empty(filter_input(INPUT_POST, "dispo")) ? ',dispo=' . filter_input(INPUT_POST, "dispo") : '';
        $indisp = !empty(filter_input(INPUT_POST, "indisp")) ? ',indisp=' . filter_input(INPUT_POST, "indisp") : '';
        $precoi = !empty(Common::returnValor(filter_input(INPUT_POST, "precoi"))) ? ',preco_ini=' . Common::returnValor(filter_input(INPUT_POST, "precoi")) : '';
        $precof = !empty(Common::returnValor(filter_input(INPUT_POST, "precof"))) ? ',preco_fin=' . Common::returnValor(filter_input(INPUT_POST, "precoi")) : '';
        $itens = !empty(filter_input(INPUT_POST, "itens")) ? ',itens=' . filter_input(INPUT_POST, "itens") : '';
        $ordenacao = !empty(filter_input(INPUT_POST, "ordenacao")) ? ',order=' . filter_input(INPUT_POST, "ordenacao") : '';
        $grup = implode(',', filter_input(INPUT_POST, "grupo", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
        $grupo = !empty($grup) ? ',gru=' . $grup : '';

        if (count($_POST)) {

            $parametros = $termo . $disp . $indisp . $precoi . $precof . $itens . $ordenacao . $grupo;
            Common::redir('Home/Busca/' . $parametros);
        } else {
            $msg = "Informe alguma informação de pesquisa";
            $situacao = 'warning';

            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    public function Busca($parametros)
    {
        $itenstermos = [];

        $busca['TODOS'] = 0;
        $busca['TERMO'] = '';
        $busca['DISP'] = 0;
        $busca['INDISP'] = 0;
        $busca['ITENS'] = 12;
        $busca['ORDENACAO'] = 1;
        $busca['GRUPO'] = null;

        $dados['filtro'] = 0;

        $pagina = 0;
        $dispckd = 0;
        $indispckd = 0;

        $itenstermosurl = explode(',', $parametros);

        foreach ($itenstermosurl as $item) {
            if (strpos($item, 'termo=') !== false) {
                $busca['TERMO'] = str_replace('termo=', '', $item);
                $itenstermos[] = $item;
            }

            if (strpos($item, 'dispo=') !== false) {
                $busca['DISP'] = str_replace('dispo=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
                $dispckd = 1;
            }

            if (strpos($item, 'indisp=') !== false) {
                $busca['INDISP'] = str_replace('indisp=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
                $indispckd = 1;
            }

            if (strpos($item, 'preco_ini=') !== false) {
                $busca['PRECOI'] = str_replace('preco_ini=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
            }

            if (strpos($item, 'preco_fin=') !== false) {
                $busca['PRECOF'] = str_replace('preco_fin=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
            }

            if (strpos($item, 'itens=') !== false) {
                $busca['ITENS'] = str_replace('itens=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
            }

            if (strpos($item, 'order=') !== false) {
                $busca['ORDENACAO'] = str_replace('order=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
            }

            if (strpos($item, 'gru=') !== false) {
                $busca['GRUPO'] = str_replace('gru=', '', $item);
                $itenstermos[] = $item;
                $dados['filtro'] = 1;
            }

            if (strpos($item, 'pag=') !== false) {
                $pagina = str_replace('pag=', '', $item);

                if ($pagina == "all") {
                    $busca['TODOS'] = 1;
                } else {
                    $busca['TODOS'] = 0;
                }
            }
        }

        if (!isset($busca['ITENS'])) {
            $busca['ITENS'] = 12;
        }

        if (!isset($busca['ORDENACAO'])) {
            $busca['ORDENACAO'] = 0;
        }

        $total = $this->model->TotalBusca($busca)[0];

        if (intval($busca['TODOS']) == 1) {
            $pagina = 1;
            $busca['ITENS'] = $total['TOTAL'];
        }

        $totalpaginas = ceil($total['TOTAL'] / $busca['ITENS']);

        if ($pagina > $totalpaginas) {
            $pagina = $totalpaginas;
        }

        if ($pagina < 1) {
            $pagina = 1;
        }

        $busca['OFFSET'] = ($pagina - 1) * $busca['ITENS'];

        $dados['listageral'] = $this->model->ResultadosBusca($busca);

        $dados['listagrupos'] = $this->model->BuscaGrupos($busca);
        $dispindisp = $this->model->TotaisBusca($busca)[0];

        if ($busca['TERMO'] != '') {
            $this->model->GravaBusca($busca['TERMO']);
        }

        $dados['totalpaginas'] = $totalpaginas;
        $dados['paginaatual'] = $pagina;
        $dados['dispckd'] = $dispckd;
        $dados['indispckd'] = $indispckd;
        $dados['disponiveis'] = $dispindisp['DISPONIVEIS'];
        $dados['ndisponiveis'] = $dispindisp['INDISPONIVEIS'];
        $dados['resultados'] = ($dispindisp['DISPONIVEIS'] + $dispindisp['INDISPONIVEIS']);
        //parametros de busca
        $dados['termo'] = $busca['TERMO'];
        $dados['dispo'] = $busca['DISP'];
        $dados['indisp'] = $busca['INDISP'];
        $dados['precoi'] = empty($busca['PRECOI']) ? '' : number_format($busca['PRECOI'], 2, ',', '.');
        $dados['precof'] = empty($busca['PRECOF']) ? '' : number_format($busca['PRECOF'], 2, ',', '.');
        $dados['itens'] = intval($busca['ITENS']);
        $dados['ordenacao'] = intval($busca['ORDENACAO']);
        $dados['exibirlogos'] = true;
        $dados['urlactionfiltro'] = SITE_URL . '/Home/Buscar';
        $dados['todos'] = $busca['TODOS'];

        $terms = implode(",", $itenstermos);

        $dados['urlaction'] = SITE_URL . '/Home/Busca/' . $terms;

        parent::prepararView("Home/pag_busca", $dados);
    }

    public function FormPadrao()
    {
        $dados['exibirlogos'] = true;
        parent::prepararView("Home/pag_busca", $dados);
    }

    public function Sair()
    {
        $token = Session::get('acesso');

        $retorno = $this->modelsistema->InativarToken($token);

        if ($retorno['O_COD_RETORNO'] != 0) {
            Session::set("log-invalido", TRUE);
            Session::set("erro-login", $retorno['O_DESC_CURTO']);
            Session::set("erro-class", 'alert-danger pt-2 pb-2');

            $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Session::destroy();
            Common::redir('Home');
        } else {
            Session::destroy();
            Common::redir('Home');
        }
    }

    public function GravarNewsletter()
    {
        $email = filter_input(INPUT_POST, "email_newsletter", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);

        $dados = [
            'EMAIL' => $email,
            'NOME_COMPLETO' => ''
        ];

        $cadastrar = $this->modelsistema->GravaNewsletter($dados)[0];

        if ($cadastrar['O_COD_RETORNO'] == 0) {

            $config = $this->modelsistema->ConfigEnvioEmail()[0];

            $info = [
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            ];

            Common::dispararEmail($dados, $email, 'Santos Dumont - Email registrado para recebimento de newsletter', $info, 4);
        }

        $msg = '<center>Muito obrigado por se registrar!<br>A partir de agora você irá receber notícias, novidades e promoções <br>sobre nossos produtos e serviços diretamente em seu e-mail!</center>';
        $situacao = 'success';

        Common::alert($msg, $situacao, 'acao');
        Common::voltar();
    }

    public function GravarContato()
    {
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $dados = [
            'EMAIL' => $email,
            'NOME_COMPLETO' => $nome
        ];

        $config = $this->modelsistema->ConfigEnvioEmail()[0];

        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
            'fromname' => 'Equipe de Venda SantosDumont',//$config['NOME_CONTATO_DEFAULT'],
            'url' => $config['URL_SISTEMA'],
            'logotipo' => $config['LOGOTIPO']
        ];

        Common::dispararEmail($dados, 'vendas@sd68.com.br', 'Santos Dumont - Email registrado para contato', $info, 5);

        $msg = '<center>Muito obrigado por se registrar!<br>A partir de agora você irá receber um contato da nossa equipe de vendas!</center>';
        $situacao = 'success';

        Common::alert($msg, $situacao, 'acao');
        Common::voltar();
    }

    public function Reset($code)
    {
        $id = Common::encrypt_decrypt('decrypt', $code);

        $usuario = $this->modelsistema->UsuarioPorID($id)[0];

        $dados['urlaction'] = SITE_URL . '/Login/TrocarSenha';
        $dados['exibirlogos'] = true;
        $dados['email'] = $usuario["EMAIL"];
        $dados['iduser'] = $id;
        $dados['code'] = $code;
        parent::prepararView("Home/pag_reset_senha", $dados);
    }

    public function ValidaDoc()
    {
        $tipo = filter_input(INPUT_POST, "tipo");
        $numdoc = filter_input(INPUT_POST, "numdoc");

        if (intval($tipo) == 1) {
            echo parent::validar_cpf($numdoc);
        } else if (intval($tipo) == 2) {
            echo parent::validar_cnpj($numdoc);
        }
    }

    public function VerificaEmailCliente()
    {

        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $usuario = $this->modelsistema->VerificaEmailClienteExiste($email)[0];

        echo intval($usuario['TOTAL']);
    }

    public function VerificaCPFCliente()
    {
        $token = Session::get('acesso');
        $cpf = preg_replace('/[^0-9]/', '', (string) filter_input(INPUT_POST, "cpf"));
        $usuario = $this->modelsistema->VerificaCPFClienteExiste($cpf, $token)[0];

        echo intval($usuario['TOTAL']);
    }
}
