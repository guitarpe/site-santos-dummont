<?php

namespace Application\Controller;

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

require_once SYSTEM_PATH . 'vendor/santosdummontsite/vendor/autoload.php';

class Login extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');
    }

    public function main()
    {
    }

    public function LogarCliente()
    {
        $this->validarCamposObrigatorio(1);

        $login = filter_input(INPUT_POST, "email_login", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $pass = filter_input(INPUT_POST, "password_login");
        $ctrl = filter_input(INPUT_POST, "ctrl");
        $metodo = filter_input(INPUT_POST, "metodo");

        if (empty(Session::get('acesso'))) {
            $logar = $this->model->ValidaLogin($login, $pass)[0];
        } else {
            $logar = [
                'O_TOKEN' => Session::get('acesso'),
                'O_COD_RETORNO' => 0,
                'O_DESC_CURTO' => '',
                'O_TOKEN_INVALIDO' => 'N'
            ];
        }

        if (intval($logar['O_COD_RETORNO']) != 0) {

            if (intval($logar['O_COD_RETORNO']) != 66) {

                Session::set("log-invalido", TRUE);
                Session::set("erro-login", $logar['O_DESC_CURTO']);
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . $logar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            } else {
                $msg = 'Atenção! ' . $logar['O_DESC_CURTO'];
                $situacao = 'warning';

                Common::alert($msg, $situacao, 'acao');
                $registro = $logar['O_TOKEN_REG'];
                Common::redir('Login/RegistroLogin/' . $registro);
            }
        } else {

            Session::delete("logado");
            Session::set('social_tipo', 0);
            Session::set("logado", TRUE);
            Session::set("acesso", $logar['O_TOKEN']);
            Session::set('selecionado', 1);
            Session::set('ativacao', 0);

            $carrinho = Session::get('carrinho');
            $token = Session::get('acesso');

            $this->model->OrganizarCarrinho($carrinho, $token);

            $listacarrinho = $this->modelsistema->ListaCarrinhoCompra($token);

            //tratamento para gravar o tempo de sessão
            Session::delete("dadossessao");
            $dadossessao = [
                'token' => $token,
                'registered' => time()
            ];

            Session::set('dadossessao', $dadossessao);

            if ($metodo != "Checkout") {

                if(count($listacarrinho) > 0){
                    Common::redir('Usuarios/Checkout/' . $token);
                }else{
                    Common::redir('Home');
                }
            } else {
                Common::redir('Usuarios/Checkout/' . $token);
            }
        }
    }

    public function RegistroLogin($registro = null)
    {

        $config = $this->modelsistema->Configuracoes()[0];
        $redessoci = $this->modelsistema->RedesSociais();
        $social = parent::LoadAPISocial($config);
        $cadastrorealizado = empty($registro) ? false : true;
        $cadastroconcluido = !$cadastrorealizado ? true : boolval($this->model->VerificaEndCompleto($registro)['O_RETORNO']);

        if ($cadastrorealizado && $cadastroconcluido) {
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }

        $permissions = ['email'];
        $loginURLFSocial['facebook'] = $social['facebook']->getLoginUrl(FAIL_FACEBOOK_CALLBACK, $permissions);
        $loginURLFSocial['google-plus'] = $social['google-plus'];

        foreach ($redessoci as $row) {
            if (intval($row['RE_EXEC_LOGIN'] == 1)) {
                $dados['loginsocial'][$row['RS_NOME']] = $loginURLFSocial[$row['RS_NOME']];
            }
        }

        $dados['titulo'] = "Seu Cadastro";
        $dados['exibirlogos'] = false;
        $dados["urlactionlogin"] = SITE_URL . "/Login/LogarCliente";
        $dados["urlactionesqueci"] = SITE_URL . "/Login/ResertarSenha";
        $dados["urlcadastracliente"] = SITE_URL . "/Login/RegistroCliente";
        $dados['urlcadastraendcliente'] = SITE_URL . "/Login/ConcluirRegistroCliente/" . $registro;

        $dados['redessociais'] = $this->modelsistema->RedesSociais();
        $dados['cadastrorealizado'] = $cadastrorealizado;
        $dados['cadastroconcluido'] = $cadastroconcluido;

        parent::prepararView("Usuarios/pag_registro_login", $dados);
    }

    public function RegistroCliente()
    {
        $dados['I_NOME_COMPLETO'] = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_TEL_CEL'] = filter_input(INPUT_POST, "tel_cel");
        $dados['I_NUM_DOC'] = preg_replace('/\D/', '', filter_input(INPUT_POST, "cpf"));
        $dados['I_TIPO_DOC'] = filter_input(INPUT_POST, "tipo_doc");
        $dados['I_EMAIL'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $dados['I_DT_NASCIMENTO'] = filter_input(INPUT_POST, "data_nasc");
        $senha = filter_input(INPUT_POST, "senha_nova");
        $csenha = filter_input(INPUT_POST, "conf_senha");
        $pass = strtoupper(md5($dados['I_EMAIL'] . $senha));
        $cpass = strtoupper(md5($dados['I_EMAIL'] . $csenha));

        $dados['I_SENHA'] = $pass;
        $dados['I_CONF_SENHA'] = $cpass;

        $registrar = $this->model->RegistrarCliente($dados);

        if (intval($registrar['O_COD_RETORNO']) != 0) {

            Session::set("log-invalido", TRUE);
            Session::set("erro-login", $registrar['O_DESC_CURTO']);
            Session::set("erro-class", 'alert-danger pt-2 pb-2');

            $msg = 'Atenção! ' . $registrar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        } else {

            $registro = $registrar['O_TOKEN_REG'];
            Common::redir('Login/RegistroLogin/' . $registro);
        }
    }

    public function ConcluirRegistroCliente($registro)
    {
        $carrinho = Session::get("carrinho");

        $dados['I_TOKEN_REG'] = $registro;
        $dados['I_TOKEN_SES'] = Session::get('carrinho');
        $dados['I_LOGRADOURO'] = filter_input(INPUT_POST, "logradouro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_BAIRRO'] = filter_input(INPUT_POST, "bairro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_NUMERO'] = preg_replace('/\D/', '', filter_input(INPUT_POST, "numero"));
        $dados['I_CEP'] = preg_replace('/\D/', '', filter_input(INPUT_POST, "cep"));
        $dados['I_CIDADE'] = filter_input(INPUT_POST, "cidade", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_UF'] = filter_input(INPUT_POST, "uf");
        $dados['I_PAIS'] = filter_input(INPUT_POST, "pais", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_COMPLEMENTO'] = filter_input(INPUT_POST, "complemento", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        $dados['I_TIPO'] = filter_input(INPUT_POST, "tipo");
        $dados['I_END_PADRAO'] = 1;
        $dados['I_ACEITA_CONDICOES'] = filter_input(INPUT_POST, "aceita_condicoes");

        $concluir = $this->model->ConcluirRegistroCliente($dados);

        if (intval($concluir['O_COD_RETORNO']) != 0) {

            Session::set("log-invalido", TRUE);
            Session::set("erro-login", $concluir['O_DESC_CURTO']);
            Session::set("erro-class", 'alert-danger pt-2 pb-2');

            $msg = 'Atenção! ' . $concluir['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        } else {
            $token = $concluir['O_TOKEN'];
            $checkout = intval($concluir['O_CHECKOUT']);

            Session::delete("logado");
            Session::set('social_tipo', 0);
            Session::set("logado", TRUE);
            Session::set("acesso", $token);
            Session::set('selecionado', 1);
            Session::set('ativacao', 0);

            if ($checkout == 1) {
                $this->model->OrganizarCarrinho($carrinho, $token);
                Session::delete("carrinho");

                Common::redir('Usuarios/Checkout/' . $token);
            } else {
                Common::redir('Usuarios/Bemvindo');
            }
        }
    }

    public function SocialRedirect($op)
    {
        $config = $this->modelsistema->Configuracoes()[0];
        $social = parent::LoadAPISocial($config);

        if ($op == 1) {
            try {
                $accessToken = $social['facebook']->getAccessToken();
            } catch (FacebookResponseException $e) {

                Session::set("log-invalido", TRUE);
                Session::set("erro-login", "SDK Exception: " . $e->getMessage());
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . "SDK Exception: " . $e->getMessage();
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home');
            } catch (FacebookSDKException $e) {

                Session::set("log-invalido", TRUE);
                Session::set("erro-login", "SDK Exception: " . $e->getMessage());
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . "SDK Exception: " . $e->getMessage();
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home');
            }

            if (!$accessToken) {
                Session::set("log-invalido", TRUE);
                Session::set("erro-login", "É necessário logar no Facebook");
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . "É necessário logar no Facebook";
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home');
            }

            $oAuth2Client = $social['fb']->getOAuth2Client();

            if (!$accessToken->isLongLived()) {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            }

            $response = $social['fb']->get("/me?fields=id, first_name, last_name, email, picture.type(large)", $accessToken);
            $userData = $response->getGraphNode()->asArray();

            $token = (string) $accessToken;

            $now = new \DateTime();
            $pasta = $now->format('Y_m_d_H_i_s');

            if (!file_exists(FOTOCLIENTE_URL . '/' . $pasta)) {
                mkdir(FOTOCLIENTE_URL . '/' . $pasta, 0777, true);
            }

            $retorno = $this->modelsistema->GravarUsuarioSocial($token, $userData, $pasta, $op);

            if (intval($retorno['O_COD_RETORNO']) == 0) {
                Session::delete("logado");
                Session::set('social_tipo', 1);
                Session::set("logado", TRUE);
                Session::set("acesso", $retorno['O_TOKEN']);
                Session::set('selecionado', 1);

                Common::redir('Home');
            }
        } elseif ($op == 2) {
            try {
                $data = parent::GetGoogleAccessToken($social['gg'], $_GET['code']);

                if (!$data['access_token']) {
                    Session::set("log-invalido", TRUE);
                    Session::set("erro-login", "É necessário logar no Google Plus");
                    Session::set("erro-class", 'alert-danger pt-2 pb-2');

                    $msg = 'Atenção! ' . "É necessário logar no Google Plus";
                    $situacao = 'danger';

                    Common::alert($msg, $situacao, 'acao');
                    Common::redir('Home');
                }

                $userData = parent::GetGoogleUserProfileInfo($data['access_token']);

                $token = (string) $data['access_token'];

                $now = new \DateTime();
                $pasta = $now->format('Y_m_d_H_i_s');

                if (!file_exists(FOTOCLIENTE_URL . '/' . $pasta)) {
                    mkdir(FOTOCLIENTE_URL . '/' . $pasta, 0777, true);
                }

                $retorno = $this->modelsistema->GravarUsuarioSocial($token, $userData, $pasta, $op);

                if (intval($retorno['O_COD_RETORNO']) == 0) {
                    Session::delete("logado");
                    Session::set('social_tipo', 1);
                    Session::set("logado", TRUE);
                    Session::set("acesso", $retorno['O_TOKEN']);
                    Session::set('selecionado', 1);

                    Common::redir('Home');
                }
            } catch (\Exception $e) {
                Session::set("log-invalido", TRUE);
                Session::set("erro-login", "SDK Exception: " . $e->getMessage());
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . "SDK Exception: " . $e->getMessage();
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home');
            }
        }
    }

    public function ResertarSenha()
    {
        $this->validarCamposObrigatorio(3);

        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);

        $dados = $this->model->ClientePorEmail($email)[0];
        $config = $this->modelsistema->ConfigEnvioEmail()[0];

        $parametros = [
            'CLI_ID' => $dados['CLI_ID'],
            'CLI_EMAIL' => $email,
            'CLI_SENHA' => '',
            'CLI_STATUS' => 0
        ];

        $resetar = $this->model->TrocarSenhaCliente($parametros)[0];

        if (intval($resetar['O_COD_RETORNO']) != 0) {
            $msg = $resetar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        } else {

            $info = array(
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            );

            $enviado = Common::dispararEmail($dados, $email, 'Santos Dumont - Reset de Senha', $info, 2);

            if ($enviado) {
                $msg = 'Senha resetada com sucesso!<br/>Você receberá um e-mail para gerar uma nova senha.';
                $situacao = 'success';
            } else {
                $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                $situacao = 'danger';
            }

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        }
    }

    public function TrocarSenha()
    {
        $this->validarCamposObrigatorio(3);

        $id = filter_input(INPUT_POST, "id");
        $code = filter_input(INPUT_POST, "code");
        $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $senha = filter_input(INPUT_POST, "senha");
        $confsenha = filter_input(INPUT_POST, "confsenha");

        if ($senha != $confsenha) {
            $msg = 'Você deve confirmar a senha!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home/Reset/' . $code);
        } else {

            $senha = strtoupper(md5($email . $confsenha));

            $parametros = [
                'CLI_ID' => $id,
                'CLI_EMAIL' => $email,
                'CLI_SENHA' => $senha,
                'CLI_STATUS' => 1
            ];

            $resetar = $this->model->TrocarSenhaCliente($parametros);

            if ($resetar['list']['O_COD_RETORNO'] != 0) {
                $msg = $resetar['list']['o_ret_msg'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home/Reset/' . $code);
            } else {
                $msg = 'Senha resetada com sucesso!<br/>Você já pode realizar o login no sistema.';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Home');
            }
        }
    }

    private function validarCamposObrigatorio($tipo)
    {

        if ($tipo == 1) {
            $dados['E-Mail'] = filter_input(INPUT_POST, "email_login", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
            $dados['Senha'] = filter_input(INPUT_POST, "password_login");

            Common::validarInputsObrigatorio($dados, 'Home');
        }

        if ($tipo == 2) {
            $dados['Confirmar E-mail'] = filter_input(INPUT_POST, "confemail_login_reg", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
            $dados['E-mail'] = filter_input(INPUT_POST, "email_login_reg", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);

            Common::validarInputsObrigatorio($dados, 'Home');
        }

        if ($tipo == 3) {
            $dados['E-mail'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);

            Common::validarInputsObrigatorio($dados, 'Home');
        }
    }
}
