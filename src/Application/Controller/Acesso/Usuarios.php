<?php

namespace Application\Controller\Acesso;

require_once SYSTEM_PATH . 'vendor/santosdummontsite/vendor/autoload.php';

use santosdummontsite\Controller,
    santosdummontsite\Common,
    santosdummontsite\Session;

class Usuarios extends Controller
{

    public function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');
        parent::loadModel('Application\Model\ModelSistema', 'modelsistema');
    }

    public function main()
    {
    }

    public function Bemvindo()
    {
        $token = Session::get('acesso');

        $config = $this->modelsistema->Configuracoes()[0];

        $dados['logado'] = true;
        $dados['exibirlogos'] = false;
        $dados['titulo'] = "Seja bem vindo";
        $dados['config'] = $config;

        parent::prepararView("Usuarios/bem_vindo", $dados);
    }

    public function MinhaConta($id = null)
    {
        $dados['exibirlogos'] = true;

        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            if (intval(Session::get('ativacao')) == 1) {
                $token = Session::get('acesso');
                $social_tipo = Session::get('social_tipo');
                $dados['cliente'] = $this->modelsistema->UsuarioPorToken($token, $social_tipo);
            }

            parent::prepararView("Usuarios/pag_minha_conta", $dados);
        }
    }

    public function Carrinhoold($id, $pagina = null)
    {
        if (empty(Session::get("acesso"))) {
            $token = Session::get('carrinho');
            $dados['logado'] = false;
            $logado = false;
        } else {
            $token = Session::get('acesso');
            $dados['logado'] = true;
            $logado = true;
        }

        $busca['ITENS'] = 12;

        $total = $this->model->TotalCheckout($token, $logado)[0];
        $totalpaginas = ceil($total['TOTAL'] / $busca['ITENS']);

        if ($pagina > $totalpaginas) {
            $$pagina = $totalpaginas;
        }
        if ($pagina < 1) {
            $pagina = 1;
        }

        $busca['TOKEN'] = $token;
        $busca['OFFSET'] = ($pagina - 1) * $busca['ITENS'];

        $dados['exibirlogos'] = false;
        $dados['listameiosentrega'] = $this->model->ListaMeiosEntrega($token);
        $dados['listacheckout'] = $this->model->ListaCheckout($busca, $logado);
        $dados['totaischeckout'] = $this->model->TotaisCheckout($token, $logado);
        $dados['totalpaginas'] = $totalpaginas;
        $dados['paginaatual'] = $pagina;
        $dados['resultados'] = $total['TOTAL'];
        $dados['urlaction'] = SITE_URL . '/Usuarios/Carrinho/' . $id;

        //cálculo de frete
        $dados['calcularfrete'] = SITE_URL . "/Usuarios/CalcularFrete";
        $dados['cancelarcompra'] = SITE_URL . "/Usuarios/CancelarCompra";
        $dados['token'] = $token;

        if (!empty(Session::get('sessfrete'))) {
            $sessfrete = Session::get('sessfrete');

            if (intval($sessfrete['PRAZO']) > 1) {
                $dados['sessPrazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dias úteis' : null;
            } else {
                $dados['sessPrazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dia útil' : null;
            }

            $dados['sessFrete'] = str_replace(',', '.', str_replace('.', '', $sessfrete['FRETE']));
            $dados['sessTotal'] = str_replace(',', '.', str_replace('.', '', $sessfrete['TOTAL_PEDIDO']));
            $dados['sessPreco'] = str_replace(',', '.', str_replace('.', '', $sessfrete['PRECO_TOTAL']));
            $dados['sessCep'] = $sessfrete['CEP_DESTINO'];
            $dados['sesTipoEntr'] = $sessfrete['TIPO_ENTREGA'];
            $dados['sesTipoEntrImagem'] = IMGS_URL . '/' . $sessfrete['IMAGEM'];
        } else {
            $dados['sessPrazo'] = null;
            $dados['sessFrete'] = null;
            $dados['sessTotal'] = null;
            $dados['sessPreco'] = null;
            $dados['sessCep'] = null;
            $dados['sesTipoEntr'] = null;
            $dados['sesTipoEntrImagem'] = null;
        }
        parent::prepararView("Usuarios/pag_carrinho", $dados);
    }

    public function EnviarLoja()
    {
        $dados["urlaction"] = SITE_URL . '/Usuarios/EnviarParaLoja';
        parent::prepararBasicView("Home/modal_contato_envia", $dados);
    }

    public function EnviarParaLoja(){

        $contatosite = $this->modelsistema->ContatosdoSite()[0];

        if (empty(Session::get('acesso'))) {
            if (empty(Session::get('carrinho'))) {
                $token = session_id();
                Session::set('carrinho', $token);
            } else {
                $token = Session::get('carrinho');
            }
        } else {
            $token = Session::get('acesso');
        }

        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
        $telefone = filter_input(INPUT_POST, 'telefone');
        $nomeCompleto = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        $config = $this->modelsistema->ConfigEnvioEmail()[0];
        $listaprodutos = $this->model->ListaProdutosCarrinhoCliente($token);

        $registro = $this->model->registrarIntencaoCompra($token);

        $titulo = 'Santos Dumont - Pedido enviado para Loja Física  - '.$registro['O_IDENTIFICADOR'];

        $html = '<html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                            <title>'.$titulo.'</title>
                        </head>
                        <body>
                            <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                                <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                    <img src="' . URL_IMG_MAIL . '/' . $config['LOGOTIPO'] . '">
                                </div>
                                <div class="container" style="padding:20px">
                                <p>O cliente '.$nomeCompleto.' ('.$email.') entrou em contato através do site enviando as seguinte relação de produtos para compra.<br/>
                                Entre em contato o mais rápido possível através do telefone: <a href="https://api.whatsapp.com/send/?phone=55'.$telefone.'">'.$telefone.'</a>!</p>
                                </div>
                                <div class="container" style="padding:20px">';

        $html .= '<table>
            <thead>
                <tr>
                    <th class="checkout-description">SKU</th>
                    <th class="checkout-quantity">QTDE</th>
                    <th class="checkout-description">Descrição</th>
                    <th class="checkout-model hidden-xs">Modelo</th>
                    <th class="checkout-price">Preço</th>
                    <th class="checkout-total">Total</th>
                </tr>
            </thead>
            <tbody>';
                foreach ($listaprodutos as $produto) {

                    //$principal = Common::imagem_existe($produto['PASTA_IMAGENS'], $produto['PRD_IMAGEM_PRINCIPAL'], 2);

                    $linkprod = URL . '/Produtos/Item/' . $produto['PRD_PATH'];

                    $html .= '<tr>
                        <td class="checkout-description">
                            <h5><a href="'.$linkprod.'" target="_blank">'.$produto['PRD_SKU'].'</a></h5>
                        </td>
                        <td class="checkout-quantity">'.$produto['QTDE'].'</td>
                        <td class="checkout-description">
                            <h5><a href="'.$linkprod.'" target="_blank">'.$produto['PRD_NOME'].'</a></h5>
                        </td>
                        <td class="checkout-model hidden-xs">'.$produto['PRD_MODELO'].'</td>
                        <td class="checkout-price">
                            <strong><span>R$</span>'.number_format($produto['PRD_PRECO'], 2, ',', '.').'</strong>
                        </td>
                        <td class="checkout-total">
                            <strong><span>R$</span>'.number_format($produto['PRECO_TOTAL'], 2, ',', '.').'</strong>
                        </td>
                    </tr>';
                }
        $html .= '</tbody>
                </table>
                    </div>
                </div>
            </body>
        </html>';

        $info = [
            'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
            'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
            'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
            'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
            'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
            'fromname' => $config['NOME_CONTATO_DEFAULT'],
            'url' => $config['URL_SISTEMA'],
            'logotipo' => $config['LOGOTIPO']
        ];

        $enviado = Common::dispararEmailPersonalizado($titulo, $html, 'vendas@sd68.com.br', $info);

        if ($enviado) {
            $msg = 'Pedido enviado com sucesso!<br/>A loja entrará em contato contigo.';
            $situacao = 'success';
        } else {
            $msg = 'Ocorreu um erro ao enviar pedido!<br/>Entre em contato com o administrador.';
            $situacao = 'danger';
        }

        $infolink = '&text=Ol%C3%A1%2C%20quero%20falar%20com%20um%20Consultor%20de%20Vendas%20-%20código%20do%20carrinho%20-%20';
        $linkretorno ='https://api.whatsapp.com/send?phone=55' . $contatosite['WHATSAPP'].$infolink.$registro['O_IDENTIFICADOR'];

        //Common::alert($msg, $situacao, 'acao');
        //Common::redir('Home');

        Common::redir($linkretorno, 1);
    }

    public function CheckoutOld($id, $pagina = null)
    {
        if (empty(Session::get("acesso"))) {
            Common::redir('Login/RegistroLogin');
        } else {
            $token = Session::get('acesso');
            $dados['logado'] = true;
        }

        $config = $this->modelsistema->Configuracoes()[0];

        $dados['titulo'] = "Finalizar Compra";
        $dados['urlaction'] = SITE_URL . '/Usuarios/Checkout/' . $id;
        $dados['urlgerarpedido'] = SITE_URL . '/Usuarios/GerarPedido/' . $token;
        $dados['calcularfrete'] = SITE_URL . "/Usuarios/CalcularFrete";
        $dados['cancelarcompra'] = SITE_URL . "/Usuarios/CancelarCompra";
        $dados['token'] = $token;
        $dados['exibirlogos'] = false;

        $busca['ITENS'] = 12;

        $total = $this->model->TotalCheckout($token)[0];
        $totalpaginas = ceil($total['TOTAL'] / $busca['ITENS']);

        if ($pagina > $totalpaginas) {
            $$pagina = $totalpaginas;
        }
        if ($pagina < 1) {
            $pagina = 1;
        }

        $busca['TOKEN'] = $token;
        $busca['OFFSET'] = ($pagina - 1) * $busca['ITENS'];

        try {
            \PagSeguro\Library::initialize();
            $credenciais = $this->retornaCredenciaisPagSeguro($config);

            $sessionId = \PagSeguro\Services\Session::create($credenciais);
        } catch (\Exception $e) {
            $msg = Common::retornoMsgXml($e->getMessage());
            $situacao = 'danger';
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
            Common::redir('Usuarios/Checkout/' . $id);
        }

        $dados['listameiosentrega'] = $this->model->ListaMeiosEntrega($token);
        $dados['cartoescliente'] = $this->model->ListaCartoesCliente($token);
        $dados['meiospagamentos'] = $this->modelsistema->ListaMeiosPagamentos();
        $dados['listacheckout'] = $this->model->ListaCheckout($busca);
        $dados['totaischeckout'] = $this->model->TotaisCheckout($token);
        $dados['totalpaginas'] = $totalpaginas;
        $dados['paginaatual'] = $pagina;
        $dados['resultados'] = $total['TOTAL'];
        $dados['sessionId'] = $sessionId->getResult();

        if (!empty(Session::get('sessfrete'))) {
            $sessfrete = Session::get('sessfrete');

            if (intval($sessfrete['PRAZO']) > 1) {
                $dados['sessPrazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dias úteis' : null;
            } else {
                $dados['sessPrazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dia útil' : null;
            }

            $dados['sessFrete'] = str_replace(',', '.', str_replace('.', '', $sessfrete['FRETE']));
            $dados['sessTotal'] = str_replace(',', '.', str_replace('.', '', $sessfrete['TOTAL_PEDIDO']));
            $dados['sessPreco'] = str_replace(',', '.', str_replace('.', '', $sessfrete['PRECO_TOTAL']));
            $dados['sessCep'] = $sessfrete['CEP_DESTINO'];
            $dados['sesTipoEntr'] = $sessfrete['TIPO_ENTREGA'];
            $dados['sesTipoEntrImagem'] = IMGS_URL . '/' . $sessfrete['IMAGEM'];
        } else {
            $dados['sessPrazo'] = null;
            $dados['sessFrete'] = null;
            $dados['sessTotal'] = null;
            $dados['sessPreco'] = null;
            $dados['sessCep'] = null;
            $dados['sesTipoEntr'] = null;
            $dados['sesTipoEntrImagem'] = null;
        }

        parent::prepararView("Usuarios/pag_checkout_log", $dados);
    }

    public function CancelarCompra()
    {
        $token = Session::get('acesso');

        $cancelar = $this->model->CancelarCompraUsuario($token);

        echo json_encode($cancelar);
    }

    public function CalcularFrete($id = null)
    {
        $token = empty(Session::get("acesso")) ? Session::get('carrinho') : Session::get("acesso");

        $contatosite = $this->modelsistema->ContatosdoSite()[0];

        $cep = filter_input(INPUT_POST, 'cep_frete');
        $tipo_entrega = filter_input(INPUT_POST, 'tipo_entrega');
        $qtd = intval(filter_input(INPUT_POST, 'qtde'));
        $cep_destino = Common::somenteNumeros($cep);
        $cep_origem = Common::somenteNumeros($contatosite['CEP']);

        $ret = parent::valida_cep_correio($cep_destino);

        if (!empty($id)) {
            $atualiza = $this->model->AtualizarProdutosCarrinho($token, 0, $id, $qtd);
        } else {
            $atualiza = $this->model->AtualizarProdutosCarrinho($token, 1);
        }
        $pesoPedido = $this->model->PesoRealPedido($token, intval($id), intval($qtd));

        if (!empty($ret['erro']) && intval($ret['erro']) == 1) {
            $errovl = number_format(0, 2, ',', '');

            echo json_encode(['O_COD_RETORNO' => 1, 'MSG' => 'CEP Inválido', 'PRAZO' => 0, 'FRETE' => $errovl, 'TOTAL_PEDIDO' => $errovl, 'PRECO_TOTAL' => $errovl]);
        } else {
            //verifica o tipo de meio de entrega
            $meioentrega = $this->model->DadosMeioEntrega($tipo_entrega)[0];

            if ((intval($meioentrega['CALC_FRETE']) > 0) && (intval($meioentrega['RESTRICAO_ABRANG']) != 1)) {
                $raiz_cubica = round(pow($pesoPedido['PESO_CUBICO'], 1 / 3), 2);
                $comprimento =  $raiz_cubica < 16 ? 16 : $raiz_cubica;
                $altura = $raiz_cubica < 2 ? 2 : $raiz_cubica;
                $largura = $raiz_cubica < 11 ? 11 : $raiz_cubica;
                $peso = $pesoPedido['PESO'] < 0.3 ? 0.3 : $pesoPedido['PESO'];
                $diametro = hypot($comprimento, $largura);

                $frete = Common::calcValodFrete($tipo_entrega, $cep_origem, $cep_destino, $peso, $altura, $largura, $comprimento, $diametro);
                $freteTotal = $frete['VALOR'];
                $prazototal = $frete['PRAZO'];
                $msgerro = $frete['MSG'];
                $total = $atualiza['PRECO_TOTAL'] + $freteTotal;

                $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                $vl_total = number_format($total, 2, ',', '.');
                $vl_frete = number_format($freteTotal, 2, ',', '.');

                $sessfrete = [
                    'PRAZO' => $prazototal,
                    'FRETE' => $vl_frete,
                    'TOTAL_PEDIDO' => $vl_total,
                    'PRECO_TOTAL' => $vl_prods,
                    'CEP_DESTINO' => $cep,
                    'TIPO_ENTREGA' => $tipo_entrega,
                    'IMAGEM' => $meioentrega['IMAGEM']
                ];

                if (!empty(Session::get('sessfrete'))) {
                    Session::set('sessfrete', null);
                }
                Session::set('sessfrete', $sessfrete);

                echo json_encode(['O_COD_RETORNO' => 1, 'MSG' => $msgerro, 'PRAZO' => $prazototal, 'FRETE' => $vl_frete, 'TOTAL_PEDIDO' => $vl_total, 'PRECO_TOTAL' => $vl_prods], JSON_UNESCAPED_UNICODE);
            } elseif ((intval($meioentrega['CALC_FRETE']) == 1) && (intval($meioentrega['RESTRICAO_ABRANG']) == 1)) {

                //VERIFICA SE O CEP ESTÁ NA ÁREA DE ABRANGÊNCIA
                $buscacep = self::InfoAbrangenciaComCep($meioentrega['MEIO_ID'], $cep_destino);
                $vl_frete = 0;
                $vl_total = 0;
                $vl_prods = 0;
                if (intval($buscacep['ERRO']) == 0) {

                    if (intval($buscacep['EXISTE']) == 1) {
                        $vl_frete = number_format($meioentrega['VALOR_COBRADO'], 2, ',', '');

                        $total = $atualiza['PRECO_TOTAL'] + $meioentrega['VALOR_COBRADO'];

                        $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                        $vl_total = number_format($total, 2, ',', '.');

                        $sessfrete = [
                            'PRAZO' => intval($meioentrega['PRAZO_MAXIMO']),
                            'FRETE' => !empty($meioentrega['VALOR_COBRADO']) ? $vl_frete : null,
                            'TOTAL_PEDIDO' => $vl_total,
                            'PRECO_TOTAL' => $vl_prods,
                            'CEP_DESTINO' => $cep,
                            'TIPO_ENTREGA' => $tipo_entrega,
                            'IMAGEM' => $meioentrega['IMAGEM']
                        ];

                        if (!empty(Session::get('sessfrete'))) {
                            Session::set('sessfrete', null);
                        }
                        Session::set('sessfrete', $sessfrete);

                        echo json_encode(['O_COD_RETORNO' => 0, 'MSG' => 'Sucesso', 'PRAZO' => intval($meioentrega['PRAZO_MAXIMO']), 'FRETE' => $vl_frete, 'TOTAL_PEDIDO' => $vl_total, 'PRECO_TOTAL' => $vl_prods]);
                    } else {

                        $vl_frete = number_format(NULL, 2, ',', '');

                        $total = $atualiza['PRECO_TOTAL'] + 0;

                        $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                        $vl_total = number_format($total, 2, ',', '.');
                        $vl_frete = number_format(0, 2, ',', '.');

                        $sessfrete = [
                            'PRAZO' => 0,
                            'FRETE' => $vl_frete,
                            'TOTAL_PEDIDO' => $vl_total,
                            'PRECO_TOTAL' => $vl_prods,
                            'CEP_DESTINO' => $cep,
                            'TIPO_ENTREGA' => $tipo_entrega,
                            'IMAGEM' => $meioentrega['IMAGEM']
                        ];

                        if (!empty(Session::get('sessfrete'))) {
                            Session::set('sessfrete', null);
                        }
                        Session::set('sessfrete', $sessfrete);

                        echo json_encode(['O_COD_RETORNO' => 1, 'MSG' => 'Este endereço não está na área de abrangência de entrega pelo meio selecionado, por gentileza selecione outro', 'PRAZO' => 0, 'FRETE' => $vl_frete, 'TOTAL_PEDIDO' => $vl_total, 'PRECO_TOTAL' => $vl_prods]);
                    }
                } else {
                    echo json_encode(['O_COD_RETORNO' => 1, 'MSG' => 'Erro ao verificar área de abrangência', 'PRAZO' => 0, 'FRETE' => $vl_frete, 'TOTAL_PEDIDO' => $vl_total, 'PRECO_TOTAL' => $vl_prods]);
                }
            } else {
                $vl_frete = number_format($meioentrega['VALOR_COBRADO'], 2, ',', '');

                $total = $atualiza['PRECO_TOTAL'] + $meioentrega['VALOR_COBRADO'];

                $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                $vl_total = number_format($total, 2, ',', '.');

                $sessfrete = [
                    'PRAZO' => intval($meioentrega['PRAZO_MAXIMO']),
                    'FRETE' => !empty($meioentrega['VALOR_COBRADO']) ? $vl_frete : null,
                    'TOTAL_PEDIDO' => $vl_total,
                    'PRECO_TOTAL' => $vl_prods,
                    'CEP_DESTINO' => $cep,
                    'TIPO_ENTREGA' => $tipo_entrega,
                    'IMAGEM' => $meioentrega['IMAGEM']
                ];

                if (!empty(Session::get('sessfrete'))) {
                    Session::set('sessfrete', null);
                }
                Session::set('sessfrete', $sessfrete);

                echo json_encode(['O_COD_RETORNO' => 0, 'MSG' => '', 'PRAZO' => intval($meioentrega['PRAZO_MAXIMO']), 'FRETE' => $vl_frete, 'TOTAL_PEDIDO' => $vl_total, 'PRECO_TOTAL' => $vl_prods]);
            }
        }
    }

    public function ListaMeiosEntregaAbrangencia($token)
    {

        $lista = [];
        $listameios = $this->model->ListaMeiosEntregaCepToken($token);

        foreach ($listameios as $row) {
            if (!empty($row['CEP_USER']) && intval($row['RESTRICAO_ABRANG'] == 1)) {

                $ret = self::InfoAbrangenciaComCep($row['MEIO_ID'], $row['CEP_USER']);

                if (intval($ret['ERRO']) == 0 && intval($ret['EXISTE']) > 0) {
                    $lista[] = $row;
                }
            } else {
                $lista[] = $row;
            }
        }

        return $lista;
    }

    public function AjaxInibirOpcaoEntrega()
    {

        $str = filter_input(INPUT_POST, 'cep');

        $cep = preg_replace('/[^0-9]/', '', (string) $str);
        $url = "http://viacep.com.br/ws/" . $cep . "/json/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);

        if (!isset($json->erro)) {
            $params = [
                'UF' => $json->uf,
                'CIDADE' => $json->localidade
            ];

            $res = $this->model->CidadesAbrangenciaMeio($params);

            $dados = [
                'ERRO' => 0,
                'MEIOS' => $res
            ];
        } else {
            $dados = [
                'ERRO' => 1,
                'MEIOS' => null
            ];
        }
        return $dados;
    }

    public function InfoAbrangenciaComCep($id, $str)
    {
        $cep = preg_replace('/[^0-9]/', '', (string) $str);
        $url = "http://viacep.com.br/ws/" . $cep . "/json/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($result);

        if (!isset($json->erro)) {
            $params = [
                'MEIO_ID' => $id,
                'UF' => $json->uf,
                'CIDADE' => $json->localidade
            ];

            $res = $this->model->AreaAbrangenciaMeio($params);

            $dados = [
                'ERRO' => 0,
                'EXISTE' => $res[0]['EXISTE']
            ];
        } else {
            $dados = [
                'ERRO' => 1,
                'EXISTE' => 0
            ];
        }
        return $dados;
    }

    public function RemoverItemCarrinho()
    {
        $token = empty(Session::get("acesso")) ? Session::get('carrinho') : Session::get("acesso");

        $id = filter_input(INPUT_POST, "id");
        $remover = $this->model->RemoveItemCarrinho($id, $token);

        echo json_encode($remover);
    }

    public function AddRemoverQtdCarrinho()
    {
        $token = empty(Session::get("acesso")) ? Session::get('carrinho') : Session::get("acesso");

        $retorno = [];

        $id = filter_input(INPUT_POST, "id");
        $qtde = filter_input(INPUT_POST, "qtde");

        $remover = $this->model->AddRemQtdCarrinho($id, $qtde, $token);

        if (intval($remover['O_COD_RETORNO']) != 0) {
            $retorno['O_COD_RETORNO'] = $remover['O_COD_RETORNO'];
            $retorno['O_DESC_CURTO'] = $remover['O_DESC_CURTO'];
            $retorno['O_TOKEN_INVALIDO'] = $remover['O_TOKEN_INVALIDO'];
            $retorno['O_PRECO_PRODUTO'] = number_format(0, 2, ',', '');
            $retorno['O_PRECO_TOTAL'] = number_format(0, 2, ',', '');
            $retorno['O_PRECO_TOTAL_PRODUTOS'] = number_format(0, 2, ',', '');
            $retorno['O_PRECO_TOTAL_PEDIDO'] = number_format(0, 2, ',', '');
        } else {
            $retorno['O_COD_RETORNO'] = $remover['O_COD_RETORNO'];
            $retorno['O_DESC_CURTO'] = $remover['O_DESC_CURTO'];
            $retorno['O_TOKEN_INVALIDO'] = $remover['O_TOKEN_INVALIDO'];
            $retorno['O_PRECO_PRODUTO'] = number_format($remover['O_PRECO_PRODUTO'], 2, ',', '');
            $retorno['O_PRECO_TOTAL'] = number_format($remover['O_PRECO_TOTAL'], 2, ',', '');
            $retorno['O_PRECO_TOTAL_PRODUTOS'] = number_format($remover['O_PRECO_TOTAL_PRODUTOS'], 2, ',', '');
            $retorno['O_PRECO_TOTAL_PEDIDO'] = number_format($remover['O_PRECO_TOTAL_PEDIDO'], 2, ',', '');
        }

        //calcular o frete e o total do pedido
        $sessfrete = Session::get('sessfrete');

        if (!empty($sessfrete['TIPO_ENTREGA'])) {

            $atualiza = $this->model->AtualizarProdutosCarrinho($token, 1);

            $tipo_entrega = $sessfrete['TIPO_ENTREGA'];
            $cep_destino = Common::somenteNumeros($sessfrete['CEP_DESTINO']);

            $contatosite = $this->modelsistema->ContatosdoSite()[0];

            $cep_origem = Common::somenteNumeros($contatosite['CEP']);

            $pesoPedido = $this->model->PesoRealPedido($token, 0, 0);

            try {
                if (intval($atualiza['O_COD_RETORNO']) == 0) {

                    //verifica o tipo de meio de entrega
                    $meioentrega = $this->model->DadosMeioEntrega($tipo_entrega)[0];

                    if (intval($meioentrega['CALC_FRETE']) == 1 && !empty($cep_destino)) {

                        $raiz_cubica = round(pow($pesoPedido['PESO_CUBICO'], 1 / 3), 2);
                        $comprimento =  $raiz_cubica < 16 ? 16 : $raiz_cubica;
                        $altura = $raiz_cubica < 2 ? 2 : $raiz_cubica;
                        $largura = $raiz_cubica < 11 ? 11 : $raiz_cubica;
                        $peso = $pesoPedido['PESO'] < 0.3 ? 0.3 : $pesoPedido['PESO'];
                        $diametro = hypot($comprimento, $largura);

                        $frete = Common::calcValodFrete($tipo_entrega, $cep_origem, $cep_destino, $peso, $altura, $largura, $comprimento, $diametro);
                        $freteTotal = $frete['VALOR'];
                        $prazototal = $frete['PRAZO'];
                        $msgerro = $frete['MSG'];

                        $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                        $vl_total = number_format(($atualiza['PRECO_TOTAL'] + $freteTotal), 2, ',', '.');
                        $vl_frete = number_format($freteTotal, 2, ',', '.');

                        $retorno['O_PRECO_TOTAL_PRODUTOS'] = $vl_prods;
                        $retorno['O_FRETE'] = $vl_frete;
                        $retorno['O_PRAZO'] = $prazototal;
                        $retorno['O_PRECO_TOTAL_PEDIDO'] = $vl_total;
                        $retorno['O_MSG'] = $msgerro;
                    } else {
                        $freteTotal = $meioentrega['VALOR_COBRADO'];
                        $prazo = intval($meioentrega['PRAZO_MAXIMO']);

                        $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                        $vl_total = number_format(($atualiza['PRECO_TOTAL'] + $freteTotal), 2, ',', '.');
                        $vl_frete = number_format($freteTotal, 2, ',', '.');

                        $retorno['O_PRECO_TOTAL_PRODUTOS'] = $vl_prods;
                        $retorno['O_FRETE'] = $vl_frete;
                        $retorno['O_PRAZO'] = $prazo;
                        $retorno['O_PRECO_TOTAL_PEDIDO'] = $vl_total;
                        $retorno['O_MSG'] = '';
                    }
                } else {
                    //$retorno['O_PRECO_TOTAL_PRODUTOS'] = 0;
                    $retorno['O_FRETE'] = 0;
                    $retorno['O_PRAZO'] = 0;
                    //$retorno['O_PRECO_TOTAL_PEDIDO'] = 0;
                    $retorno['O_MSG'] = $atualiza['O_DESC_CURTO'];;
                }
            } catch (\Exception $e) {
                //$retorno['O_PRECO_TOTAL_PRODUTOS'] = 0;
                $retorno['O_FRETE'] = 0;
                //$retorno['O_PRECO_TOTAL_PEDIDO'] = 0;
                $retorno['O_MSG'] = $e->getMessage();
            }
        }

        echo json_encode($retorno);
    }

    public function AtualizarCarrinho()
    {
        $sessfrete = Session::get('sessfrete');
        $token = empty(Session::get("acesso")) ? Session::get('carrinho') : Session::get("acesso");

        $atualiza = $this->model->AtualizarProdutosCarrinho($token, 1);

        $tipo_entrega = !empty($sessfrete['TIPO_ENTREGA']) ? $sessfrete['TIPO_ENTREGA'] : filter_input(INPUT_POST, 'tipo_entrega');
        $cep_destino = Common::somenteNumeros(filter_input(INPUT_POST, 'cep_frete'));

        $contatosite = $this->modelsistema->ContatosdoSite()[0];

        $cep_origem = Common::somenteNumeros($contatosite['CEP']);

        $pesoPedido = $this->model->PesoRealPedido($token, 0, 0);

        try {
            if (intval($atualiza['O_COD_RETORNO']) == 0) {

                //verifica o tipo de meio de entrega
                $meioentrega = $this->model->DadosMeioEntrega($tipo_entrega)[0];

                if (intval($meioentrega['CALC_FRETE']) == 1 && !empty($cep_destino)) {

                    $raiz_cubica = round(pow($pesoPedido['PESO_CUBICO'], 1 / 3), 2);
                    $comprimento =  $raiz_cubica < 16 ? 16 : $raiz_cubica;
                    $altura = $raiz_cubica < 2 ? 2 : $raiz_cubica;
                    $largura = $raiz_cubica < 11 ? 11 : $raiz_cubica;
                    $peso = $pesoPedido['PESO'] < 0.3 ? 0.3 : $pesoPedido['PESO'];
                    $diametro = hypot($comprimento, $largura);

                    $frete = Common::calcValodFrete($tipo_entrega, $cep_origem, $cep_destino, $peso, $altura, $largura, $comprimento, $diametro);
                    $freteTotal = $frete['VALOR'];
                    $prazototal = $frete['PRAZO'];
                    $msgerro = $frete['MSG'];


                    $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                    $vl_total = number_format(($atualiza['PRECO_TOTAL'] + $freteTotal), 2, ',', '.');
                    $vl_frete = number_format($freteTotal, 2, ',', '.');

                    $exibir = [
                        'O_COD_RETORNO' => $atualiza['O_COD_RETORNO'],
                        'PRECO_TOTAL' => $vl_prods,
                        'FRETE' => $vl_frete,
                        'PRAZO' => $prazototal,
                        'TOTAL_PEDIDO' => $vl_total,
                        'MSG' => $msgerro
                    ];
                } else {
                    $freteTotal = $meioentrega['VALOR_COBRADO'];
                    $prazo = intval($meioentrega['PRAZO_MAXIMO']);

                    $vl_prods = number_format($atualiza['PRECO_TOTAL'], 2, ',', '');
                    $vl_total = number_format(($atualiza['PRECO_TOTAL'] + $freteTotal), 2, ',', '.');
                    $vl_frete = number_format($freteTotal, 2, ',', '.');

                    $exibir = [
                        'O_COD_RETORNO' => $atualiza['O_COD_RETORNO'],
                        'PRECO_TOTAL' => $vl_prods,
                        'FRETE' => $vl_frete,
                        'PRAZO' => $prazo,
                        'TOTAL_PEDIDO' => $vl_total,
                        'MSG' => 'Sucesso'
                    ];
                }
            } else {
                $exibir = [
                    'O_COD_RETORNO' => $atualiza['O_COD_RETORNO'],
                    'PRECO_TOTAL' => 0,
                    'FRETE' => 0,
                    'PRAZO' => 0,
                    'TOTAL_PEDIDO' => 0,
                    'MSG' => $atualiza['O_DESC_CURTO']
                ];
            }
        } catch (\Exception $e) {
            $exibir = [
                'O_COD_RETORNO' => 1,
                'PRECO_TOTAL' => 0,
                'FRETE' => 0,
                'TOTAL_PEDIDO' => 0,
                'MSG' => $e->getMessage()
            ];
        }

        echo json_encode($exibir);
    }

    public function RemoverCheckout()
    {
        $token = Session::get('acesso');

        $id = filter_input(INPUT_POST, "id");
        $remover = $this->model->RemoverProdutoCheckout($id, $token);

        echo json_encode($remover);
    }

    private function retornaFreteCode($pedido)
    {
        if (!empty($pedido['O_NM_ENTREGA'])) {
            $tipoentrega = strtoupper($pedido['O_NM_ENTREGA']);

            if ($tipoentrega == 'SEDEX' || $tipoentrega == 'PAC') {
                if ($tipoentrega == 'SEDEX') {
                    return \PagSeguro\Enum\Shipping\Type::SEDEX;
                } else if ($tipoentrega == 'PAC') {
                    return \PagSeguro\Enum\Shipping\Type::PAC;
                }
            } else {
                return \PagSeguro\Enum\Shipping\Type::NOT_SPECIFIED;
            }
        } else {
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $msg = 'Não foi informado o tipo de entrega no pedido, por favor verifique os dados';
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    private function retornaURLPagSeguro($formaspagamento, $payment, $credenciais, $pedido)
    {
        try {

            $transaction = $payment->register($credenciais);
            $this->retornaURLPagamento($formaspagamento, $transaction, $pedido['O_TOKEN_PED'], $pedido);
        } catch (\Exception $e) {
            $msg = Common::retornoMsgXml($e->getMessage());
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    private function retornaCredenciaisPagSeguro($config, $pedido = null)
    {
        $email = Common::encrypt_decrypt('decrypt', $config['PAGSEG_EMAIL']);

        if (!empty($email)) {
            if (!empty($config['PAGSEG_TIPO_ENVIO'])) {
                if ($config['PAGSEG_TIPO_ENVIO'] == 'sandbox') {
                    $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_SAND_TOKEN']);
                } else {
                    $token = Common::encrypt_decrypt('decrypt', $config['PAGSEG_PRD_TOKEN']);
                }

                $configPagSeguro = new \PagSeguro\Configuration\Configure();
                $configPagSeguro->setEnvironment($config['PAGSEG_TIPO_ENVIO']);
                $configPagSeguro->setCharset('UTF-8');
                $configPagSeguro->setLog(true, LOGS_URL . '/pseg.log');
                $configPagSeguro::setAccountCredentials($email, $token);

                return $configPagSeguro::getAccountCredentials();
            } else {
                if (!empty($pedido)) {
                    $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
                }
                $msg = 'Não foi informado o tipo de ambiente do PagSeguro, por favor verifique com o administrador';
                $situacao = 'warning';
                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            }
        } else {
            if (!empty($pedido)) {
                $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            }
            $msg = 'Não foi informado o email de credencial para o PagSeguro, por favor verifique com o administrador';
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    private function retornaListaProdutos($pedido, $payment, $tipoentrega, $cep_origem, $cep_destino)
    {
        $listapedido = $this->model->ListaProdutosPedido($pedido['O_TOKEN'], $pedido['O_PED_ID']);

        if (count($listapedido) > 0) {

            $cep = preg_replace("/[^0-9]/", '', $pedido['O_CEP']);

            if (!empty($cep) && !empty($pedido['O_LOGRADOURO']) && !empty($pedido['O_NUMERO']) && !empty($pedido['O_BAIRRO']) && !empty($pedido['O_CIDADE']) && !empty($pedido['O_UF']) && !empty($pedido['O_PAIS'])) {
                foreach ($listapedido as $ped) {

                    //$raiz_cubica = round(pow($ped['PESO_CUBICO'], 1 / 3), 2);
                    //$comprimento =  $raiz_cubica < 16 ? 16 : $raiz_cubica;
                    //$altura = $raiz_cubica < 2 ? 2 : $raiz_cubica;
                    //$largura = $raiz_cubica < 11 ? 11 : $raiz_cubica;
                    //$peso = $ped['PESO'] < 0.3 ? 0.3 : $ped['PESO'];
                    //$diametro = hypot($comprimento, $largura);

                    //$frete = Common::calcValodFrete($tipoentrega, $cep_origem, $cep_destino, $peso, $altura, $largura, $comprimento, $diametro);
                    //$freteTotal = number_format($frete['VALOR'], 2, '.', '');

                    //$payment->addItem($ped['PRD_ID'], $ped['PRD_NOME'], $ped['QTDE'], $ped['PRECO_TOTAL'], $peso, $freteTotal);

                    $payment->addItems()->withParameters($ped['PRD_ID'], $ped['PRD_NOME'], $ped['QTDE'], $ped['PRD_PRECO']);
                }

                return $payment;
            } else {
                $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
                $msg = 'Não foi possível gerar o pedido pois faltam informações do endereço, por favor verifique os dados';
                $situacao = 'warning';
                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            }
        } else {
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $msg = 'Não foi possível resgatar os produtos do carrinho, por favor entre em contato com o administrador';
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    private function retornaInformacoesCartao($hashcartao, $valorparcela, $pedido, $payment, $parcelas, $cpfcnpjcartao)
    {
        $cep = preg_replace("/[^0-9]/", '', $pedido['O_CEP']);
        $tel = Common::separa_tel_ddd($pedido['O_TEL_CEL']);

        if (!empty($cep) && !empty($pedido['O_LOGRADOURO']) && !empty($pedido['O_NUMERO']) && !empty($pedido['O_BAIRRO']) && !empty($pedido['O_CIDADE']) && !empty($pedido['O_UF']) && !empty($pedido['O_PAIS'])) {

            if (!empty($parcelas) && !empty($valorparcela) && !empty($cpfcnpjcartao) && !empty($tel)) {

                $payment->setToken($hashcartao);
                $payment->setInstallment()->withParameters($parcelas, $valorparcela, 2);
                $payment->setBilling()->setAddress()->withParameters(
                    $pedido['O_LOGRADOURO'],
                    $pedido['O_NUMERO'],
                    $pedido['O_BAIRRO'],
                    $cep,
                    $pedido['O_CIDADE'],
                    $pedido['O_UF'],
                    $pedido['O_PAIS'],
                    !empty($pedido['O_COMPLEMENTO']) ? $pedido['O_COMPLEMENTO'] : ''
                );
                $payment->setHolder()->setBirthdate(date($pedido['O_DT_NASCIMENTO']));
                $payment->setHolder()->setName($pedido['O_NOME_COMPLETO']);
                $payment->setHolder()->setPhone()->withParameters(
                    preg_replace("/[^0-9]/", '', $tel[0]),
                    preg_replace("/[^0-9]/", '', $tel[1])
                );
                $payment->setHolder()->setDocument()->withParameters(
                    strlen($cpfcnpjcartao) == 11 ? 'CPF' : 'CNPJ',
                    $cpfcnpjcartao
                );

                return $payment;
            } else {
                $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
                $msg = 'Não foi possivel gerar o pedido pois faltam algumas informações de parcelamento, telefone ou cpf do usuário do cartão, por favor verifique os dados';
                $situacao = 'warning';
                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            }
        } else {
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $msg = 'Não foi possivel gerar o pedido pois faltam algumas informações de endereço do usuário do cartão, por favor verifique os dados';
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    private function retornaURLPagamento($oppag, $transaction, $tokenped, $pedido)
    {
        $dados = [];

        $autorizado = true;
        $redirect = false;

        if (!empty($oppag) && !empty($tokenped)) {
            if ($transaction) {
                switch ($oppag) {
                    case 'CREDIT_CARD':

                        $dados['I_TOKEN'] = $tokenped;
                        $dados['I_DATE'] = $transaction->getDate();
                        $dados['I_CODE'] = $transaction->getCode();
                        $dados['I_REFERENCE'] = $transaction->getReference();
                        $dados['I_STATUS'] = $transaction->getStatus();
                        $dados['I_PAYMENT_METHOD_TYPE'] = $transaction->getPaymentMethod()->getType();
                        $dados['I_PAY_METHOD_TYPE_STR'] = 'CREDIT_CARD';
                        $dados['I_PAYMENT_METHOD_CODE'] = $transaction->getPaymentMethod()->getCode();
                        $dados['I_PAYMENT_LINK'] = NULL;
                        $dados['I_ITEM_COUNT'] = $transaction->getItemCount();

                        $redirect = false;

                        break;
                    case 'BOLETO':

                        $dados['I_TOKEN'] = $tokenped;
                        $dados['I_DATE'] = $transaction->getDate();
                        $dados['I_CODE'] = $transaction->getCode();
                        $dados['I_REFERENCE'] = $transaction->getReference();
                        $dados['I_STATUS'] = $transaction->getStatus();
                        $dados['I_PAYMENT_METHOD_TYPE'] = $transaction->getPaymentMethod()->getType();
                        $dados['I_PAY_METHOD_TYPE_STR'] = 'BOLETO';
                        $dados['I_PAYMENT_METHOD_CODE'] = $transaction->getPaymentMethod()->getCode();
                        $dados['I_PAYMENT_LINK'] = $transaction->getPaymentLink();
                        $dados['I_ITEM_COUNT'] = $transaction->getItemCount();

                        $redirect = false;

                        break;
                    case 'ONLINE_DEBIT':

                        $dados['I_TOKEN'] = $tokenped;
                        $dados['I_DATE'] = $transaction->getDate();
                        $dados['I_CODE'] = $transaction->getCode();
                        $dados['I_REFERENCE'] = $transaction->getReference();
                        $dados['I_STATUS'] = $transaction->getStatus();
                        $dados['I_PAYMENT_METHOD_TYPE'] = $transaction->getPaymentMethod()->getType();
                        $dados['I_PAY_METHOD_TYPE_STR'] = 'ONLINE_DEBIT';
                        $dados['I_PAYMENT_METHOD_CODE'] = $transaction->getPaymentMethod()->getCode();
                        $dados['I_PAYMENT_LINK'] = NULL;
                        $dados['I_ITEM_COUNT'] = $transaction->getItemCount();

                        $paymentlink = $transaction->getPaymentLink();
                        $redirect = true;

                        break;
                }

                if ($autorizado) {
                    if ($transaction) {
                        $this->model->AtualizarPedidoSiteAPI($dados);
                    }
                }

                if (!$redirect) {
                    Common::redir('Pedidos/ResumoPedido/' . $tokenped);
                } else {
                    Common::redirExt($paymentlink);
                }
            } else {
                $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
                $msg = 'Não foi possivel gerar o pedido pois não foi gerada a transação do PagSeguro, por favor entre em contato com o administrador';
                $situacao = 'warning';
                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            }
        } else {
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $msg = 'Não foi possivel gerar o pedido pois faltam algumas informações de opção de pagamento e token do pedido, por favor verifique os dados';
            $situacao = 'warning';
            Common::alert($msg, $situacao, 'acao');
            Common::redir($_SERVER['HTTP_REFERER'], 1);
        }
    }

    public function retornaDadoscartao()
    {

        $token = Session::get('acesso');
        $id = filter_input(INPUT_POST, "cartid");

        $cartao = $this->modelsistema->CartoesUsuarioPorTokenId($token, $id)[0];

        $dados['CART_NOME'] = Common::encrypt_decrypt('decrypt', $cartao['CART_NOME']);
        $dados['CART_NUMERO'] = Common::encrypt_decrypt('decrypt', $cartao['CART_NUMERO']);
        $dados['CART_CCV'] = Common::encrypt_decrypt('decrypt', $cartao['CART_CCV']);
        $dados['CART_VALIDADE'] = Common::encrypt_decrypt('decrypt', $cartao['CART_VALIDADE']);
        $dados['CART_CPF'] = Common::encrypt_decrypt('decrypt', $cartao['CART_CPF']);

        echo json_encode($dados);
    }

    public function GerarPedido()
    {
        try {
            $token = Session::get("acesso");

            $contatosite = $this->modelsistema->ContatosdoSite()[0];
            $config = $this->modelsistema->Configuracoes()[0];
            $cliente = $this->model->ClientePorToken($token);

            $dados['I_TOKEN'] = $token;
            $endpadrao = filter_input(INPUT_POST, "endereco", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

            $dados['I_END_PADRAO'] = $endpadrao[0];

            //DADOSPARA A GRAVAÇAO DO PEDIDO
            $dados['I_TIPO_ENTREGA'] = filter_input(INPUT_POST, "tipo_entrega");
            $dados['I_MEIO_PAGAMENTO'] = filter_input(INPUT_POST, "meio");
            $dados['I_COMENTARIO'] = filter_input(INPUT_POST, "comentario", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_BANDEIRA'] = filter_input(INPUT_POST, "bandeiraimg");

            //DADOS DO CARTÃO
            $numerocartao = preg_replace("/[^0-9]/", "", filter_input(INPUT_POST, "numerocartao"));
            $ccv = filter_input(INPUT_POST, "cvv");
            $titular = filter_input(INPUT_POST, "titular");
            $validadecartao = filter_input(INPUT_POST, "validadecartao");
            $cpftitular = filter_input(INPUT_POST, "cpftitular");

            $dados['I_CART_ID'] = intval(filter_input(INPUT_POST, "opcartaoslct"));
            $dados['I_NUM_CARTAO'] = Common::encrypt_decrypt('encrypt', $numerocartao);
            $dados['I_CVV'] = Common::encrypt_decrypt('encrypt', $ccv);
            $dados['I_TITULAR'] = Common::encrypt_decrypt('encrypt', $titular);
            $dados['I_VALIDADE'] = Common::encrypt_decrypt('encrypt', $validadecartao);
            $dados['I_CPF_TITULAR'] = Common::encrypt_decrypt('encrypt', $cpftitular);

            //CONFIGURAÇÕES PARA O PAGSEGURO
            $opcart = filter_input(INPUT_POST, "opcart");
            $hashcartao = filter_input(INPUT_POST, "hashcartao");
            $parcelas = filter_input(INPUT_POST, "parcelas");
            $valorparcela = filter_input(INPUT_POST, "valorparcela");
            $cepfrete = filter_input(INPUT_POST, "cep_frete");
            $formaspagamento = filter_input(INPUT_POST, "formaspagamento");
            $hashComprador = filter_input(INPUT_POST, "hashComprador");

            $tel = Common::separa_tel_ddd($cliente['TELEFONE_CEL']);
            $nome = $cliente['NOME_COMPLETO'];
            $email = $cliente['EMAIL'];
            $cod = preg_replace('/\D/', '', $tel[0]);
            $fone = preg_replace('/\D/', '', $tel[1]);
            $tipodoc = $cliente['TIPO_DOC'];
            $doc = preg_replace('/\D/', '', $cliente['NUM_DOC']);
            $banco = filter_input(INPUT_POST, "banco");

            $dados['I_VALOR_PAGTO'] = filter_input(INPUT_POST, "valorPgto");
            $dados['I_HASH_CARTAO'] = $hashcartao;
            $dados['I_HASH_COMPRADOR'] = $hashComprador;
            $dados['I_PARCELAS'] = $parcelas;
            $dados['I_VALOR_PARCELAS'] = $valorparcela;

            //TRATA O CPF DO TITULAR DO CARTÃO INFORMADO PARA PASSAR PARA A API
            if (intval($opcart) == 2) {
                $cpfcartao = preg_replace('/\D/', '', $cpftitular);
            } else if (intval($opcart) > 0) {
                $cpfcartao = Common::encrypt_decrypt('decrypt', $this->model->DadosCartaoCliente($dados['I_CART_ID'])[0]['CART_CPF']);
            }

            //CALCULA O FRETE
            $cep_origem = Common::somenteNumeros($contatosite['CEP']);
            $cep_destino = Common::somenteNumeros($cepfrete);

            $pesoPedido = $this->model->PesoRealPedido($token, 0, 0);

            //verifica o tipo de meio de entrega
            $meioentrega = $this->model->DadosMeioEntrega($dados['I_TIPO_ENTREGA'])[0];

            if (intval($meioentrega['CALC_FRETE']) == 1 && intval($meioentrega['RESTRICAO_ABRANG']) != 1) {
                $raiz_cubica = round(pow($pesoPedido['PESO_CUBICO'], 1 / 3), 2);
                $comprimento =  $raiz_cubica < 16 ? 16 : $raiz_cubica;
                $altura = $raiz_cubica < 2 ? 2 : $raiz_cubica;
                $largura = $raiz_cubica < 11 ? 11 : $raiz_cubica;
                $peso = $pesoPedido['PESO'] < 0.3 ? 0.3 : $pesoPedido['PESO'];
                $diametro = hypot($comprimento, $largura);

                $frete = Common::calcValodFrete($dados['I_TIPO_ENTREGA'], $cep_origem, $cep_destino, $peso, $altura, $largura, $comprimento, $diametro);
                $freteTotal = $frete['VALOR'];
                $prazo = $frete['PRAZO'];
            } else {
                $freteTotal = $meioentrega['VALOR_COBRADO'];
                $prazo = intval($meioentrega['PRAZO_MAXIMO']);
            }

            $dados['I_FRETE'] = number_format((float) floatval($freteTotal), 4, '.', '');
            $dados['I_PRAZO'] = $prazo;

            $pedido = $this->model->GerarPedidoSite($dados);
            $payment = null;

            if (intval($pedido['O_COD_RETORNO']) != 0) {

                $this->modelsistema->DeletePedido($pedido['O_PED_ID']);

                $msg = $pedido['O_DESC_CURTO'];
                $situacao = 'danger';
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios/Checkout/' . $token);
            } else {

                if (intval($pedido['O_TP_PAG']) == 6) {

                    $pagamento = null;

                    try {
                        \PagSeguro\Library::initialize();
                        $credenciais = $this->retornaCredenciaisPagSeguro($config, $pedido);
                        $fretecode = $this->retornaFreteCode($pedido);
                        $tokenped = $pedido['O_TOKEN'] . '-' . $pedido['O_TOKEN_PED'];
                        $codigo = Common::encrypt_decrypt('encrypt', $tokenped);

                        switch ($formaspagamento) {
                            case 'CREDIT_CARD':
                                $payment = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard();
                                $payment = $this->retornaInformacoesCartao($hashcartao, $valorparcela, $pedido, $payment, $parcelas, $cpfcartao);
                                break;
                            case 'BOLETO':
                                $payment = new \PagSeguro\Domains\Requests\DirectPayment\Boleto();
                                break;
                            case 'ONLINE_DEBIT':
                                $payment = new \PagSeguro\Domains\Requests\DirectPayment\OnlineDebit();
                                $payment->setBankName($banco);
                                break;
                            case 'DEPOSIT':
                                break;
                        }
                        $payment->setMode('DEFAULT');
                        $payment->setSender()->setHash($hashComprador);
                        $payment->setSender()->setIp(Common::getUserIP());
                        $payment->setReference($pedido['O_TOKEN_PED'] . '-' . $formaspagamento);
                        $payment->setReceiverEmail(Common::encrypt_decrypt('decrypt', $config['PAGSEG_EMAIL']));
                        $payment->setCurrency("BRL");
                        $payment = $this->retornaListaProdutos($pedido, $payment, $dados['I_TIPO_ENTREGA'], $cep_origem, $cep_destino);
                        $payment->setSender()->setName($nome);
                        $payment->setSender()->setEmail($email);
                        $payment->setSender()->setPhone()->withParameters(
                            $cod,
                            $fone
                        );
                        $payment->setSender()->setDocument()->withParameters(
                            $tipodoc,
                            $doc
                        );
                        $payment->setShipping()->setAddress()->withParameters(
                            $pedido['O_LOGRADOURO'],
                            $pedido['O_NUMERO'],
                            $pedido['O_BAIRRO'],
                            $pedido['O_CEP'],
                            $pedido['O_CIDADE'],
                            $pedido['O_UF'],
                            $pedido['O_PAIS'],
                            $pedido['O_COMPLEMENTO']
                        );
                        $payment->setShipping()->setCost()->withParameters($dados['I_FRETE']);
                        $payment->setShipping()->setType()->withParameters($fretecode);
                        $payment->addParameter()->withArray([
                            'redirectUrl', URL . SITE_URL . '/Pedidos/RetornoPagamento/' . $codigo,
                            'notificationURL', URL . SITE_URL . '/Pedidos/SituacaoPedido/' . $pedido['O_TOKEN_PED']
                        ]);

                        $this->retornaURLPagSeguro($formaspagamento, $payment, $credenciais, $pedido);
                    } catch (\Exception $e) {
                        $msg = Common::retornoMsgXml($e->getMessage());
                        $this->model->RemoverPedidoCliente($pedido['O_TOKEN_PED']);
                        $situacao = 'danger';
                        Common::alert($msg, $situacao, 'acao');
                        Common::redir('Usuarios/Checkout/' . $token);
                    }
                } else {
                    Session::delete('carrinhovazio');

                    Common::redir('Pedidos/ResumoPedido/' . $pedido['O_TOKEN_PED']);
                }
            }
        } catch (\Exception $e) {
            $msg = Common::retornoMsgXml($e->getMessage());
            $this->modelsistema->DeletePedido($pedido['O_PED_ID']);
            $situacao = 'danger';
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/Checkout/' . $token);
        }
    }

    public function EditarInformacoes($id)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            if (Session::get('acesso'))
                $dados['tab'] = empty(Session::get('tab')) ? 1 : Session::get('tab');

            if (intval(Session::get('ativacao')) == 1) {
                $token = Session::get('acesso');
                $social_tipo = Session::get('social_tipo');
                $dados['cliente'] = $this->modelsistema->UsuarioPorToken($token, $social_tipo);
            }

            $dados['exibirlogos'] = false;
            $dados['urlactionpessoal'] = SITE_URL . "/Usuarios/SalvarDadosPessoais/" . $id;
            $dados['urlactionsenha'] = SITE_URL . "/Usuarios/SalvarNovaSenha/" . $id;
            $dados['urlactionimage'] = SITE_URL . "/Usuarios/SalvarFoto/" . $id;
            $dados['urlactionendereco'] = SITE_URL . "/Usuarios/SalvarEndereco/" . $id;
            $dados['urlactioncartao'] = SITE_URL . "/Usuarios/SalvarCartao/" . $id;
            $dados['removerendereco'] = SITE_URL . "/Usuarios/RemoverEndereco";
            $dados['removercartao'] = SITE_URL . "/Usuarios/RemoverCartao";

            parent::prepararView("Usuarios/pag_minhas_informacoes", $dados);
        }
    }

    public function MudarFavoritos($id, $pagina = null)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $token = Session::get('acesso');

            $busca['ITENS'] = 12;
            $total = $this->model->TotalProdutosFavoritos($token)[0];
            $totalpaginas = ceil($total['TOTAL'] / $busca['ITENS']);

            if ($pagina > $totalpaginas) {
                $$pagina = $totalpaginas;
            }
            if ($pagina < 1) {
                $pagina = 1;
            }

            $busca['TOKEN'] = $token;
            $busca['OFFSET'] = ($pagina - 1) * $busca['ITENS'];

            $dados['exibirlogos'] = false;
            $dados['listafavoritos'] = $this->model->ListaProdutosFavoritos($busca);
            $dados['totalpaginas'] = $totalpaginas;
            $dados['paginaatual'] = $pagina;
            $dados['resultados'] = $total['TOTAL'];
            $dados['urlaction'] = SITE_URL . '/Usuarios/MudarFavoritos/' . $id;

            parent::prepararView("Usuarios/pag_mudar_favoritos", $dados);
        }
    }

    public function RemoverFavorito()
    {
        $token = Session::get('acesso');

        $id = filter_input(INPUT_POST, "id");
        $remover = $this->model->RemoverProdutoFavorito($id, $token);

        echo json_encode($remover);
    }

    public function MeusPedidos($id = null)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            if (empty(Session::get("acesso"))) {
                $token = Session::get('carrinho');
            } else {
                $token = Session::get('acesso');
            }

            $dados['exibirlogos'] = false;
            $dados['listapedidos'] = $this->model->ListaPedidos($token);
            $dados['totaischeckout'] = !empty($dados['listapedidos']) ? $this->modelsistema->TotalPedido($token, $dados['listapedidos'][0]['TOKEN']) : 0;
            parent::prepararView("Usuarios/pag_meus_pedidos", $dados);
        }
    }

    public function SalvarDadosPessoais($id)
    {
        if (empty(Session::get("acesso"))) {
            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $this->validarCamposObrigatorio(1);

            $dados['I_TOKEN'] = Session::get('acesso');
            $dados['I_NOME_COMPLETO'] = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_TEL_RESID'] = filter_input(INPUT_POST, "tel_resid");
            $dados['I_TEL_COM'] = filter_input(INPUT_POST, "tel_com");
            $dados['I_TEL_CEL'] = filter_input(INPUT_POST, "tel_cel");
            $dados['I_TIPO_DOC'] = filter_input(INPUT_POST, "tipo_doc");
            $dados['I_NUM_DOC'] = filter_input(INPUT_POST, "cpf");
            $dados['I_DT_NASC'] = filter_input(INPUT_POST, "tel_dt_nascimento");
            $dados['I_GENERO'] = filter_input(INPUT_POST, "genero");
            $dados['I_EMAIL'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
            $dados['I_OCUPACAO'] = filter_input(INPUT_POST, "ocupacao", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            $retorno = $this->model->SalvarInfoDadosPessoais($dados);

            if (intval($retorno['O_COD_RETORNO']) != 0) {
                $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {
                $msg = 'Realizado com sucesso!';
                $situacao = 'success';
            }

            Session::set('tab', 1);
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/EditarInformacoes/' . $id);
        }
    }

    public function RefazerPedido($tokenped)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem permissão para realizar esta atividade';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $token = Session::get('acesso');

            $dados = [
                'TOKEN' => $token,
                'PED_TOKEN' => $tokenped
            ];

            $retorno = $this->model->RefazerPedidoCliente($dados);

            if (intval($retorno['O_COD_RETORNO']) != 0) {
                $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir($_SERVER['HTTP_REFERER'], 1);
            } else {
                $msg = 'Pedido refeito com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios/Checkout/' . $token);
            }
        }
    }

    public function SalvarFoto($id)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $this->validarCamposObrigatorio(2);

            $now = new \DateTime();

            $pastaantiga = filter_input(INPUT_POST, "pasta");

            $pasta = !empty($pastaantiga) ? $pastaantiga : $now->format('Y_m_d_H_i_s');

            if ($_FILES['foto_cliente']['name'] != "") {

                $ret = self::salvarImagem($_FILES['foto_cliente'], $pasta);

                $arquivo = $ret['list']['imagem'];

                $dados['I_TOKEN'] = Session::get('acesso');
                $dados['I_IMAGEM'] = $arquivo;
                $dados['I_PASTA'] = $pasta;

                $retorno = $this->model->SalvarFotoPessoal($dados);

                if (intval($retorno['O_COD_RETORNO']) != 0) {
                    $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                    $situacao = 'danger';
                } else {
                    $msg = 'Realizado com sucesso!';
                    $situacao = 'success';
                }
            } else {
                $msg = 'Nenhuma imagem selecionada, os dados permanecem os mesmos!';
                $situacao = 'success';
            }

            Session::set('tab', 2);
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/EditarInformacoes/' . $id);
        }
    }

    public function SalvarNovaSenha($id)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $this->validarCamposObrigatorio(3);

            $login = filter_input(INPUT_POST, "login");
            $atual = filter_input(INPUT_POST, "senha_atual");
            $senha = filter_input(INPUT_POST, "senha_nova");
            $csenha = filter_input(INPUT_POST, "conf_senha");

            $passatual = strtoupper(md5($login . $atual));
            $pass = strtoupper(md5($login . $senha));
            $cpass = strtoupper(md5($login . $csenha));

            if ($cpass != $pass) {
                $msg = 'Atenção! É necessário repetir a mesma senha';
                $situacao = 'danger';
            } else {
                $dados['I_TOKEN'] = Session::get('acesso');
                $dados['I_SENHA_ATUAL'] = $passatual;
                $dados['I_NOVA_SENHA'] = $pass;
                $dados['I_CONF_SENHA'] = $cpass;

                $retorno = $this->model->SalvarSenhaPessoal($dados);

                if (intval($retorno['O_COD_RETORNO']) != 0) {
                    $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                    $situacao = 'danger';
                } else {
                    $msg = 'Realizado com sucesso!';
                    $situacao = 'success';
                }
            }

            Session::set('tab', 3);
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/EditarInformacoes/' . $id);
        }
    }

    public function RemoverEndereco()
    {
        $dados['END_ID'] = filter_input(INPUT_POST, "id");
        $retorno = $this->model->RemoveEndereco($dados);

        if (intval($retorno['O_COD_RETORNO']) != 0) {
            $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
            $situacao = 'danger';
        } else {
            $msg = 'Realizado com sucesso!';
            $situacao = 'success';
        }

        echo json_encode(['msg' => $msg, 'tipo' => $situacao]);
    }

    public function RemoverCartao()
    {
        $dados['CART_ID'] = filter_input(INPUT_POST, "id");
        $retorno = $this->model->RemoveCartao($dados);

        if (intval($retorno['O_COD_RETORNO']) != 0) {
            $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
            $situacao = 'danger';
        } else {
            $msg = 'Realizado com sucesso!';
            $situacao = 'success';
        }

        echo json_encode(['msg' => $msg, 'tipo' => $situacao]);
    }

    public function SalvarEndereco($id)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $this->validarCamposObrigatorio(4);

            $dados['I_TOKEN'] = Session::get('acesso');
            $dados['I_END_ID'] = filter_input(INPUT_POST, "id");
            $dados['I_LOGRADOURO'] = filter_input(INPUT_POST, "logradouro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_COMPLEMENTO'] = filter_input(INPUT_POST, "complemento", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_CIDADE'] = filter_input(INPUT_POST, "cidade", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_BAIRRO'] = filter_input(INPUT_POST, "bairro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_UF'] = filter_input(INPUT_POST, "uf");
            $dados['I_NUM'] = filter_input(INPUT_POST, "numero");
            $dados['I_CEP'] = filter_input(INPUT_POST, "cep");
            $dados['I_PAIS'] = filter_input(INPUT_POST, "pais", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['I_ENVIAR'] = filter_input(INPUT_POST, "enviar");
            $dados['I_TIPO_END'] = filter_input(INPUT_POST, "tipo");
            $dados['I_END_STATUS'] = filter_input(INPUT_POST, "status");

            $retorno = $this->model->SalvarEnderecoPessoal($dados);

            if (intval($retorno['O_COD_RETORNO']) != 0) {
                $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {
                $msg = 'Realizado com sucesso!';
                $situacao = 'success';
            }

            Session::set('tab', 4);
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/EditarInformacoes/' . $id);
        }
    }

    public function SalvarCartao($id)
    {
        if (empty(Session::get("acesso"))) {

            $msg = 'Atenção! Você não tem acesso';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Home');
        } else {
            $this->validarCamposObrigatorio(6);

            $numerocartao = preg_replace('/\D/', '', filter_input(INPUT_POST, "numerocartao"));

            $dados['I_TOKEN'] = Session::get('acesso');
            $dados['I_CART_ID'] = empty(filter_input(INPUT_POST, "id")) ? 0 : filter_input(INPUT_POST, "id");
            $dados['I_CART_NOME'] = Common::encrypt_decrypt('encrypt', filter_input(INPUT_POST, "titular"));
            $dados['I_CART_NUMERO'] = Common::encrypt_decrypt('encrypt', $numerocartao);
            $dados['I_CART_CVV'] = Common::encrypt_decrypt('encrypt', filter_input(INPUT_POST, "cvv"));
            $dados['I_CPF_TITULAR'] = Common::encrypt_decrypt('encrypt', filter_input(INPUT_POST, "cpftitular"));
            $dados['I_CART_VALIDADE'] = Common::encrypt_decrypt('encrypt', filter_input(INPUT_POST, "validadecartao"));
            $dados['I_CART_STATUS'] = filter_input(INPUT_POST, "cartstatus");
            $dados['I_IS_USO'] = filter_input(INPUT_POST, "cartuso");
            $dados['I_OPCAO'] = 1;

            $retorno = $this->model->SalvarCartaoPessoal($dados);

            if (intval($retorno['O_COD_RETORNO']) != 0) {
                $msg = 'Atenção! ' . $retorno['O_DESC_CURTO'];
                $situacao = 'danger';
            } else {
                $msg = 'Realizado com sucesso!';
                $situacao = 'success';
            }

            Session::set('tab', 5);
            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios/EditarInformacoes/' . $id);
        }
    }

    private function validarCamposObrigatorio($tipo, $reg = null, $codigo = null)
    {
        if ($tipo == 1) {
            $id = filter_input(INPUT_POST, "id");

            $tipodoc = filter_input(INPUT_POST, "tipo_doc");

            $dados['Nome Completo'] = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            //$dados['Telefone Residencial'] = filter_input(INPUT_POST, "tel_resid");
            //$dados['Telefone Comercial'] = filter_input(INPUT_POST, "tel_com");
            $dados['Telefone Celular'] = filter_input(INPUT_POST, "tel_cel");

            if ($tipodoc == 'CPF') {
                $dados['CPF'] = filter_input(INPUT_POST, "cpf");
                $dados['Gênero'] = filter_input(INPUT_POST, "genero");
            } else {
                $dados['CNPJ'] = filter_input(INPUT_POST, "cpf");
            }

            $dados['E-Mail'] = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL, FILTER_VALIDATE_EMAIL);
            //$dados['Ocupação'] = filter_input(INPUT_POST, "ocupacao");

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }

        if ($tipo == 2) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Foto'] = $_FILES['foto_cliente'];

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }

        if ($tipo == 3) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Senha Atual'] = filter_input(INPUT_POST, "senha_atual");
            $dados['Nova Senha'] = filter_input(INPUT_POST, "senha_nova");
            $dados['Confirmar Senha'] = filter_input(INPUT_POST, "conf_senha");

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }

        if ($tipo == 4) {

            $id = filter_input(INPUT_POST, "idcli");
            $dados['Logradouro'] = filter_input(INPUT_POST, "logradouro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['Cidade'] = filter_input(INPUT_POST, "cidade", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['Bairro'] = filter_input(INPUT_POST, "bairro", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['UF'] = filter_input(INPUT_POST, "uf");
            $dados['Número'] = filter_input(INPUT_POST, "numero");
            $dados['CEP'] = filter_input(INPUT_POST, "cep");
            $dados['País'] = filter_input(INPUT_POST, "pais", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }

        if ($tipo == 5) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Nova Senha'] = filter_input(INPUT_POST, "senha_nova");
            $dados['Confirmar Senha'] = filter_input(INPUT_POST, "conf_senha");

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }

        if ($tipo == 6) {
            $id = filter_input(INPUT_POST, "idcli");
            $dados['Nome do Titular'] = filter_input(INPUT_POST, "titular", FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dados['Número do Cartão'] = filter_input(INPUT_POST, "numerocartao");
            $dados['CVV'] = filter_input(INPUT_POST, "cvv");
            $dados['CPF do Titular'] = filter_input(INPUT_POST, "cpftitular");
            $dados['Validade'] = filter_input(INPUT_POST, "validadecartao");

            if (!isset($reg)) {
                Common::validarInputsObrigatorio($dados, 'Usuarios/EditarInformacoes/' . $id);
            } else {
                Common::validarInputsObrigatorio($dados, 'Usuarios/AtivarRegistro/' . $codigo);
            }
        }
    }

    private function salvarImagem($foto, $pasta)
    {
        $config = array('tamanho' => 200000, 'largura' => 300, 'altura' => 300);

        $Imagem = new \Application\Controller\Common\Imagem($config);

        if (!file_exists(FOTOCLIENTE_PATH . '/' . $pasta)) {
            mkdir(FOTOCLIENTE_PATH . '/' . $pasta, 0777, true);
        }

        return $Imagem->executar(FOTOCLIENTE_PATH . '/' . $pasta, $foto);
    }

    private function removerImagem($descricao_imagem)
    {
        $caminho_imagem = FOTOCLIENTE_URL . "/" . $descricao_imagem;
        if (file_exists($caminho_imagem)) {
            @unlink($caminho_imagem);
        }
        clearstatcache(TRUE, $caminho_imagem);
    }

    public function GetEndereco()
    {
        $token = Session::get('acesso');
        $id = filter_input(INPUT_POST, "id");

        $sessfrete = Session::get('sessfrete');
        $tipoentr = $sessfrete['TIPO_ENTREGA'];

        $retorno = $this->model->BuscarEndereco($token, $id, $tipoentr);
        echo json_encode($retorno);
    }

    public function DadosOpEntrega($codigo)
    {

        $meioentrega = $this->model->DadosMeioEntrega($codigo)[0];

        $sessfrete = Session::get('sessfrete');

        if (intval($sessfrete['PRAZO']) > 1) {
            $dados['prazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dias úteis' : null;
        } else {
            $dados['prazo'] = intval($sessfrete['PRAZO']) > 0 ? 'Em até ' . $sessfrete['PRAZO'] . ' dia útil' : null;
        }

        $frete = str_replace(',', '.', str_replace('.', '', $sessfrete['FRETE']));
        $total = str_replace(',', '.', str_replace('.', '', $sessfrete['TOTAL_PEDIDO']));

        $dados['frete'] = 'R$ ' . number_format($frete, 2, ',', '.');
        $dados['total'] = 'R$ ' . number_format($total, 2, ',', '.');
        $dados['imagem'] = IMGS_URL . '/' . $meioentrega['IMAGEM'];
        $dados['nome']  = $meioentrega['NOME_MEIO'];

        echo json_encode($dados);
    }
}
